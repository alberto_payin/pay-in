package com.mobile.application.payin.views;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.application.payin.R;
import com.mobile.application.payin.common.interfaces.AsyncResponse;
import com.mobile.application.payin.common.serverconnections.ServerPost;
import com.mobile.application.payin.common.utilities.CustomGson;
import com.mobile.application.payin.common.utilities.HandleServerError;
import com.mobile.application.payin.dto.arguments.UserMobileCreateArguments;

import java.util.HashMap;

public class PaymentsUserCreate extends AppCompatActivity implements AsyncResponse {
    private EditText etTaxName, etTaxAddress, etTaxNumber, etPin, etPinConfirm;

    private Context context;
    private AsyncResponse delegate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payments_user_create);

        context = this;
        delegate = this;

        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_action_back, null));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        SharedPreferences pref = getSharedPreferences(getResources().getString(R.string.prefs), MODE_PRIVATE);

        boolean debug = pref.getBoolean("debug", false);

        if (debug) toolbar.setBackgroundColor(Color.parseColor("#ff1a1a"));
        else toolbar.setBackgroundColor(Color.parseColor("#e9af30"));

        etTaxName = (EditText) findViewById(R.id.etTaxName);
        etTaxAddress = (EditText) findViewById(R.id.etTaxAddress);
        etTaxNumber = (EditText) findViewById(R.id.etTaxNumber);
        etPin = (EditText) findViewById(R.id.etPin);
        etPinConfirm = (EditText) findViewById(R.id.etPinConfirm);

        Button btnCancel = (Button) findViewById(R.id.btnCancel);
        Button btnSend = (Button) findViewById(R.id.btnSend);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!etPin.getText().toString().matches("\\d{4}")) {
                    android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
                    builder.setTitle("Error en la introducción de datos")
                            .setMessage("El código Pin ha de consistir en cuatro caracteres numéricos.")
                            .setPositiveButton(getString(R.string.button_ok), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });
                    builder.create().show();
                } else if (!etPin.getText().toString().matches("\\d{4}") && !etPin.getText().toString().equals(etPinConfirm.getText().toString())) {
                    android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
                    builder.setTitle("Error en la introducción de datos")
                            .setMessage("Los códigos PIN introducidos no coinciden.")
                            .setPositiveButton(getString(R.string.button_ok), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });
                    builder.create().show();
                } else {
                    UserMobileCreateArguments args = new UserMobileCreateArguments();

                    args.TaxName = etTaxName.getText().toString();
                    args.TaxAddress = etTaxAddress.getText().toString();
                    args.TaxNumber = etTaxNumber.getText().toString();
                    args.Pin = etPin.getText().toString();

                    ServerPost tarea = new ServerPost(context);
                    tarea.delegate = delegate;
                    tarea.refresh = true;
                    tarea.execute(getString(R.string.apiUser), CustomGson.getGson().toJson(args));
                }
            }
        });
    }

    @Override
    public void onAsyncFinish(HashMap<String, String> map) {
        if (HandleServerError.Handle(map, this, this)) return ;

        if (map.get("route").contains(getString(R.string.apiUser))) {
            Toast.makeText(this, "Creación de usuario correcta", Toast.LENGTH_LONG).show();
            Intent i = new Intent(PaymentsUserCreate.this, CreateTarjeta.class);
            startActivity(i);
            finish();
        }
    }
}
