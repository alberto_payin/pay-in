﻿using PayIn.Common.Resources;
using System.ComponentModel.DataAnnotations;
using Xp.Application.Arguments;

namespace PayIn.Application.Dto.Internal.Arguments.PaymentMedia
{
	public partial class PaymentMediaCreateWebCardArguments : MobileConfigurationArguments
	{
		[RegularExpression(@"^\d{4}$", ErrorMessageResourceType = typeof(UserResources), ErrorMessageResourceName = "PinErrorMessage")]
		[Required(AllowEmptyStrings=false)]   public string  Pin                  { get; set; }
		[Required(AllowEmptyStrings=false)]   public string  Name                 { get; set; }
		                                      public int     OrderId              { get; set; }
		                                      public int     PublicPaymentMediaId { get; set; }
		                                      public int     PublicTicketId       { get; set; }
		                                      public int     PublicPaymentId      { get; set; }
		[Required(AllowEmptyStrings = false)] public string  BankEntity           { get; set; }

		#region PaymentMediaCreateWebCardArguments
		public PaymentMediaCreateWebCardArguments(string pin, string name, int orderId, int publicPaymentMediaId, int publicTicketId, int publicPaymentId, string bankEntity, 
			string deviceManufacturer, string deviceModel, string deviceName, string deviceSerial, string deviceId, string deviceOperator, string deviceImei, string deviceMac, string operatorSim, string operatorMobile)
			: base(deviceManufacturer, deviceModel, deviceName, deviceSerial, deviceId, deviceOperator, deviceImei, deviceMac, operatorSim, operatorMobile)
		{
			Pin = pin;
			Name = name;
			OrderId = orderId;
			PublicPaymentMediaId = publicPaymentMediaId;
			PublicTicketId = publicTicketId;
			PublicPaymentId = publicPaymentId;
			BankEntity = bankEntity;
		}
		#endregion PaymentMediaCreateWebCardArguments
	}
}
