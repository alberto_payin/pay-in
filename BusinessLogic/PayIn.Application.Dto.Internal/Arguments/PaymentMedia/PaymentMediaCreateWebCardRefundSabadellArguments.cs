﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xp.Application.Arguments;
using Xp.Common;
using Xp.Common.Dto.Arguments;

namespace PayIn.Application.Dto.Internal.Arguments.PaymentMedia
{
	public class PaymentMediaCreateWebCardRefundSabadellArguments : IArgumentsBase
	{
		public int PublicPaymentMediaId { get; set; }
		public int PublicTicketId       { get; set; }
		public int PublicPaymentId      { get; set; }
		public decimal Amount           { get; set; }
		public string Currency          { get; set; }
		public int OrderId              { get; set; }
		public int Terminal             { get; set; }

		#region Constructors
		public PaymentMediaCreateWebCardRefundSabadellArguments(int publicPaymentMediaId, int publicTicketId, int publicPaymentId, decimal amount, string currency, int orderId, int terminal)
		{
			PublicPaymentMediaId = publicPaymentMediaId;
			PublicTicketId = publicTicketId;
			PublicPaymentId = publicPaymentId;
			Amount = amount;
			Currency = currency;
			OrderId = orderId;
			Terminal = terminal;
		}
		#endregion Constructors
	}
}
