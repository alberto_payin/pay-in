﻿using PayIn.Common.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xp.Common.Dto.Arguments;

namespace PayIn.Application.Dto.Internal.Arguments.PaymentMedia
{
	public class PaymentMediaRefundArguments : IArgumentsBase
	{
		[RegularExpression(@"^\d{4}$", ErrorMessageResourceType = typeof(UserResources), ErrorMessageResourceName = "PinErrorMessage")]
		[Required(AllowEmptyStrings=false)] public string  Pin                  { get; set; }
		                                    public int     PublicPaymentMediaId { get; set; }
		                                    public int     PublicTicketId       { get; set; }
		                                    public int     PublicPaymentId      { get; set; }
		[Range(0, double.MaxValue)]         public decimal Amount               { get; set; }
		                                    public int     Order                { get; set; }

		#region Constructors
		public PaymentMediaRefundArguments(string pin, int publicPaymentMediaId, int publicTicketId, int publicPaymentId, int order, decimal amount)
		{
			Pin = pin;
			PublicPaymentMediaId = publicPaymentMediaId;
			PublicTicketId = publicTicketId;
			PublicPaymentId = publicPaymentId;
			Order = order;
			Amount = amount;
		}
		#endregion Constructors
	}
}
