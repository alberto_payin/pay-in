﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayIn.Application.Dto.Internal.Results.PaymentMedia
{
	public class PaymentMediaPayResult
	{
		public class Data
		{
			public int PaymentMediaId { get; set; }
			public int PublicPaymentMediaId { get; set; }
			public int PublicTicketId { get; set; }
			public int PublicPaymentId { get; set; }
		}

		public int Amount { get; set; }
		public string Currency { get; set; }
		public int OrderId { get; set; }
		public string Signature { get; set; }
		public string CommerceCode { get; set; }
		public int Terminal { get; set; }
		public string Response { get; set; }
		public string Code { get; set; }
		public string ErrorCode { get; set; }
		public string ErrorText { get; set; }
		public string ErrorPublic { get; set; }
		public bool IsError { get; set; }
		public string AuthorizationCode { get; set; }
		public string TransactionType { get; set; }
		public bool SecurePayment { get; set; }
		public string Language { get; set; }
		public string CardIdentifier { get; set; }
		public Data MerchantData { get; set; }
		public string CardCountry { get; set; }
	}
}
