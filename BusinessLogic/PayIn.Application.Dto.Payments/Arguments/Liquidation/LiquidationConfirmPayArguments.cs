﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xp.Common.Dto.Arguments;

namespace PayIn.Application.Dto.Payments.Arguments.Liquidation
{
	public class LiquidationConfirmPayArguments : IArgumentsBase
	{
		public int Id { get; set; }

		public LiquidationConfirmPayArguments(int id)
		{
			Id  = id;
		}

	}
}
