﻿using Xp.Common;
using Xp.Common.Dto.Arguments;

namespace PayIn.Application.Dto.Payments.Arguments.Liquidation
{
	public class LiquidationGetAllArguments : IArgumentsBase
	{
		public string Filter { get; set; }
		public XpDate Since { get; set; }
		public XpDate Until { get; set; }

		public LiquidationGetAllArguments(string filter, XpDate since, XpDate until)
		{
			Filter = filter ?? "";
			Since = since;
			Until = until;
		}
	}
}
