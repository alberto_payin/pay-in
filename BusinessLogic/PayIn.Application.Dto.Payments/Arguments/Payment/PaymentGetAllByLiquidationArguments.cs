﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xp.Common;
using Xp.Common.Dto.Arguments;

namespace PayIn.Application.Dto.Payments.Arguments.Payments
{
	public class PaymentGetAllByLiquidationArguments : IArgumentsBase
	{
		public string Filter { get; set; }
		public int? LiquidationId { get; set; }

		#region Constructors
		public PaymentGetAllByLiquidationArguments(string filter, int? liquidationId)
		{
			Filter = filter ?? "";
			LiquidationId = liquidationId;
		}
		#endregion Constructors
	}
}
