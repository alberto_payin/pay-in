﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xp.Common.Dto.Arguments;
using System.ComponentModel.DataAnnotations;
using PayIn.Common.Resources;
using Xp.Common;
using PayIn.Common.Security;


namespace PayIn.Application.Dto.Payments.Arguments.PaymentConcession
{
	public class PaymentConcessionCreateArguments : IArgumentsBase
	{
		#region SupplierName
		[Display(Name = "resources.paymentConcession.supplierName")]
		[Required]
		public string SupplierName { get; set; }
		#endregion SupplierName

		#region TaxName
		[Display(Name = "resources.paymentConcession.taxName")]
		[Required]
		public string TaxName { get; set; }
		#endregion TaxName

		#region TaxNumber
		[Display(Name = "resources.paymentConcession.taxNumber")]
		[Required]
		public string TaxNumber { get; set; }
		#endregion TaxNumber

		#region TaxAddress
		[Display(Name = "resources.paymentConcession.taxAddress")]
		[Required]
		public string TaxAddress { get; set; }
		#endregion TaxAddress

		#region Pin
		[RegularExpression(@"^\d{4}$", ErrorMessageResourceType = typeof(UserResources), ErrorMessageResourceName = "PinErrorMessage")]
		[Display(Name = "resources.paymentConcession.pin")]
		[Required]
		[DataType(DataType.Password)]
		public string Pin { get; set; }
		#endregion Pin

		#region PinConfirmation
		[Compare("Pin", ErrorMessageResourceType = typeof(UserResources), ErrorMessageResourceName = "ConfirmPin")]
		[Display(Name = "resources.paymentConcession.pinConfirmation")]
		[Required]
		[DataType(DataType.Password)]
		public string PinConfirmation { get; set; }
		#endregion PinConfirmation

		#region Observations
		[Display(Name = "resources.paymentConcession.observations")]
		public string Observations { get; set; }
		#endregion Observations

		#region Address
		[Display(Name = "resources.paymentConcession.address")]
		[Required]
		public string Address { get; set; }
		#endregion Address

		#region Phone
		[Display(Name = "resources.paymentConcession.phone")]
		[Required]
		public string Phone { get; set; }
		#endregion Phone

		#region BankAccountNumber
		[RegularExpression(@"^(\s*\w){2}(\s*\d){22}\s*$", ErrorMessageResourceType = typeof(UserResources), ErrorMessageResourceName = "AccountErrorMessage")]
		[Display(Name = "resources.paymentConcession.bankAccountNumber")]
		[Required]
		public string BankAccountNumber { get; set; }
		#endregion BankAccountNumber

		#region FormA
		[DataSubType(DataSubType.Image, DataSubType.Pdf)]
		[Display(Name = "resources.paymentConcession.formA")]
		[Required]
		public byte[] FormA { get; set; }
		#endregion FormA

		#region AcceptTerms
		[Display(Name = "resources.security.acceptTerms")]
		[Required]
		public bool AcceptTerms { get; set; }
		#endregion AcceptTerms

		#region Constructors
		public PaymentConcessionCreateArguments(string supplierName, string taxName, string taxNumber, string taxAddress, string pin, string pinConfirmation, string observations, string address, string phone, string bankAccountNumber, byte[] formA, bool acceptTerms)
		{
			//ServiceSupplier
			SupplierName      = supplierName;
			TaxName           = taxName;
			TaxNumber         = taxNumber;
			TaxAddress        = taxAddress;
			Pin               = pin;
			PinConfirmation   = pinConfirmation;
			//PaymentConcession
			Observations      = observations;
			Address           = address;
			Phone             = phone;
			BankAccountNumber = bankAccountNumber;
			FormA             = formA;
			AcceptTerms       = acceptTerms;
		}
		#endregion Constructors
	}
}
