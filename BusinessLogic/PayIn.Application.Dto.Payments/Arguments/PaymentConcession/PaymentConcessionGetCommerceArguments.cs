﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xp.Common.Dto.Arguments;

namespace PayIn.Application.Dto.Payments.Arguments.PaymentConcession
{
	public class PaymentConcessionGetCommerceArguments: IArgumentsBase
	{
		public int Id { get; private set; }

		#region Constructors
		public PaymentConcessionGetCommerceArguments(int id)
		{
			Id = id;
		}
		#endregion Constructors
	}
}
