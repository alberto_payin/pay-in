﻿using PayIn.Common;
using PayIn.Common.Resources;
using System.ComponentModel.DataAnnotations;
using Xp.Common;
using Xp.Domain;
using Xp.Common.Dto.Arguments;

namespace PayIn.Application.Dto.Payments.Arguments.PaymentConcession
{
	public partial class PaymentConcessionUpdateArguments : IUpdateArgumentsBase<PayIn.Domain.Payments.PaymentConcession>
	{																												           
																						  public int Id { get; private set; }
		[Display(Name = "resources.paymentConcession.name")]                 [Required]	  public string Name { get; private set; }		
		//[Display(Name = "resources.paymentConcession.state")]	             			  public ConcessionState State { get; private set; }
		[Display(Name = "resources.enumResources.concessionState_")]                	  public ConcessionState State { get; private set; }
		[Display(Name = "resources.paymentConcession.payinCommission")]	     [Required]   public decimal PayinCommission { get; private set; }
				    													      			  public XpDate ConcessionCreateDate { get; private set; }
		[Display(Name = "resources.paymentConcession.liquidationAmountMin")] [Required]	  public decimal LiquidationAmountMin { get; private set; }
    																			       	  public string Address { get; private set; }
		[Display(Name = "resources.paymentConcession.phone")]                [Required]   public string Phone { get; private set; }			
		[Display(Name = "resources.paymentConcession.observations")]					  public string Observations { get; private set; }

		#region Constructors
		public PaymentConcessionUpdateArguments(int id, string name, ConcessionState state, decimal payinCommission,decimal liquidationAmountMin, XpDate concessionCreateDate,string address, string phone, string observations)
		{
			Id = id;
			Name = name;		
			State = state;
			PayinCommission = payinCommission;
			ConcessionCreateDate = concessionCreateDate;
			LiquidationAmountMin = liquidationAmountMin;
			Address = address;
			Phone = phone;			
			Observations = observations;

		}
		#endregion Constructors
	}
}
