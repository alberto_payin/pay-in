﻿using PayIn.Common;
using PayIn.Common.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xp.Common;
using Xp.Common.Dto.Arguments;

namespace PayIn.Application.Dto.Payments.Arguments.PaymentConcession
{
	public class PaymentConcessionUpdateCommerceArguments : IArgumentsBase
	{
																							public int Id { get; set; }
		[Display(Name = "resources.paymentConcession.bankAccountNumber")]	    [Required]	public string AccountNumber { get; set; }
		[Display(Name = "resources.paymentConcession.payinCommission")]		    [Required]	public decimal PayinCommission { get; set; }
		[Display(Name = "resources.paymentConcession.liquidationAmountMin")]	[Required]	public decimal LiquidationAmountMin { get; set; }
																							public string FormUrl { get; set; }
		[Display(Name = "resources.paymentConcession.name")]			         			public string Name { get; set; }		
		[Display(Name = "resources.paymentConcession.observations")]						public string Observations { get; set; }
		[Display(Name = "resources.paymentConcession.phone")]				    [Required]	public string Phone { get; set; }
		[Display(Name = "resources.enumResources.concessionState_")]                        public ConcessionState State { get; set; }

		#region FormA
		[DataSubType(DataSubType.Image, DataSubType.Pdf)]
		[Display(Name = "resources.paymentConcession.formA")]		
		public byte[] FormA { get; set; }
		#endregion FormA
		
		#region Constructors
		public PaymentConcessionUpdateCommerceArguments(int id, string accountNumber, decimal payinCommission, decimal liquidationAmountMin, string formUrl, string name, string observations, string phone, ConcessionState state, byte[] formA)
		{
			Id = id;
			AccountNumber = accountNumber;
			PayinCommission = payinCommission;
			LiquidationAmountMin = liquidationAmountMin;
			FormUrl = FormUrl;
			Name = name;
			Observations = observations;
			Phone = phone;
			State = state;
			FormA = formA;
		}

		#endregion Constructors
	}
}
