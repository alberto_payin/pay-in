﻿using Xp.Common.Dto.Arguments;

namespace PayIn.Application.Dto.Payments.Arguments.PaymentMedia
{
	public class PaymentMediaCreateWebCardSabadellArguments : IArgumentsBase
	{
		public string Datos { get; set; }

		#region Constructors
		public PaymentMediaCreateWebCardSabadellArguments(string datos)
		{
			Datos = datos;
		}
		#endregion Constructors
	}
}
