﻿using Xp.Application.Dto;

namespace PayIn.Application.Dto.Arguments.PaymentMedia
{
	public partial class PaymentMediaDeleteArguments : ArgumentsBase
	{
		public int Id { get; set; }

		#region Constructors
		public PaymentMediaDeleteArguments()
		{
		}
		public PaymentMediaDeleteArguments(int id)
		{
			Id = id;
		}
		#endregion Constructors
	}
}
