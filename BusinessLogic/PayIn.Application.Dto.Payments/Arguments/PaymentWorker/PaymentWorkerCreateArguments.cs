﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xp.Common.Dto.Arguments;

namespace PayIn.Application.Dto.Payments.Arguments.PaymentWorker
{
	public class PaymentWorkerCreateArguments : IArgumentsBase
	{
		[Display(Name = "resources.paymentWorker.name")] 	public string Name { get; set; }
		[Display(Name = "resources.paymentWorker.login")]	public string Login { get; set; }

		#region Constructors
		public PaymentWorkerCreateArguments(string name, string login)
		{
			Name = name;
			Login = login;
		}
		#endregion Constructors
	}
}
