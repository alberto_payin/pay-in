﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xp.Common.Dto.Arguments;

namespace PayIn.Application.Dto.Payments.Arguments.PaymentWorker
{
	public class PaymentWorkerMobileAcceptAssignmentArguments : IArgumentsBase
	{
		public int Id { get; set; }
	}
}
