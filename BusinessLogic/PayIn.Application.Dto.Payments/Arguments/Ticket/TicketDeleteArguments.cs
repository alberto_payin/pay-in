﻿using Xp.Common.Dto.Arguments;

namespace PayIn.Application.Dto.Payments.Arguments.Ticket
{
	public partial class TicketDeleteArguments : IDeleteArgumentsBase<PayIn.Domain.Payments.Ticket>
	{
		public int Id { get; set; }

		#region Constructors
		public TicketDeleteArguments(int id)
		{
			Id = id;
		}
		#endregion Constructors
	}
}