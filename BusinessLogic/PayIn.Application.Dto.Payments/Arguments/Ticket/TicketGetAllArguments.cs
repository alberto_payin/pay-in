﻿using Xp.Common;
using Xp.Common.Dto.Arguments;

namespace PayIn.Application.Dto.Payments.Arguments.Ticket
{
	public partial class TicketGetAllArguments : IArgumentsBase
	{
		public string Filter { get; set; }
		public XpDate Since { get; set; }
		public XpDate Until { get; set; }

		#region Constructors
		public TicketGetAllArguments(string filter,XpDate since,XpDate until) 
		{
			Filter = filter ?? "";
			Since = since;
			Until = until;
		}
		#endregion Constructors
	}
}