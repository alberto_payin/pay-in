﻿//using PayIn.Application.Dto.Results.Main;
//using PayIn.Application.Dto.Results.Ticket;
//using Xp.Application.Dto;
using Xp.Common.Dto.Arguments;

namespace PayIn.Application.Dto.Payments.Arguments.Ticket
{
	public class TicketGetArguments : IArgumentsBase
	{
		public int Id { get; set; }

		#region Constructors
		public TicketGetArguments(int id)
		{
			Id = id;
		}
		#endregion Constructors

		//#region Cast from TicketPayResult
		//public static implicit operator TicketGetArguments(TicketPayResult item)
		//{
		//	return new TicketGetArguments
		//	{
		//		Id = item.TicketId
		//	};
		//}
		//#endregion Cast from TicketPayResult

		//#region Cast from MainGetAll_PaymentResult
		//public static implicit operator TicketGetArguments(MainMobileGetAllResultBase.Payment item)
		//{
		//	return new TicketGetArguments
		//	{
		//		Id = item.Id
		//	};
		//}
		//#endregion Cast from MainGetAll_PaymentResult
	}
}
