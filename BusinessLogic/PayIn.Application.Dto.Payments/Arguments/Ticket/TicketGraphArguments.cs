﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xp.Common;
using Xp.Common.Dto.Arguments;

namespace PayIn.Application.Dto.Payments.Arguments.Ticket
{
	public class TicketGraphArguments : IArgumentsBase
    {
		public XpDate Since { get; set; }
		public XpDate Until { get; set; }

		public TicketGraphArguments(XpDate since, XpDate until) 
		{
			Since = since;
			Until = until;
		}
    }
}
