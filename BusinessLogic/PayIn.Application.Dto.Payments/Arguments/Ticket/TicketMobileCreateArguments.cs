﻿using PayIn.Common.Resources;
using System;
using System.ComponentModel.DataAnnotations;
using Xp.Common;
using Xp.Common.Dto.Arguments;

namespace PayIn.Application.Dto.Payments.Arguments.Ticket
{
	public class TicketMobileCreateArguments : ICreateArgumentsBase<PayIn.Domain.Payments.Ticket>
	{
		           public string      Reference    { get; set; }
		           public string      Title        { get; set; }
		[Required] public XpDateTime  Date         { get; set; }
		[Range(0.01, double.MaxValue, ErrorMessageResourceType=typeof(TicketResources), ErrorMessageResourceName="AmountMoreThanZeroException")]
		[Required] public decimal     Amount       { get; set; }
		[Required] public int         ConcessionId { get; set; }

		#region Constructor
		public TicketMobileCreateArguments(string reference, string title, DateTime date, decimal amount, int concessionId)
		{
			Reference    = reference ?? "";
			Title        = title ?? "";
			Date         = date;
			Amount       = amount;
			ConcessionId = concessionId;
		}
		#endregion Constructor
	}
}
