﻿using Xp.Common.Dto.Arguments;

namespace PayIn.Application.Dto.Payments.Arguments.Ticket
{
	public class TicketMobileGetWithPaymentMediasArguments : IArgumentsBase
	{
		public int Id { get; set; }

		#region Constructors
		public TicketMobileGetWithPaymentMediasArguments(int id)
		{
			Id = id;
		}
		#endregion Constructors
	}
}
