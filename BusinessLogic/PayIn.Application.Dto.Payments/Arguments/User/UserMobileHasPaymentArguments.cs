﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Xp.Common.Dto.Arguments;

namespace PayIn.Application.Dto.Payments.Arguments.User
{
	public class UserMobileHasPaymentArguments : IArgumentsBase
	{
		[Required(AllowEmptyStrings = false)] public string Login { get; set; }

		#region Constructors
		public UserMobileHasPaymentArguments(string login)
		{
			Login = login;
		}
		#endregion Constructors
	}
}
