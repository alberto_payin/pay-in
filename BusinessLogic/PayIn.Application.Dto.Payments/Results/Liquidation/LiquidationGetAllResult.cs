﻿using PayIn.Common;
using System;
using Xp.Common;

namespace PayIn.Application.Dto.Payments.Results.Liquidation
{
	public partial class LiquidationGetAllResult 
	{
		public int Id { get; set; }
		public decimal Amount { get; set; }
		public decimal Payin { get; set; }
		public decimal Total { get; set; }
		public XpDate PaymentDate { get; set; }
		public XpDate Since { get; set; }
		public XpDate Until { get; set; }
		public LiquidationState State { get; set; }
		public XpDateTime RequestDate { get; set; }
		public int PaymentsCount { get; set; }		
	}
}
