﻿using PayIn.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayIn.Application.Dto.Payments.Results.PaymentConcession
{
	public class PaymentConcessionGetCommerceResult
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public decimal PayinCommission { get; set; }
		public decimal LiquidationAmountMin { get; set; }
		public string FormUrl { get; set; }
		public string AccountNumber { get; set; }
		public string Observations { get; set; }
		public string Phone { get; set; }
		public ConcessionState State { get; set; }
	}
}
