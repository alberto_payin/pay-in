﻿using PayIn.Common;
using System.Collections.Generic;

namespace PayIn.Application.Dto.Payments.Results.PaymentConcession
{

	public partial class PaymentConcessionGetResult
	{

		public int Id { get; set; }
		public string Name { get; set; }
		public ConcessionState State { get; set; }
		public ServiceType Type { get; set; }	
		public decimal PayinCommission { get; set; }
		public decimal LiquidationAmountMin { get; set; }
		public string Phone { get; set; }
		public string Observations { get; set; }
	}
}