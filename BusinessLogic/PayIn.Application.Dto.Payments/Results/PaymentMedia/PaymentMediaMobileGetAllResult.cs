﻿using PayIn.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayIn.Application.Dto.Payments.Results.PaymentMedia
{
	public class PaymentMediaMobileGetAllResult
	{
		public int               Id                  { get; set; }
		public string            Title               { get; set; }
		public string            Subtitle            { get; set; }
		public string            NumberHash          { get; set; }
		public int               VisualOrder         { get; set; }
		public int?              ExpirationMonth     { get; set; }
		public int?              ExpirationYear      { get; set; }
		public PaymentMediaState State               { get; set; }
		public PaymentMediaType  Type                { get; set; }
		public string            BankEntity          { get; set; }
	}
}