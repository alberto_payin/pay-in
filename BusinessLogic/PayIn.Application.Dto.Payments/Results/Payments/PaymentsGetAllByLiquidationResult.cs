﻿using PayIn.Common;
using Xp.Common;

namespace PayIn.Application.Dto.Payments.Results.Payments
{
	public class PaymentsGetAllByLiquidationResult
	{
		public int              Id                { get; set; }
		public decimal          Amount            { get; set; }
		public decimal          Payin             { get; set; }
		public decimal          Total             { get; set; }
		public XpDateTime       Date              { get; set; }
		public string           Name              { get; set; }
		public string           PaymentMedia      { get; set; }
		public string           TaxName           { get; set; }
		public string           SupplierName      { get; set; }
		public string           SupplierAddress   { get; set; }
		public string           SupplierTaxNumber { get; set; }
		public string           UserLogin         { get; set; }
		public string           UserName          { get; set; }
		public decimal          TicketAmount      { get; set; }
		public string           NumberHash        { get; set; }
		public PaymentMediaType CardType          { get; set; }
		public PaymentState     State             { get; set; }
		public string           StateName         { get; set; }
		public string           FotoUrl           { get; set; }
		public string           SupplierLogin     { get; set; }
		public bool             FotoExists        { get; set; }
		public int              TicketId          { get; set; }
	}
}
