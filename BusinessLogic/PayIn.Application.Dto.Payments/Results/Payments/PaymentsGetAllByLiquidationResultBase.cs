﻿using Xp.Application.Dto;
using Xp.Common;

namespace PayIn.Application.Dto.Payments.Results.Payments
{
	public class PaymentsGetAllByLiquidationResultBase : ResultBase<PaymentsGetAllByLiquidationResult>
	{
		public XpDateTime LiquidationSince { get; set; }
		public XpDateTime LiquidationUntil { get; set; }
	}
}
