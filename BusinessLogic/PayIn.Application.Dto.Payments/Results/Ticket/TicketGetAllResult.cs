﻿using PayIn.Common;
using PayIn.Domain.Payments;
using System;
using System.Collections.Generic;
using Xp.Common;

namespace PayIn.Application.Dto.Payments.Results.Ticket
{
	public class TicketGetAllResult
	{
		public XpDateTime Date { get; set; }
		public int Id { get; set; }
		public string Reference { get; set; }
		public string Title { get; set; }
		public decimal Amount { get; set; }
		public decimal PayedAmount { get; set; }
		public TicketStateType State { get; set; }
	}
}
