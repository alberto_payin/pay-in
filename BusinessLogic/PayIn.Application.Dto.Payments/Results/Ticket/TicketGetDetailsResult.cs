﻿using PayIn.Common;
using PayIn.Domain.Payments;
using System;
using System.Collections.Generic;
using Xp.Common;

namespace PayIn.Application.Dto.Payments.Results.Ticket
{
	public class TicketGetDetailsResult
	{
		//public class UserLogin 
		//{
		//	public string UserLogins { get; set; }
		//}
		public class Payment
		{
			public int Id { get; set; }
			public XpDateTime Date { get; set; }
			public PaymentState State { get; set; }
			public string StateAlias { get; set; }
			public string TaxName { get; set; }
			public string NumberHash { get; set; }
			public decimal Amount { get; set; }
			public decimal Payin { get; set; }
			public decimal Total { get; set; }
		}

		public int Id { get; set; }
		public XpDateTime Date { get; set; }
		public TicketStateType State { get; set; }
		public string Title { get; set; }
		public string Reference { get; set; }
		public decimal Amount { get; set; }
		public decimal Total { get; set; }
		public string SupplierName { get; set; }
		public string SupplierTaxAddress { get; set; }
		public string SupplierTaxNumber { get; set; }
		public string SupplierLogin { get; set; }
		public string SupplierFotoUrl { get; set; }
		public decimal PayedAmount { get; set; }
		public string WorkerName { get; set; }
		public IEnumerable<Payment> Payments { get; set; }

		#region Constructors
		public TicketGetDetailsResult()
		{
			Payments = new List<Payment>();
		}
		#endregion Constructors
	}
}
