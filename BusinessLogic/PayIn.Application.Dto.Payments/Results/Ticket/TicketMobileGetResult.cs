﻿using PayIn.Common;
using System;
using System.Collections.Generic;
using Xp.Common;

namespace PayIn.Application.Dto.Payments.Results.Ticket
{
	public class TicketMobileGetResult
	{
		public class Payment
		{
			public int          Id               { get; set; }
			public decimal      Amount           { get; set; }
			public string       UserName         { get; set; }
			public string       PaymentMediaName { get; set; }
			public XpDateTime   Date             { get; set; }
			public PaymentState State            { get; set; }
			public bool         CanBeReturned    { get; set; }
		}

		public int             Id              { get; set; }
		public string          Reference       { get; set; }
		public string          Title           { get; set; }
		public decimal         Amount          { get; set; }
		public decimal         PayedAmount     { get; set; }
		public XpDateTime      Date            { get; set; }
		public TicketStateType State           { get; set; }
		public bool            CanReturn       { get; set; }
		public string          SupplierName    { get; set; }
		public string          SupplierAddress { get; set; }
		public string          SupplierNumber  { get; set; }
		public string          SupplierPhone   { get; set; }
		public string          WorkerName      { get; set; }

		public IEnumerable<Payment> Payments   { get; set; }
	}
}
