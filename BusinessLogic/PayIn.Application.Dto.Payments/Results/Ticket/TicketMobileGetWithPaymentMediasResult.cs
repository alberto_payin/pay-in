﻿using PayIn.Common;
using System.Collections.Generic;
using Xp.Common;

namespace PayIn.Application.Dto.Payments.Results.Ticket
{
	public class TicketMobileGetWithPaymentMediasResult
	{
		public class Payment
		{
			public int Id { get; set; }
			public decimal Amount { get; set; }
			public string UserName { get; set; }
			public string PaymentMediaName { get; set; }
			public XpDateTime Date { get; set; }
			public PaymentState State { get; set; }
			public bool CanBeReturned { get; set; }
		}

		public class PaymentMedia
		{
			public int Id { get; set; }
			public string Title { get; set; }
			public string Subtitle { get; set; }
			public string NumberHash { get; set; }
			public int VisualOrder { get; set; }
			public int? ExpirationMonth { get; set; }
			public int? ExpirationYear { get; set; }
			public PaymentMediaState State { get; set; }
			public PaymentMediaType Type { get; set; }
			public string BankEntity { get; set; }
		}

		public class TicketResult
		{
			public int Id { get; set; }
			public string Reference { get; set; }
			public string Title { get; set; }
			public decimal Amount { get; set; }
			public decimal PayedAmount { get; set; }
			public XpDateTime Date { get; set; }
			public TicketStateType State { get; set; }
			public bool CanReturn { get; set; }
			public string SupplierName { get; set; }
			public string SupplierAddress { get; set; }
			public string SupplierNumber { get; set; }
			public string SupplierPhone { get; set; }
			public string WorkerName { get; set; }

			public IEnumerable<Payment> Payments { get; set; }
		}

		public bool HasPayment { get; set; }
		public TicketResult Ticket { get; set; }
		public IEnumerable<PaymentMedia> PaymentMedias { get; set; }
	}
}
