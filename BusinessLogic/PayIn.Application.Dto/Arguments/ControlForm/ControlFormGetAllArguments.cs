﻿using System;
using System.Collections.Generic;
using System.Text;
using Xp.Application.Dto;
using Xp.Common.Dto.Arguments;

namespace PayIn.Application.Dto.Arguments.ControlForm
{
    public class ControlFormGetAllArguments : IArgumentsBase
    {
		public string Filter { get; set; }

		#region Constructors
		public ControlFormGetAllArguments(string filter)
		{
			Filter = filter ?? "";
		}
		#endregion Constructors
    }
}
