﻿using PayIn.Common;
using PayIn.Common.Resources;
using System.ComponentModel.DataAnnotations;
using Xp.Common.Dto.Arguments;

namespace PayIn.Application.Dto.Arguments.ControlFormArgument
{
    public class ControlFormArgumentCreateArguments : IArgumentsBase
	{
		[Display(Name = "resources.controlFormArgument.name")]                      public string                    Name         { get; set; }
		[Display(Name = "resources.controlFormArgument.observations")]              public string                    Observations { get; set; }
		[Display(Name = "resources.enumResources.controlFormArgumentType_")]	   	public ControlFormArgumentType   Type         { get; set; }
		[Display(Name = "resources.enumResources.controlFormArgumentTarget_")]      public ControlFormArgumentTarget Target       { get; set; }
																	                public int                       FormId       { get; set; }
		[Display(Name = "resources.controlFormArgument.isRequired")]                public bool                      IsRequired   { get; set; }

		#region Constructors
		public ControlFormArgumentCreateArguments(string name, string observations, ControlFormArgumentType type, ControlFormArgumentTarget target, int formId, bool isRequired)
		{
			Name         = name;
			Observations = observations;
			Type         = type;
			Target       = target;
			FormId       = formId;
			IsRequired   = isRequired;
		}
		#endregion Constructors
    }
}
