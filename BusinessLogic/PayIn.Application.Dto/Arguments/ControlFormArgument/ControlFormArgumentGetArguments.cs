﻿using Xp.Common.Dto.Arguments;

namespace PayIn.Application.Dto.Arguments.ControlFormArgument
{
    public class ControlFormArgumentGetArguments : IArgumentsBase
    {
		public int Id { get; set; }

		#region Constructors
		public ControlFormArgumentGetArguments(int id)
		{
			Id = id;
		}
		#endregion Constructors
    }
}
