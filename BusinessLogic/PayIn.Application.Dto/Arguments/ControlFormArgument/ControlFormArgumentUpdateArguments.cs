﻿using PayIn.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Xp.Application.Dto;
using Xp.Common.Dto.Arguments;

namespace PayIn.Application.Dto.Arguments.ControlFormArgument
{
	public class ControlFormArgumentUpdateArguments : IArgumentsBase
	{
		                                                                          public int                       Id           { get; set; }
		[Display(Name = "resources.controlFormArgument.name")]		              public string                    Name         { get; set; }
		[Display(Name = "resources.controlFormArgument.observations")]		      public string                    Observations { get; set; }
		[Display(Name = "resources.enumResources.controlFormArgumentType_")]	  public ControlFormArgumentType   Type         { get; set; }
		[Display(Name = "resources.enumResources.controlFormArgumentTarget_")]	  public ControlFormArgumentTarget Target       { get; set; }
		[Display(Name = "resources.controlFormArgument.isRequired")]		      public bool                      IsRequired   { get; set; }

		#region Constructors
		public ControlFormArgumentUpdateArguments(int id, string name, string observations, ControlFormArgumentType type, ControlFormArgumentTarget target, bool isRequired)
		{
			Id           = id;
			Name         = name;
			Observations = observations;
			Type         = type;
			Target       = target;
			IsRequired   = isRequired;
		}
		#endregion Constructors
	}
}
