﻿using System;
using System.Collections.Generic;
using Xp.Common.Dto.Arguments;

namespace PayIn.Application.Dto.Arguments.ControlFormAssign
{
	public partial class ControlFormAssignUpdateArguments : IArgumentsBase
	{
		public class Value
		{
			public int      Id { get; set; }
			public string    ValueString { get; set; }
			public decimal?  ValueNumeric { get; set; }
			public bool?     ValueBool { get; set; }
			public DateTime? ValueDateTime { get; set; }
		}
		
		public class Assign
		{
			public int                Id     { get; set; }
			public IEnumerable<Value> Values { get; set; }
		}

		public IEnumerable<Assign> Assigns { get; set; }

		#region Constructors
		public ControlFormAssignUpdateArguments(IEnumerable<Assign> assigns)
		{
			Assigns = assigns;
		}
		#endregion Constructors
	}
}
