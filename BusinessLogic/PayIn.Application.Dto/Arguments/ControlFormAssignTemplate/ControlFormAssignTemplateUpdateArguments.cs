﻿using System;
using System.Collections.Generic;
using Xp.Common.Dto.Arguments;

namespace PayIn.Application.Dto.Arguments.ControlFormAssignTemplate
{
	public partial class ControlFormAssignTemplateUpdateArguments : IArgumentsBase
	{
		public class Value
		{
			public int      Id { get; set; }
			public string    ValueString { get; set; }
			public decimal?  ValueNumeric { get; set; }
			public bool?     ValueBool { get; set; }
			public DateTime? ValueDateTime { get; set; }
		}

		public int                Id     { get; set; }
		public IEnumerable<Value> Values { get; set; }

		#region Constructors
		public ControlFormAssignTemplateUpdateArguments(int id, IEnumerable<Value> values)
		{
			Id = id;
			Values = values;
		}
		#endregion Constructors
	}
}
