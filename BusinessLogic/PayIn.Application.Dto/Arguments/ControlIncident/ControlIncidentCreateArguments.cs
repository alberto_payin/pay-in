﻿using PayIn.Common;
using System;
using System.Collections.Generic;
using Xp.Common;
using Xp.Common.Dto.Arguments;

namespace PayIn.Application.Dto.Arguments.ControlIncident
{
    public class ControlIncidentCreateArguments : IArgumentsBase
    {

		public class IncidentItem {
			//public PresenceType PresenceType { get; set; }
			public XpDateTime Date { get; set; }
			public byte[] Image { get; set; }
			public decimal? Latitude { get; set; }
			public decimal? Longitude { get; set; }
			//public Boolean SaveFacialRecognition { get; set; } // Sobra
			//public Boolean SaveTrack { get; set; } // Sobra
			//public Boolean CheckTimetable { get; set; } // Sobra
			public int ItemId { get; set; }
		}
		public partial class Value
		{
			public int Id { get; set; }
			public string ValueString { get; set; }
			public decimal? ValueNumeric { get; set; }
			public bool? ValueBool { get; set; }
			public DateTime? ValueDateTime { get; set; }
		}
		public partial class Assign
		{
			public int Id { get; set; }
			public IEnumerable<Value> Values { get; set; }
		}
		public partial class PlanningCheck
		{
			public int Id { get; set; }
			public IEnumerable<Assign> Assigns { get; set; }
		}

		public partial class PlanningItem
		{
			public int Id { get; set; }
			public IEnumerable<PlanningCheck> Checks { get; set; }
		}

		public IncidentType IncidentType { get; set; }
		public String Observations { get; set; }
		public IncidentItem Item { get; set; }
		public IEnumerable<PlanningItem> PlanningItems { get; private set; }
		public IEnumerable<PlanningCheck> PlanningChecks { get; private set; }
		
		#region Constructors
		public ControlIncidentCreateArguments(IncidentType incidentType, String observations, IncidentItem item, IEnumerable<PlanningItem> planningItems, IEnumerable<PlanningCheck> planningChecks)
		{
			IncidentType = incidentType;
			PlanningItems = planningItems;
			PlanningChecks = planningChecks;
			Observations = observations ?? "";
			Item = item;
		}
		#endregion Constructors
    }
}
