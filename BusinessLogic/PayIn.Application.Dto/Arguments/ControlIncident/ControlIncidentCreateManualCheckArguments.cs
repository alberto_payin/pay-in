﻿using PayIn.Common;
using System;
using System.Collections.Generic;
using Xp.Common;
using Xp.Common.Dto.Arguments;

namespace PayIn.Application.Dto.Arguments.ControlIncident
{
	public class ControlIncidentCreateManualCheckArguments : IArgumentsBase
	{
		public partial class Value
		{
			public int       Id { get; set; }
			public string    ValueString { get; set; }
			public decimal?  ValueNumeric { get; set; }
			public bool?     ValueBool { get; set; }
			public DateTime? ValueDateTime { get; set; }
			public byte[]    ValueImage { get; set; }
		}
		public partial class Assign
		{
			public int Id { get; set; }
			public IEnumerable<Value> Values { get; set; }
		}
		public partial class IncidentItem
		{
			public XpDateTime Date { get; set; }
			public byte[]     Image { get; set; }
			public decimal?   Latitude { get; set; }
			public decimal?   Longitude { get; set; }
			public int        Id { get; set; }
			public int?       CheckPointId { get; set; }
			public int?       CheckId { get; set; }
			public IEnumerable<Assign> Assigns { get; set; }
		}

		public IncidentType IncidentType { get; set; }
		public String       Observations { get; set; }
		public IncidentItem Item { get; set; }

		#region Constructors
		public ControlIncidentCreateManualCheckArguments(IncidentType incidentType, String observations, IncidentItem item)
		{
			IncidentType = incidentType;
			Observations = observations ?? "";
			Item = item;
		}
		#endregion Constructors
	}
}
