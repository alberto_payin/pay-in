﻿using System;
using System.Collections.Generic;
using Xp.Common;
using Xp.Common.Dto.Arguments;

namespace PayIn.Application.Dto.Arguments.ControlPresence
{
	public partial class ControlPresenceMobileCheckArguments : IArgumentsBase
	{
		public partial class Value {
			public int		 Id			   { get; set; }
			public string	 ValueString   { get; set; }
			public decimal?  ValueNumeric  { get; set; }
			public bool?	 ValueBool	   { get; set; }
			public DateTime? ValueDateTime { get; set; }
			public byte[]    Image         { get; set; }
		}
		public partial class Assign
		{
			public int Id { get; set; }
			public IEnumerable<Value> Values { get; set; }
		}
		public partial class Item
		{
			public int  Id           { get; set; }
			public int  CheckPointId { get; set; }
			public int? CheckId      { get; set; }
			public IEnumerable<Assign> Assigns { get; set; }
		}

		public int							   Id             { get; private set; }
		public XpDateTime					   Date           { get; private set; }
		public byte[]						   Image		  { get; private set; }
		public decimal?						   Latitude		  { get; private set; }
		public decimal?						   Longitude   	  { get; private set; }
		public string						   Observations	  { get; private set; }
		public IEnumerable<Item>			   Items          { get; private set; }

		#region Constructors
		public ControlPresenceMobileCheckArguments(int id, XpDateTime date, byte[] image, decimal? latitude, decimal? longitude, string observations,
												   IEnumerable<Item> items)		
		{
			Id              = id;
			Date            = date;
			Image           = image;
			Latitude        = latitude;
			Longitude       = longitude;
			Items           = items;
			Observations    = observations ?? "";
		}
		#endregion Constructors
	}
}
