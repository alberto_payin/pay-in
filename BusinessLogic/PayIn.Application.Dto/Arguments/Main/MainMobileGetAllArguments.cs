﻿using Xp.Common.Dto.Arguments;

namespace PayIn.Application.Dto.Arguments.Main
{
	public partial class MainMobileGetAllArguments : IArgumentsBase
	{
		public string Filter   { get; private set; }

		#region Constructors
		public MainMobileGetAllArguments(string filter)
		{
			Filter   = filter ?? "";
		}
		#endregion Constructors
	}
}
