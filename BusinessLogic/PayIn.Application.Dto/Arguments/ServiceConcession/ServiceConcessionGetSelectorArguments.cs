﻿using Xp.Common.Dto.Arguments;

namespace PayIn.Application.Dto.Arguments.ServiceConcession
{
	public partial class ServiceConcessionGetSelectorArguments : IArgumentsBase
	{
		public string Filter { get; set; }

		#region Constructors
		public ServiceConcessionGetSelectorArguments(string param)
		{
			Filter = param;
		}
		#endregion Constructors
	}
}
