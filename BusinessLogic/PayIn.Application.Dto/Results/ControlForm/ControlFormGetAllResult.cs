﻿using System;
using System.Collections.Generic;
using System.Text;
using PayIn.Common;

namespace PayIn.Application.Dto.Results.ControlForm
{
    public class ControlFormGetAllResult
    {
		public class ControlFormGetAllResult_Arguments
		{
			public int                       Id           { get; set; }
			public string                    Name         { get; set; }
			public string                    Observations { get; set; }
			public ControlFormArgumentType   Type         { get; set; }
			public ControlFormArgumentTarget Target       { get; set; }
			public bool                      IsRequired   { get; set; }
		}
		
		public int    Id           { get; set; }
		public string Name         { get; set; }
		public string Observations { get; set; }
		public IEnumerable<ControlFormGetAllResult_Arguments> Arguments { get; set; }
    }
}
