﻿using PayIn.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace PayIn.Application.Dto.Results.ControlFormArgument
{
	public class ControlFormArgumentGetFormResult
	{
		public int                       Id           { get; set; }
		public string                    Name         { get; set; }
		public ControlFormArgumentType   Type         { get; set; }
		public string                    TypeAlias    { get; set; }
		public ControlFormArgumentTarget Target       { get; set; }
		public string                    TargetAlias  { get; set; }
		public bool                      IsRequired   { get; set; }
	}
}
