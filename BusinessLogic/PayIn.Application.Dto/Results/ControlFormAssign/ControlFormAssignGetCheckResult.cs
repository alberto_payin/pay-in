﻿
using PayIn.Common;
using System;
using System.Collections.Generic;
namespace PayIn.Application.Dto.Results.ControlFormAssign
{
	public partial class ControlFormAssignGetCheckResult
	{
		public class Value
		{
			public int                       Id { get; set; }
			public string                    Name { get; set; }
			public ControlFormArgumentType   Type { get; set; }
			public bool?                     ValueBool { get; set; }
			public DateTime?                 ValueDateTime { get; set; }
			public decimal?                  ValueNumeric { get; set; }
			public string                    ValueString { get; set; }
		}

		public int    Id       { get; set; }
		public int    FormId   { get; set; }
		public string FormName { get; set; }
		public IEnumerable<Value> Values { get; set; }
	}
}
