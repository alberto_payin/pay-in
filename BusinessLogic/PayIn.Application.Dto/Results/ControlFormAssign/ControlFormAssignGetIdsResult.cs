﻿
using PayIn.Common;
using System;
using System.Collections.Generic;
using Xp.Common;
namespace PayIn.Application.Dto.Results.ControlFormAssign
{
	public partial class ControlFormAssignGetIdsResult
	{
		public class Value
		{
			public int                       Id            { get; set; }
			public string                    Name          { get; set; }
			public string                    Observations  { get; set; }
			public ControlFormArgumentType   Type          { get; set; }
			public ControlFormArgumentTarget Target        { get; set; }
			public bool                      IsRequired    { get; set; }
			public bool?                     ValueBool     { get; set; }
			public DateTime?                 ValueDateTime { get; set; }
			public decimal?                  ValueNumeric  { get; set; }
			public string                    ValueString   { get; set; }
		}

		public class Assign
		{
			public int                Id             { get; set; }
			public int                PresencesCount { get; set; }
			public int                FormId         { get; set; }
			public string             FormName       { get; set; }
			public PresenceType       Type           { get; set; }
			public IEnumerable<Value> Values         { get; set; }
			public XpDateTime         DateTime       { get; set; }
		}

		public IEnumerable<Assign> Assigns { get; set; }
	}
}
