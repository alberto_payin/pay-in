﻿using PayIn.Common;
using System;
using System.Collections.Generic;

namespace PayIn.Application.Dto.Results.ControlFormAssignCheckPoint
{
	public partial class ControlFormAssignCheckPointGetCheckResult
	{
		public class Argument
		{
			public int Id { get; set; }
			public string Name { get; set; }
		}
		public int Id { get; set; }
		public int FormId { get; set; }
		public string FormName { get; set; }
		public string CheckPointName { get; set; }
		public IEnumerable<Argument> Arguments { get; set; }

	}
}
