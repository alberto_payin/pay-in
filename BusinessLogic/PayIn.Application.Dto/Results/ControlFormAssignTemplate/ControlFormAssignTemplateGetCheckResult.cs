﻿using PayIn.Common;
using System;
using System.Collections.Generic;

namespace PayIn.Application.Dto.Results.ControlFormAssignTemplate
{
	public partial class ControlFormAssignTemplateGetCheckResult
	{
		public class Argument
		{
			public int Id { get; set; }
			public string Name { get; set; }
		}
		public int Id { get; set; }
		public int FormId { get; set; }
		public string FormName { get; set; }
		public IEnumerable<Argument> Arguments { get; set; }

	}
}
