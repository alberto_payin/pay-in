﻿using PayIn.Common;
using System;
using System.Collections.Generic;

namespace PayIn.Application.Dto.Results.ControlFormAssignTemplate
{
	public partial class ControlFormAssignTemplateGetResult
	{
		public class Argument
		{
			public int Id { get; set; }
			public string Name { get; set; }
			public string Observations { get; set; }
			public ControlFormArgumentType Type { get; set; }
			public ControlFormArgumentTarget Target { get; set; }
			public bool IsRequired { get; set; }
		}

		public int Id { get; set; }
		public IEnumerable<Argument> Arguments { get; set; }
	}
}
