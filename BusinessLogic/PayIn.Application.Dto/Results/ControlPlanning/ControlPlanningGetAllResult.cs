﻿using PayIn.Common;
using PayIn.Domain.Public;
using System;
using System.Collections.Generic;
using Xp.Common;

namespace PayIn.Application.Dto.Results
{
	public partial class ControlPlanningGetAllResult
	{
		public class Item : CalendarItemResult
		{
			public int       Id                { get; set; }
			public bool      CheckTimetable    { get; set; }
			public CheckType EntranceCheckType { get; set; }
			public CheckType ExitCheckType     { get; set; }
			public int       WorkerId          { get; set; }
			public string    WorkerName        { get; set; }		

		}


		public int               Id            { get; set; }
		public XpDuration        SumChecks     { get; set; }
		public XpDuration        CheckDuration { get; set; }
		public XpDate            Date          { get; set; }
		public string            ItemName      { get; set; }
		public IEnumerable<Item> Items         { get; set; }

		#region Constructors
		public ControlPlanningGetAllResult()
		{
			Items = new List<Item>();
		}
		#endregion Constructors
	}
}
