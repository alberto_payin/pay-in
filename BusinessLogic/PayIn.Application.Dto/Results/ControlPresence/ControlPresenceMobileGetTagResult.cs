﻿using PayIn.Common;
using System.Collections.Generic;

namespace PayIn.Application.Dto.Results.ControlPresence
{
	public partial class ControlPresenceMobileGetTagResult
	{
		public partial class Item
		{
			public int           Id                    { get; set; }
			public PresenceType? PresenceType          { get; set; }
			public string        Name                  { get; set; }
			public string        Observations          { get; set; }
			public bool          SaveTrack             { get; set; }
			public bool          SaveFacialRecognition { get; set; }

		}

		public int               Id        { get; set; }
		public string            Reference { get; set; }
		public IEnumerable<Item> Items     { get; set; }
	}
}
