﻿using PayIn.Common;
using PayIn.Domain.Public;
using System.Collections.Generic;
using Xp.Application;
using Xp.Application.Dto;
using Xp.Common;

namespace PayIn.Application.Dto.Results.ControlTrack
{
	public class ControlTrackGetAllResult : ITrace<ControlTrackGetAllResult.Item>
	{
		public class Item : IPosition
		{
			public int        Id           { get; set; }
			public XpDateTime Date         { get; set; }
			public string     Image        { get; set; }
			public decimal?   Latitude     { get; set; }
			public decimal?   Longitude    { get; set; }
			public int        Quality      { get; set; }
			public XpDuration Elapsed      { get; set; }
			public decimal?   Distance     { get; set; }
			public decimal?   Velocity     { get; set; }
			public decimal?   Acceleration { get; set; }
			public int        FormsCount   { get; set; }
			public int?       CheckId      { get; set; }
		}

		public int               Id         { get; set; }
		public int               WorkerId   { get; set; }
		public string            WorkerName { get; set; }
		public int               ItemId     { get; set; }
		public string            ItemName   { get; set; }
		public Item              Since      { get; set; }
		public Item              Until      { get; set; }
		public IEnumerable<Item> Items      { get; set; }
	}
}
