﻿using PayIn.Common;
using System;
using System.Collections.Generic;

namespace PayIn.Application.Dto.Results.ServiceTag
{
	public partial class ServiceTagMobileGetResult
	{
		public class Value
		{
			public int                       Id            { get; set; }
			public string                    Name          { get; set; }
			public string                    Observations  { get; set; }
			public ControlFormArgumentType   Type          { get; set; }
			public ControlFormArgumentTarget Target        { get; set; }
			public bool                      IsRequired    { get; set; }
			public string                    ValueString   { get; set; }
			public decimal?                  ValueNumeric  { get; set; }
			public bool?                     ValueBool     { get; set; }
			public DateTime?                 ValueDateTime { get; set; }
		}
		public class Assign
		{
			public int                Id               { get; set; }
			public string             FormName         { get; set; }
			public string             FormObservations { get; set; }
			public IEnumerable<Value> Values           { get; set; }
		}
		public class Planning
		{
			public int                 Id              { get; set; }
			public DateTime            Date            { get; set; }
			public int                 EllapsedMinutes { get; set; }
			public IEnumerable<Assign> Assigns         { get; set; }
			public int?                CheckId         { get; set; }
			public PresenceType        PresenceType    { get; set; }
		}
		public class Item
		{
			public int                   Id                    { get; set; }
			public PresenceType?         PresenceType          { get; set; }
			public string                Name                  { get; set; }
			public string                Observations          { get; set; }
			public bool                  SaveTrack             { get; set; }
			public bool                  SaveFacialRecognition { get; set; }
			public int                   CheckPointId          { get; set; }
			public IEnumerable<Planning> Plannings             { get; set; }

		}

		public int               Id        { get; set; }
		public string            Reference { get; set; }
		public IEnumerable<Item> Items     { get; set; }
	}
}
