﻿using PayIn.BusinessLogic.Common;
using PayIn.Domain.Internal;
using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Xp.Application.Dto;
using Xp.Common.Dto.Arguments;
using Xp.Domain;

namespace PayIn.Application.Internal.Decorators
{
	public class LogServiceHandlerDecorator<TArguments> : IServiceBaseHandler<TArguments>
		where TArguments : IArgumentsBase
	{
		private readonly IServiceBaseHandler<TArguments> Handler;
		private readonly ISessionData SessionData;
		private readonly IUnitOfWork UnitOfWork;
		private readonly IEntityRepository<Log> Repository;
		private readonly string RelatedClass;
		private readonly string RelatedMethod;

		#region Contructors
		public LogServiceHandlerDecorator(
			IServiceBaseHandler<TArguments> handler,
			ISessionData sessionData,
			IUnitOfWork unitOfWork,
			IEntityRepository<Log> repository,
			string relatedClass,
			string relatedMethod
		)
		{
			if (handler == null) throw new ArgumentNullException("handler");
			if (sessionData == null) throw new ArgumentNullException("sessionData");
			if (unitOfWork == null) throw new ArgumentNullException("unitOfWork");
			if (repository == null) throw new ArgumentNullException("repository");

			Handler = handler;
			SessionData = sessionData;
			UnitOfWork = unitOfWork;
			Repository = repository;
			RelatedClass = relatedClass;
			RelatedMethod = relatedMethod;
		}
		#endregion Contructors

		#region ExecuteAsync
		public async Task<dynamic> ExecuteAsync(TArguments arguments)
		{
			var clock = new Stopwatch();
			clock.Start();
			var result = await Handler.ExecuteAsync(arguments);
			clock.Stop();

			await UnitOfWork.SaveAsync();

			var log = new Log
			{
				DateTime = DateTime.Now,
				Duration = clock.Elapsed,
				Login = SessionData.Login ?? "",
				RelatedClass = RelatedClass,
				RelatedMethod = RelatedMethod,
				RelatedId = 0
			};
			await Repository.AddAsync(log);
			foreach (var arg in arguments.GetType().GetProperties())
			{
				var value = arg.GetValue(arguments);

				if (value != null)
					log.Arguments.Add(new LogArgument
					{
						Log = log,
						Name = arg.Name,
						Value = value.ToString()
					});
			}

			return result;
		}
		#endregion ExecuteAsync
	}
}
