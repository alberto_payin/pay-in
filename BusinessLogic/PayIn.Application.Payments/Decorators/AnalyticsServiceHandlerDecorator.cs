﻿using PayIn.BusinessLogic.Common;
using System;
using System.Threading.Tasks;
using Xp.Application.Dto;
using Xp.Common.Dto.Arguments;
using Xp.Domain;
using Xp.Infrastructure;

namespace PayIn.Application.Payments.Decorators
{
	public class AnalyticsServiceHandlerDecorator<TArguments> : IServiceBaseHandler<TArguments>
		where TArguments : IArgumentsBase
	{
		private readonly IServiceBaseHandler<TArguments> Handler;
		private readonly ISessionData SessionData;
		private readonly IUnitOfWork UnitOfWork;
		private readonly IAnalyticsService Analytics;
		private readonly string RelatedClass;
		private readonly string RelatedMethod;

		#region Contructors
		public AnalyticsServiceHandlerDecorator(
			IServiceBaseHandler<TArguments> handler,
			ISessionData sessionData,
			IUnitOfWork unitOfWork,
			IAnalyticsService analytics,
			string relatedClass,
			string relatedMethod
		)
		{
			if (handler == null) throw new ArgumentNullException("handler");
			if (sessionData == null) throw new ArgumentNullException("sessionData");
			if (unitOfWork == null) throw new ArgumentNullException("unitOfWork");
			if (analytics == null) throw new ArgumentNullException("analytics");

			Handler = handler;
			SessionData = sessionData;
			UnitOfWork = unitOfWork;
			Analytics = analytics;
			RelatedClass = relatedClass;
			RelatedMethod = relatedMethod;
		}
		#endregion Contructors

		#region ExecuteAsync
		public async Task<dynamic> ExecuteAsync(TArguments arguments)
		{
			var result = await Handler.ExecuteAsync(arguments);
			await Analytics.TrackEventAsync(RelatedClass + RelatedMethod);

			return result;
		}
		#endregion ExecuteAsync
	}
}
