﻿using PayIn.Application.Dto.Payments.Arguments.Liquidation;
using PayIn.Common;
using PayIn.Domain.Payments;
using System;
using System.Threading.Tasks;
using Xp.Application.Attributes;
using Xp.Application.Dto;
using Xp.Domain;

namespace PayIn.Application.Payments.Handlers
{
	[XpLog("Liquidation", "ConfirmPay")]
	public class LiquidationConfirmPayHandler  :
		IServiceBaseHandler<LiquidationConfirmPayArguments>
	{
		private readonly IEntityRepository<Liquidation> Repository;

		#region Constructors
		public LiquidationConfirmPayHandler(
			IEntityRepository<Liquidation> repository
		)
		{
			if (repository == null)
				throw new ArgumentNullException("repository");
			Repository = repository;
		}
		#endregion Constructors

		#region ExecuteAsync
		public async Task<dynamic> ExecuteAsync(LiquidationConfirmPayArguments arguments)
		{
			var now = DateTime.Now;

			var liquidation = (await Repository.GetAsync(arguments.Id));

			liquidation.PaymentDate = now.ToUTC();
			liquidation.State = LiquidationState.Payed;

			return liquidation;
		}
		#endregion ExecuteAsync
	}
}
