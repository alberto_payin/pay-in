﻿using PayIn.Application.Dto.Payments.Arguments.Liquidation;
using PayIn.Common;
using PayIn.Domain.Payments;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xp.Application.Attributes;
using Xp.Application.Dto;
using Xp.Domain;

namespace PayIn.Application.Payments.Handlers
{
	[XpLog("Liquidation", "Create")]
	public class LiquidationCreateHandler :
		IServiceBaseHandler<LiquidationCreateArguments>
	{
		private readonly IEntityRepository<Payment> PaymentRepository;

		#region Constructors
		public LiquidationCreateHandler(
			IEntityRepository<Payment> paymentRepository
		)
		{
			if (paymentRepository == null) throw new ArgumentNullException("paymentRepository");
			PaymentRepository = paymentRepository;
		}
		#endregion Constructors

		#region ExecuteAsync
		async Task<dynamic> IServiceBaseHandler<LiquidationCreateArguments>.ExecuteAsync(LiquidationCreateArguments arguments)
		{
			var now = DateTime.Now;

			var liquidations = (await PaymentRepository.GetAsync())
				.Where(x => 
					x.Liquidation == null &&
					x.State == PaymentState.Active
				)
				.GroupBy(x => x.Ticket.Concession)
				.Select(x => new
				{
					LastLiquidationDate =
						x.Key.Liquidations.Max(y => (DateTime?)y.Until) ??  // Fecha de cierre de última liquidation
						x.Min(y => y.Date), // Primer pago
					Concession = x.Key,
					Total = x.Sum(y => y.Amount),
					Payin = x.Sum(y => y.Payin),
					Payments = x
				})
				.ToList();

			foreach (var values in liquidations)
			{
				var liquidation = new Liquidation
				{
					State = LiquidationState.Closed,
					Until = now.ToUTC(),
					Since = values.LastLiquidationDate,
					TotalQuantity = values.Total,
					PayinQuantity = values.Payin,
					PaidQuantity = values.Total - values.Payin,
					PaymentDate = null,
					RequestDate = values.Concession.LiquidationRequestDate,
					Concession = values.Concession,
					Payments = values.Payments.ToList()
				};

				values.Concession.LiquidationRequestDate = null;
				values.Concession.Liquidations.Add(liquidation);
			}

			return null;
		}
		#endregion ExecuteAsync
	}
}
