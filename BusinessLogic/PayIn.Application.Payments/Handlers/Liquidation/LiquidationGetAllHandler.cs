﻿using PayIn.Application.Dto.Payments.Arguments.Liquidation;
using PayIn.Application.Dto.Payments.Results.Liquidation;
using PayIn.BusinessLogic.Common;
using PayIn.Common;
using PayIn.Domain.Payments;
using PayIn.Domain.Security;
using System;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Threading.Tasks;
using Xp.Application.Dto;
using Xp.Domain;

namespace PayIn.Application.Payments
{
	public class LiquidationGetAllHandler :
		IQueryBaseHandler<LiquidationGetAllArguments, LiquidationGetAllResult>
	{
		private readonly ISessionData SessionData;
		private readonly IEntityRepository<Liquidation> Repository;
		private readonly IEntityRepository<PaymentConcession> ConcessionRepository;

		#region Constructors
		public LiquidationGetAllHandler(
			ISessionData sessionData,
			IEntityRepository<Liquidation> repository,
			IEntityRepository<PaymentConcession> concessionRepository
		)
		{
			if (sessionData == null) throw new ArgumentNullException("sessionData");
			if (repository == null) throw new ArgumentNullException("repository");
			if (concessionRepository == null) throw new ArgumentNullException("concessionRepository");

			SessionData = sessionData;
			Repository = repository;
			ConcessionRepository = concessionRepository;
		}
		#endregion Constructors

		#region ExecuteAsync
		public async Task<ResultBase<LiquidationGetAllResult>> ExecuteAsync(LiquidationGetAllArguments arguments)
		{
			var result = (await Repository.GetAsync())
				.Where(x =>
					x.Until >= arguments.Since &&
					x.Since <= arguments.Until &&
					x.Concession.Concession.Supplier.Login == SessionData.Login ||
					SessionData.Roles.Contains(AccountRoles.Superadministrator)
				)
				.Select(x => new
				{
					Id = x.Id,
					State = x.State,
					Amount = x.TotalQuantity,
					Payin = x.PayinQuantity,
					Total = x.PaidQuantity,
					Since = x.Since,
					Until = x.Until,
					PaymentDate = x.PaymentDate,
					RequestDate = x.RequestDate,							
					PaymentsCount = x.Payments.Count()
				})
				.OrderByDescending(x => x.Since)
				.ToList()
				.Select(x => new LiquidationGetAllResult
				{
					Id = x.Id,
					State = x.State,
					Amount = x.Amount,
					Payin = x.Payin,
					Total = x.Total,
					Since = x.Since,
					Until = x.Until,
					PaymentDate = x.PaymentDate,
					RequestDate = x.RequestDate,
					PaymentsCount = x.PaymentsCount
				})
				.ToList();

			var summarize = (await ConcessionRepository.GetAsync())
				.Where(x => x.Concession.Supplier.Login == SessionData.Login)
				.Select(x => new
				{
					Amount = x.Tickets
						.Sum(y => (decimal?)y.Payments
							.Where(z => 
								z.LiquidationId == null &&
								z.State == PaymentState.Active
							)
							.Sum(z => (decimal?)z.Amount)) ?? 0,
					Payin = x.Tickets
						.Sum(y => (decimal?)y.Payments
							.Where(z => 
								z.LiquidationId == null &&
								z.State == PaymentState.Active
							)
							.Sum(z => (decimal?)z.Payin)) ?? 0,
					PaymentsCount = x.Tickets
						.Sum(y => (int?) y.Payments
							.Where(z =>
								z.LiquidationId == null &&
								z.State == PaymentState.Active
							)
							.Count()) ?? 0,
					LiquidationSince = 
						x.Liquidations
							.Max(y => (DateTime?) y.Until) ??
						x.Tickets
							.Min(y => y.Payments
								.Min(z => (DateTime?)z.Date)
							),
					LiquidationRequestDate = x.Liquidations
						.Min(y => y.RequestDate),
					DateExists = x.LiquidationRequestDate
				})
				.ToList();
						 
			return new LiquidationGetAllResultBase
			{
				Amount = summarize.Sum(x => x.Amount),
				Payin = summarize.Sum(x => x.Payin),
				Total = summarize.Sum(x => x.Amount) - summarize.Sum(x => x.Payin),
				LiquidationSince = summarize.Min(x => x.LiquidationSince),
				LiquidationRequestDate = summarize.Min(x => x.LiquidationRequestDate),
				DateExists = summarize.FirstOrDefault().DateExists == null ? false : true,
				PaymentsCount = summarize.Sum(x => x.PaymentsCount),
				AmountSum = result.Sum(x => x.Amount),
				PayinSum = result.Sum(x => x.Payin),
				TotalSum = result.Sum(x => x.Total),
				Data = result
			};
		}
		#endregion ExecuteAsync
	}
}
