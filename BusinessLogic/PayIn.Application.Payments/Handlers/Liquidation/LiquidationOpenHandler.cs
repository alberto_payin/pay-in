﻿using PayIn.Application.Dto.Payments.Arguments.Liquidation;
using PayIn.BusinessLogic.Common;
using PayIn.Domain.Payments;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xp.Application.Attributes;
using Xp.Application.Dto;
using Xp.Domain;

namespace PayIn.Application.Payments.Handlers
{
	[XpLog("Liquidation", "Open")]
	public class LiquidationOpenHandler :
		IServiceBaseHandler<LiquidationOpenArguments>
	{
		private readonly IEntityRepository<PaymentConcession> ConcessionRepository;
		private readonly ISessionData SessionData;

		#region Constructors
		public LiquidationOpenHandler(
			ISessionData sessionData,
			IEntityRepository<PaymentConcession> concessionRepository
		)
		{
			if (sessionData == null) throw new ArgumentNullException("sessionData");
			SessionData = sessionData;

			if (concessionRepository == null)
				throw new ArgumentNullException("concessionRepository");
			ConcessionRepository = concessionRepository;
		}
		#endregion Constructors

		#region ExecuteAsync
		public async Task<dynamic> ExecuteAsync(LiquidationOpenArguments arguments)
		{
			var concession = (await ConcessionRepository.GetAsync())
				.Where(x => x.Concession.Supplier.Login == SessionData.Login)
				.FirstOrDefault();

			concession.LiquidationRequestDate = null;

			return concession;
		}
		#endregion ExecuteAsync
	}
}
