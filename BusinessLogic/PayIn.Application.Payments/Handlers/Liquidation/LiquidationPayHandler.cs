﻿using PayIn.Application.Dto.Payments.Arguments.Liquidation;
using PayIn.Application.Dto.Payments.Results.Liquidation;
using PayIn.BusinessLogic.Common;
using PayIn.Common;
using PayIn.Domain.Payments;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xp.Application.Attributes;
using Xp.Application.Dto;
using Xp.Domain;

namespace PayIn.Application.Payments.Handlers
{
	[XpLog("Liquidation", "Pay")]
	public class LiquidationPayHandler :
		IQueryBaseHandler<LiquidationPayArguments, LiquidationPayResult>
	{
		private readonly IEntityRepository<Liquidation> _Repository;
		private readonly IEntityRepository<PaymentConcession> ConcessionRepository;
		private readonly ISessionData SessionData;

		#region Constructors
		public LiquidationPayHandler(
			ISessionData sessionData,
			IEntityRepository<Liquidation> repository,
			IEntityRepository<PaymentConcession> concessionRepository
		)
		{
			if (sessionData == null) throw new ArgumentNullException("sessionData");
			SessionData = sessionData;

			if (repository == null)
				throw new ArgumentNullException("repository");
			_Repository = repository;

			if (concessionRepository == null)
			throw new ArgumentNullException("concessionRepository");
			ConcessionRepository = concessionRepository;
		}
		#endregion Constructors

		#region ExecuteAsync
		public async Task<ResultBase<LiquidationPayResult>> ExecuteAsync(LiquidationPayArguments arguments)
		{
			if (arguments.Since.Value > arguments.Until.Value)
				return new ResultBase<LiquidationPayResult>();

			var since = arguments.Since.Value.ToUTC();
			var until = arguments.Until.AddDays(1).Value.ToUTC();

			var result = (await _Repository.GetAsync())
				.Where(x =>
					x.Since >= since && 
					x.Until < until &&
					(x.State == LiquidationState.Closed || x.State == LiquidationState.Payed)
				)
				.Select(x => new
				{
					Id = x.Id,
					State = x.State,
					PaidQuantity = x.PaidQuantity,
					Since = x.Since,
					Until = x.Until,
					PaymentDate = x.PaymentDate	,
					Cif = x.Payments.Select(y => y.Ticket.Concession.Concession.Supplier.TaxNumber),
					ConcessionName = x.Payments.Select(y => y.Ticket.Concession.Concession.Supplier.TaxName),
					AccountNumber = x.Payments.Select(y => y.Ticket.Concession.BankAccountNumber),
					PaymentsCount = x.Payments.Count()
				})
				.ToList()
				.Select(x => new LiquidationPayResult
				{
					Id = x.Id,
					State = x.State,
					PaidQuantity = x.PaidQuantity,
					LiquidationSince = x.Since,
					LiquidationUntil = x.Until,
					PaymentDate = x.PaymentDate,
					Cif = x.Cif.FirstOrDefault(),
					ConcessionName = x.ConcessionName.FirstOrDefault(),
					AccountNumber = x.AccountNumber.FirstOrDefault(),
					PaymentsCount = x.PaymentsCount
				})
				.OrderByDescending(x => x.LiquidationSince);

			return new ResultBase<LiquidationPayResult> {  Data = result };
		}
		#endregion ExecuteAsync
	}
}
