﻿using PayIn.Application.Dto.Payments.Arguments.Payments;
using PayIn.Application.Dto.Payments.Results.Payments;
using PayIn.BusinessLogic.Common;
using PayIn.Common;
using PayIn.Domain.Payments;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xp.Application.Dto;
using Xp.Domain;

namespace PayIn.Application.Payments.Handlers
{
	public class PaymentGetAllByLiquidationHandler :
		IQueryBaseHandler<PaymentGetAllByLiquidationArguments, PaymentsGetAllByLiquidationResult>
	{
		private readonly IEntityRepository<Payment> Repository;
		private readonly ISessionData SessionData;
		private readonly IEntityRepository<Liquidation> LiquidationRepository;

		#region Constructors
		public PaymentGetAllByLiquidationHandler(
			ISessionData sessionData,
			IEntityRepository<Payment> repository,
			IEntityRepository<Liquidation> liquidationRepository
		)
		{
			if (sessionData == null) throw new ArgumentNullException("sessionData");
			SessionData = sessionData;

			if (repository == null)
				throw new ArgumentNullException("repository");
			Repository = repository;


			if (liquidationRepository == null)
				throw new ArgumentNullException("liquidationRepository");
			LiquidationRepository = liquidationRepository;
		}
		#endregion Constructors

		#region ExecuteAsync
		public async Task<ResultBase<PaymentsGetAllByLiquidationResult>> ExecuteAsync(PaymentGetAllByLiquidationArguments arguments)
		{
			var items = (await Repository.GetAsync())
				.Where(x =>
					x.LiquidationId == arguments.LiquidationId &&
					x.State == PaymentState.Active
				);
			if (!arguments.Filter.IsNullOrEmpty())
				items = items
					.Where(x => (
						x.TaxName.Contains(arguments.Filter)
					));

			var result = items
				.Select(x => new
				{
					Id = x.Id,
					Amount = x.Amount,
					Payin = x.Payin,
					TicketAmount = x.Ticket.Amount,
					TaxName = x.TaxName,
					TicketId = x.TicketId,
					Date = x.Date,
					State = (x.RefundFromId != null && x.State == PaymentState.Active) ? PaymentState.Returned : x.State
				})
				.OrderBy(x => x.Date)
				.ToList()
				.Select(x => new PaymentsGetAllByLiquidationResult
				{
					Id = x.Id,
					TaxName = x.TaxName,
					Amount = x.Amount,
					Payin = x.Payin,
					Total = x.Amount - x.Payin,
					TicketAmount = x.TicketAmount,
					TicketId = x.TicketId,
					Date = x.Date,
					State = x.State
				});

			var liquidation = (await LiquidationRepository.GetAsync())
				.Where(x => x.Id == arguments.LiquidationId)
				.Select(x => new
				{
					Since = x.Since,
					Until = x.Until
				})
				.FirstOrDefault();

			return new PaymentsGetAllByLiquidationResultBase
			{
				LiquidationSince = liquidation == null ? null : (DateTime?)liquidation.Since,
				LiquidationUntil = liquidation == null ? null : (DateTime?)liquidation.Until,
				Data = result
			};
		}
		#endregion ExecuteAsync
	}
}

