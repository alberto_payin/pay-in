﻿using PayIn.Application.Dto.Payments.Arguments.Payments;
using PayIn.Application.Dto.Payments.Results.Payments;
using PayIn.BusinessLogic.Common;
using PayIn.Common;
using PayIn.Domain.Payments;
using PayIn.Domain.Security;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xp.Application.Dto;
using Xp.Domain;

namespace PayIn.Application.Payments.Handlers
{
	public class PaymentsGetAllHandler :
		IQueryBaseHandler<PaymentGetAllArguments, PaymentsGetAllResult>
	{
		private readonly IEntityRepository<Payment> Repository;
		private readonly ISessionData SessionData;

		#region Constructors
		public PaymentsGetAllHandler(
			ISessionData sessionData,
			IEntityRepository<Payment> repository
		)
		{
			if (sessionData == null) throw new ArgumentNullException("sessionData");
			SessionData = sessionData;

			if (repository == null)
				throw new ArgumentNullException("repository");
			Repository = repository;
		}
		#endregion Constructors

		#region ExecuteAsync
		public async Task<ResultBase<PaymentsGetAllResult>> ExecuteAsync(PaymentGetAllArguments arguments)
		{
			if (arguments.Since.Value > arguments.Until.Value)
				return new ResultBase<PaymentsGetAllResult>();

			var until = arguments.Until.AddDays(1);
			var items = (await Repository.GetAsync())
				.Where(x=>
					x.Date >= arguments.Since && 
					x.Date < until &&
					(x.UserLogin == SessionData.Login || SessionData.Roles.Contains(AccountRoles.Superadministrator))
				);
			
			if (!arguments.Filter.IsNullOrEmpty())
				items = items.Where(x => (
					x.Ticket.Reference.Contains(arguments.Filter) ||
					x.TaxName.Contains(arguments.Filter)
				));
				
			var result = items
				.Select(x => new
				{
					Id = x.Id,
					Ticket = x.Ticket.Reference,
					Amount = x.Amount,
					Payin = x.Payin,
					TicketAmount = x.Ticket.Amount,
					TaxName = x.TaxName,
					TicketId = x.TicketId,
					Date = x.Date,
					State = (x.RefundFromId != null && x.State == PaymentState.Active) ? PaymentState.Returned : x.State,
					RefundFromId = x.RefundFromId,
					RefundFromDate = (x.RefundFrom != null) ? (DateTime?)x.RefundFrom.Date : null,
					RefundToId = (x.RefundTo.Count() != 0) ? (int?)x.RefundTo.FirstOrDefault().Id : null,
					RefundToDate = (x.RefundTo.Count() != 0) ? (DateTime?)x.RefundTo.FirstOrDefault().Date : null
				})
				.OrderByDescending(x => x.Date)
				.ToList()
				.Select(x => new PaymentsGetAllResult
				{
					Id = x.Id,
					Ticket = x.Ticket,
					TaxName = x.TaxName,
					Amount = x.Amount,
					Payin = x.Payin,
					Total = x.Amount - x.Payin,
					TicketAmount = x.TicketAmount,
					TicketId = x.TicketId,
					Date = x.Date,
					State = x.State,
					RefundFromId = x.RefundFromId,
					RefundToId = x.RefundToId,
					RefundToDate = x.RefundToDate,
					RefundFromDate = x.RefundFromDate
				});

			return new ResultBase<PaymentsGetAllResult> { Data = result };
		}
		#endregion ExecuteAsync
	}
}
