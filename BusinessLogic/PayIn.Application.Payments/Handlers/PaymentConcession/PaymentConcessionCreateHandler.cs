﻿using PayIn.Application.Dto.Payments.Arguments.PaymentConcession;
using PayIn.BusinessLogic.Common;
using PayIn.Common;
using PayIn.Common.Resources;
using PayIn.Common.Security;
using PayIn.Domain.Payments;
using PayIn.Domain.Payments.Infrastructure;
using PayIn.Domain.Public;
using PayIn.Domain.Security;
using PayIn.Infrastructure.Security;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xp.Application.Attributes;
using Xp.Application.Dto;
using Xp.Domain;
using Xp.Infrastructure.Repositories;

namespace PayIn.Application.Payments.Handlers
{
	[XpLog("PaymentConcession", "Create")]
	[XpAnalytics("PaymentConcession", "Create")]
	public class PaymentConcessionCreateHandler : IServiceBaseHandler<PaymentConcessionCreateArguments>
	{
		private ISessionData SessionData { get; set; }
		private IEntityRepository<ServiceSupplier> RepositoryServiceSupplier { get; set; }
		private IEntityRepository<PaymentConcession> Repository { get; set; }
		private IUnitOfWork UnitOfWork;
		private IInternalService InternalService;
			
		#region Construtors
		public PaymentConcessionCreateHandler
		(
			ISessionData sessionData, 
			IEntityRepository<ServiceSupplier> repositoryServiceSupplier,
			IEntityRepository<PaymentConcession>repository,
			IUnitOfWork unitOfWork,
			IInternalService internalService
		)
		{
			if (sessionData == null) throw new ArgumentNullException("sessionData");
			if (repositoryServiceSupplier == null) throw new ArgumentNullException("repositoryServiceSupplier");
			if (repository == null) throw new ArgumentNullException("repository");
			if (unitOfWork == null) throw new ArgumentNullException("unitOfWork");
			if (internalService == null) throw new ArgumentNullException("internalService");

			SessionData = sessionData;
			RepositoryServiceSupplier = repositoryServiceSupplier;
			Repository = repository;
			UnitOfWork = unitOfWork;
			InternalService = internalService;
		}
		#endregion Constructors

		#region ExecuteAsync
		public async Task<dynamic> ExecuteAsync(PaymentConcessionCreateArguments arguments)
		{
			if (!arguments.AcceptTerms)
				throw new ApplicationException(string.Format(SecurityResources.AcceptTermsException));

			var now = DateTime.Now;
			var securityRepository = new SecurityRepository();

			var supplier = (await RepositoryServiceSupplier.GetAsync("Concessions"))
				.Where(x => x.Login == SessionData.Login)
				.FirstOrDefault();

			if (supplier == null)
			{
				supplier = new ServiceSupplier
				{					
					Login = SessionData.Login,   
					Name = arguments.SupplierName,     
					TaxName = arguments.TaxName,
					TaxNumber = arguments.TaxNumber,
					TaxAddress = arguments.TaxAddress					
				};
				await RepositoryServiceSupplier.AddAsync(supplier);
			}

			var serviceConcession = new ServiceConcession
			{
				Name = arguments.SupplierName,
				Type =  ServiceType.Charge,
				State = Common.ConcessionState.Pending
			};
			supplier.Concessions.Add(serviceConcession);
			
			//Space Remove from BankAccountNumber
			var bankAccountNumberCleaned = arguments.BankAccountNumber.Replace(" ","");

			var paymentConcession = new PaymentConcession
			{
				Observations = arguments.Observations ?? "",
				Phone = arguments.Phone,
				BankAccountNumber = bankAccountNumberCleaned,
				Concession = serviceConcession,
				FormUrl = "",
				Address = arguments.Address,
				CreateConcessionDate = now.ToUTC()
			};
			await Repository.AddAsync(paymentConcession);
			await securityRepository.AddRole(paymentConcession.Concession.Supplier.Login, AccountRoles.CommercePayment);

			if (!(await InternalService.UserHasPaymentAsync()))
				await InternalService.UserCreateAsync(arguments.Pin);

			await UnitOfWork.SaveAsync();
	
			//Save file in Azure
			var repositoryAzure = new AzureBlobRepository();			
			var guid = Guid.NewGuid();

#if TEST || DEBUG || EMULATOR
			paymentConcession.FormUrl = PaymentConcessionResources.FileUrlTest.FormatString(paymentConcession.Id, guid);
#else // TEST
			paymentConcession.FormUrl = PaymentConcessionResources.FileUrl.FormatString(paymentConcession.Id, guid);
#endif // TEST
			repositoryAzure.SaveFile(PaymentConcessionResources.FileShortUrl.FormatString(paymentConcession.Id, guid), arguments.FormA);
			
			return paymentConcession;
		}
		#endregion ExecuteAsync
	}
}
