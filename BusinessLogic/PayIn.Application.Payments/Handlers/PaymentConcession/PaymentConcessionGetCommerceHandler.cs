﻿using PayIn.Application.Dto.Payments.Arguments.PaymentConcession;
using PayIn.Application.Dto.Payments.Results.PaymentConcession;
using PayIn.Domain.Payments;
using PayIn.Domain.Public;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xp.Application.Dto;
using Xp.Domain;

namespace PayIn.Application.Payments.Handlers
{
	public class PaymentConcessionGetCommerceHandler :
		IQueryBaseHandler<PaymentConcessionGetCommerceArguments, PaymentConcessionGetCommerceResult>
	{		
		private readonly IEntityRepository<PaymentConcession> Repository;

		#region Constructors
		public PaymentConcessionGetCommerceHandler(IEntityRepository<PaymentConcession> repository) 
		{
			if (repository == null) throw new ArgumentNullException("repository");
						
			Repository = repository;			
		}
		#endregion Constructors

		#region ExecuteAsync
		public async Task<ResultBase<PaymentConcessionGetCommerceResult>> ExecuteAsync(PaymentConcessionGetCommerceArguments arguments)
		{		
			var result = (await Repository.GetAsync())
				.Where(x => x.ConcessionId == arguments.Id)
				.Select(x => new PaymentConcessionGetCommerceResult
				{
					Id = x.Id,
					Name = x.Concession.Name,
					FormUrl = x.FormUrl,
					AccountNumber = x.BankAccountNumber,
					Observations = x.Observations,
					PayinCommission = x.PayinCommision,
					LiquidationAmountMin = x.LiquidationAmountMin,
     				Phone = x.Phone,
					State = x.Concession.State
				});
		return new ResultBase<PaymentConcessionGetCommerceResult> { Data = result };
		}
		#endregion ExecuteAsync
	}
}
