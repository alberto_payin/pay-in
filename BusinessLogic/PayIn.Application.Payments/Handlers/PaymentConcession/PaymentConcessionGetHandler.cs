﻿using PayIn.Application.Dto.Payments.Arguments.PaymentConcession;
using PayIn.Application.Dto.Payments.Results.PaymentConcession;
using PayIn.Common;
using PayIn.Domain.Payments;
using PayIn.Domain.Public;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xp.Application.Dto;
using Xp.Domain;

namespace PayIn.Application.Public.Handlers
{
	public class PaymentConcessionGetHandler :
		IQueryBaseHandler<PaymentConcessionGetArguments, PaymentConcessionGetResult>
	{
		private readonly IEntityRepository<ServiceConcession> Repository;
		private readonly IEntityRepository<PaymentConcession> PaymentRepository;


		#region Constructors
		public PaymentConcessionGetHandler(IEntityRepository<ServiceConcession> repository, IEntityRepository<PaymentConcession> paymentRepository)
		{
			if (repository == null) throw new ArgumentNullException("repository");
			Repository = repository;

			if (paymentRepository == null) throw new ArgumentNullException("paymentRepository");
			PaymentRepository = paymentRepository;
		}
		#endregion Constructors

		#region ExecuteAsync
		public async Task<ResultBase<PaymentConcessionGetResult>> ExecuteAsync(PaymentConcessionGetArguments arguments)
		{
			var paymentConcesion = (await PaymentRepository.GetAsync())
				.Where(x => x.ConcessionId == (arguments.Id))
				.FirstOrDefault();			

			var items = await Repository.GetAsync();
			var result = items
				.Where(x => x.Id.Equals(arguments.Id))
				.Select(x => new PaymentConcessionGetResult
				{
					Id = x.Id,
					Name = x.Name,
					State = x.State,
					Type = x.Type,						
					PayinCommission =  paymentConcesion.PayinCommision,
					LiquidationAmountMin = paymentConcesion.LiquidationAmountMin,
					Phone = paymentConcesion.Phone,
					Observations = paymentConcesion.Observations
				});


				return new ResultBase<PaymentConcessionGetResult> { Data = result };
			
		}
		#endregion ExecuteAsync
	}
}
