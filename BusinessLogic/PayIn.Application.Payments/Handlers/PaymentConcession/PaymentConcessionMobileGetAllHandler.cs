﻿using PayIn.Application.Dto.Payments.Arguments.PaymentConcession;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PayIn.Application.Payments.Handlers;
using Xp.Domain;
using PayIn.Domain.Payments;
using Xp.Application.Dto;
using PayIn.Application.Dto.Payments.Results.PaymentConcession;
using PayIn.BusinessLogic.Common;
using PayIn.Common;

namespace PayIn.Application.Payments.Handlers
{
	public class PaymentConcessionMobileGetAllHandler :
		IQueryBaseHandler<PaymentConcessionMobileGetAllArguments, PaymentConcessionMobileGetAllResult>
	{
		private readonly SessionData SessionData;
		private readonly IEntityRepository<PaymentConcession> PaymentsConcessionRepository;

		#region Contructors
		public PaymentConcessionMobileGetAllHandler(
			SessionData sessionData,
			IEntityRepository<PaymentConcession> paymentsConcessionRepository
		)
		{
			if (sessionData == null) throw new ArgumentNullException("sessionData");
			if (paymentsConcessionRepository == null) throw new ArgumentNullException("paymentsConcessionRepository");
			SessionData = sessionData;
			PaymentsConcessionRepository = paymentsConcessionRepository;
		}
		#endregion Contructors

		#region ExecuteAsync
		public async Task<ResultBase<PaymentConcessionMobileGetAllResult>> ExecuteAsync(PaymentConcessionMobileGetAllArguments arguments)

		{
			var items = (await PaymentsConcessionRepository.GetAsync())
				.Where(x => (x.Concession.Supplier.Login == SessionData.Login ||
							 x.PaymentWorkers.Any(y => y.Login == SessionData.Login && 
												       y.State == WorkerState.Active) && 
							 x.Concession.State == Common.ConcessionState.Active));

			var result = items
				.Select(x => new PaymentConcessionMobileGetAllResult
				{
					Id = x.Id,
					Phone = x.Phone,
					Name = x.Concession.Name,
					Address = x.Concession.Supplier.TaxAddress,
					Cif = x.Concession.Supplier.TaxNumber,
				});

			return new ResultBase<PaymentConcessionMobileGetAllResult> { Data = result };
		}
		#endregion ExecuteAsync
	}
}
