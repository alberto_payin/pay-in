﻿using PayIn.Application.Dto.Payments.Arguments.PaymentConcession;
using PayIn.Common;
using PayIn.Domain.Payments;
using PayIn.Domain.Security;
using PayIn.Infrastructure.Security;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xp.Application.Attributes;
using Xp.Application.Dto;
using Xp.Domain;

namespace PayIn.Application.Public.Handlers
{
	[XpLog("PaymentConcession", "Update")]
	public class PaymentConcessionUpdateHandler :
		IServiceBaseHandler<PaymentConcessionUpdateArguments>
	{
		private readonly IEntityRepository<PaymentConcession> Repository;

		#region Constructors
		public PaymentConcessionUpdateHandler
		(
			IEntityRepository<PaymentConcession> repository
		)
		{
			if (repository == null) throw new ArgumentNullException("repository");
			Repository = repository;
		}
		#endregion Constructors

		#region ExecuteAsync
		public async Task<dynamic> ExecuteAsync(PaymentConcessionUpdateArguments arguments)
		{
			var securityRepository = new SecurityRepository();

			var paymentConcessions = (await Repository.GetAsync("Concession.Supplier"));
			var paymentConcession = paymentConcessions
				.Where(x => x.ConcessionId == arguments.Id)
				.FirstOrDefault();

			var activeConcessions = paymentConcessions
				.Where(x => x.Concession.Supplier.Login == paymentConcession.Concession.Supplier.Login && x.Concession.State == ConcessionState.Active)
				.Count();			

			var previousState = paymentConcession.Concession.State;

			if (paymentConcession != null)
			{				
				paymentConcession.PayinCommision = arguments.PayinCommission;
				paymentConcession.LiquidationAmountMin = arguments.LiquidationAmountMin;				
				paymentConcession.Concession.Name = arguments.Name;			
				paymentConcession.Concession.State = arguments.State;
			}
			if (arguments.State == ConcessionState.Active && activeConcessions == 0)
				await securityRepository.AddRole(paymentConcession.Concession.Supplier.Login, AccountRoles.CommercePayment);
			return paymentConcession;
		}
		#endregion ExecuteAsync
	}
}

