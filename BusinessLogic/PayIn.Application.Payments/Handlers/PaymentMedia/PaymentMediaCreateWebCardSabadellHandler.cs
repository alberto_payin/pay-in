﻿using PayIn.Application.Dto.Arguments.Notification;
using PayIn.Application.Dto.Payments.Arguments.PaymentMedia;
using PayIn.Application.Public.Handlers;
using PayIn.Common;
using PayIn.Common.Resources;
using PayIn.Domain.Payments;
using PayIn.Domain.Payments.Infrastructure;
using PayIn.Infrastructure.Sabadell;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xp.Application.Attributes;
using Xp.Application.Dto;
using Xp.Domain;

namespace PayIn.Application.Payments.Handlers
{
	[XpLog("PaymentMedia", "CreateWebCardSabadell")]
	public class PaymentMediaCreateWebCardSabadellHandler :
		IServiceBaseHandler<PaymentMediaCreateWebCardSabadellArguments>
	{
		public readonly IUnitOfWork UnitOfWork;
		public readonly IEntityRepository<PaymentMedia> Repository;
		public readonly IEntityRepository<Payment> PaymentRepository;
		public readonly IInternalService InternalService;
		private readonly ServiceNotificationCreateHandler ServiceNotificationCreate;

		#region Contructors
		public PaymentMediaCreateWebCardSabadellHandler(
			IUnitOfWork unitOfWork,
			IEntityRepository<PaymentMedia> repository,
			IEntityRepository<Payment> paymentRepository,
			IInternalService internalService,
			ServiceNotificationCreateHandler serviceNotificationCreate
		)
		{
			if (unitOfWork == null) throw new ArgumentNullException("unitOfWork");
			if (repository == null) throw new ArgumentNullException("repository");
			if (paymentRepository == null) throw new ArgumentNullException("paymentRepository");
			if (internalService == null) throw new ArgumentNullException("internalService");
			if (serviceNotificationCreate == null) throw new ArgumentNullException("serviceNotificationCreate");

			UnitOfWork = unitOfWork;
			Repository = repository;
			PaymentRepository = paymentRepository;
			InternalService = internalService;
			ServiceNotificationCreate = serviceNotificationCreate;
		}
		#endregion Contructors

		#region ExecuteAsync
		public async Task<dynamic> ExecuteAsync(PaymentMediaCreateWebCardSabadellArguments arguments)
		{
			var now = DateTime.Now;

			var internalArguments = SabadellGatewayFunctions.GetWebPaymentResponse(arguments.Datos);
			await InternalService.PaymentMediaCreateWebCardSabadellAsync(internalArguments);

			// Actualizar datos pago
			var payment = (await PaymentRepository.GetAsync("Ticket"))
				.Where(x =>
					x.Id == internalArguments.PublicPaymentId &&
					x.State == PaymentState.Pending
				)
				.FirstOrDefault();
			payment.AuthorizationCode = internalArguments.AuthorizationCode;

			// Actualización datos tarjeta
			var paymentMedia = (await Repository.GetAsync())
				.Where(x =>
					x.Id == internalArguments.PublicPaymentMediaId &&
					x.State == PaymentMediaState.Pending
				)
				.FirstOrDefault();
			paymentMedia.NumberHash = "{0} {1} {2} {3}".FormatString(
				internalArguments.CardNumberHash.Substring(0, 4),
				internalArguments.CardNumberHash.Substring(4, 4),
				internalArguments.CardNumberHash.Substring(8, 4),
				internalArguments.CardNumberHash.Substring(12)
			);
			paymentMedia.ExpirationMonth = internalArguments.ExpirationMonth;
			paymentMedia.ExpirationYear = internalArguments.ExpirationYear;

			if (internalArguments.IsError)
			{
				payment.ErrorCode = internalArguments.ErrorCode;
				payment.ErrorText = internalArguments.ErrorText;
				payment.ErrorPublic = internalArguments.ErrorPublic;

				payment.State = PaymentState.Error;
				paymentMedia.State = PaymentMediaState.Error;

				await ServiceNotificationCreate.ExecuteAsync(new ServiceNotificationCreateArguments(
					type: NotificationType.PaymentError,
					message: PaymentResources.PaymentMediaCreationPaymentErrorPushMessage.FormatString(
						payment.Amount,
						ServiceNotificationResources.GatewayError.FormatString(
							internalArguments.ErrorCode,
							internalArguments.ErrorText
						)
					),
					referenceId: payment.TicketId,
					referenceClass: "Ticket",
					senderLogin: "info@pay-in.es",
					receiverLogin: payment.UserLogin
				));
				await ServiceNotificationCreate.ExecuteAsync(new ServiceNotificationCreateArguments(
					type: NotificationType.PaymentMediaCreateError,
					message: PaymentMediaResources.PaymentMediaCreateExceptionPayment,
					referenceId: paymentMedia.Id,
					referenceClass: "PaymentMedia",
					senderLogin: "info@pay-in.es",
					receiverLogin: payment.UserLogin
				));
				await UnitOfWork.SaveAsync();

				return null;
			}
			else
			{
				payment.State = PaymentState.Active;

				await ServiceNotificationCreate.ExecuteAsync(new ServiceNotificationCreateArguments(
					type: NotificationType.PaymentSucceed,
					message: PaymentResources.PaymentMediaCreationPaymentPushMessage.FormatString(
						payment.Amount
					),
					referenceId: payment.TicketId,
					referenceClass: "Ticket",
					senderLogin: "info@pay-in.es",
					receiverLogin: payment.UserLogin
				));
			}

			// Crear devolución
			var refund = new Payment
			{
				AuthorizationCode = "",
				Amount = - payment.Amount,
				Date = now.ToUTC(),
				Order = payment.Order,
				Payin = 0,
				PaymentMedia = paymentMedia,
				ErrorCode = "",
				ErrorText = "",
				ErrorPublic = "",
				State = PaymentState.Pending,
				TaxAddress = payment.TaxAddress,
				TaxName = payment.TaxName,
				TaxNumber = payment.TaxNumber,
				Ticket = payment.Ticket,
				UserLogin = payment.UserLogin,
				UserName = payment.UserName,
				RefundFrom = payment
			};
			await PaymentRepository.AddAsync(refund);
			await UnitOfWork.SaveAsync();

			// Ejecutar devolución
			var refundResponse = await InternalService.PaymentMediaCreateWebCardRefundSabadellAsync(
				publicPaymentMediaId: internalArguments.PublicPaymentMediaId,
				publicTicketId: internalArguments.PublicTicketId,
				publicPaymentId: refund.Id,
				amount: internalArguments.Amount,
				currency: internalArguments.Currency,
				orderId: internalArguments.OrderId,
				terminal: internalArguments.Terminal
			);

			// Actualizar datos devolución
			refund.AuthorizationCode = refundResponse.AuthorizationCode;
			refund.Order = refundResponse.OrderId;
			if (refundResponse.IsError)
			{
				refund.ErrorCode = refundResponse.ErrorCode;
				refund.ErrorText = refundResponse.ErrorText;

				refund.State = PaymentState.Error;
				paymentMedia.State = PaymentMediaState.Error;

				await ServiceNotificationCreate.ExecuteAsync(new ServiceNotificationCreateArguments(
					type: NotificationType.RefundError,
					message: PaymentResources.PaymentMediaCreationRefundErrorPushMessage.FormatString(
						payment.Amount,
						ServiceNotificationResources.GatewayError.FormatString(
							refundResponse.ErrorCode,
							refundResponse.ErrorText
						)
					),
					referenceId: refund.TicketId,
					referenceClass: "Ticket",
					senderLogin: "info@pay-in.es",
					receiverLogin: refund.UserLogin
				));
				await ServiceNotificationCreate.ExecuteAsync(new ServiceNotificationCreateArguments(
					type: NotificationType.PaymentMediaCreateError,
					message: PaymentMediaResources.PaymentMediaCreateExceptionRefund,
					referenceId: paymentMedia.Id,
					referenceClass: "PaymentMedia",
					senderLogin: "info@pay-in.es",
					receiverLogin: payment.UserLogin
				));
			}
			else
			{
				refund.State = PaymentState.Active;
				paymentMedia.State = PaymentMediaState.Active;

				await ServiceNotificationCreate.ExecuteAsync(new ServiceNotificationCreateArguments(
					type: NotificationType.RefundSucceed,
					message: PaymentResources.PaymentMediaCreationRefundPushMessage.FormatString(
						payment.Amount
					),
					referenceId: refund.TicketId,
					referenceClass: "Ticket",
					senderLogin: "info@pay-in.es",
					receiverLogin: refund.UserLogin
				));
				await ServiceNotificationCreate.ExecuteAsync(new ServiceNotificationCreateArguments(
					type: NotificationType.PaymentMediaCreateSucceed,
					message: PaymentMediaResources.PaymentMediaCreateSuccess,
					referenceId: paymentMedia.Id,
					referenceClass: "PaymentMedia",
					senderLogin: "info@pay-in.es",
					receiverLogin: refund.UserLogin
				));
			}

			return refund;
		}
		#endregion ExecuteAsync
	}
}
