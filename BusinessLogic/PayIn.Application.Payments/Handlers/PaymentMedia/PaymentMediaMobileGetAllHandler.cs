﻿using PayIn.Application.Dto.Payments.Arguments.PaymentMedia;
using PayIn.Application.Dto.Payments.Results.PaymentMedia;
using PayIn.Common;
using PayIn.Domain.Payments;
using PayIn.Domain.Payments.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xp.Application.Dto;
using Xp.Domain;

namespace PayIn.Application.Payments.Handlers
{
	public class PaymentMediaMobileGetAllHandler :
		IQueryBaseHandler<PaymentMediaMobileGetAllArguments, PaymentMediaMobileGetAllResult>
	{
		private readonly IEntityRepository<PaymentMedia> _Repository;
		private readonly IInternalService InternalService;

		#region Constructors
		public PaymentMediaMobileGetAllHandler(
			IEntityRepository<PaymentMedia> repository,
			IInternalService internalService
		)
		{
			if (repository == null) throw new ArgumentNullException("repository");
			if (internalService == null) throw new ArgumentNullException("internalService");

			_Repository = repository;
			InternalService = internalService;
		}
		#endregion Constructors

		#region ExecuteAsync
		public async Task<ResultBase<PaymentMediaMobileGetAllResult>> ExecuteAsync(PaymentMediaMobileGetAllArguments arguments)
		{
			if (!await InternalService.UserHasPaymentAsync())
				throw new ArgumentNullException("user");

			var items = await _Repository.GetAsync();

			var result = items
				.Where(x => 
					x.State != PaymentMediaState.Error &&
					x.State != PaymentMediaState.Delete
				)
				.Select(x => new PaymentMediaMobileGetAllResult
				{
					Id              = x.Id,
					Title           = x.Name,
					Subtitle        = x.Type.ToString(),
					VisualOrder     = x.VisualOrder,
					NumberHash      = x.NumberHash,
					ExpirationMonth = x.ExpirationMonth,
					ExpirationYear  = x.ExpirationYear,
					Type            = x.Type,
					State           = x.State,
					BankEntity      = x.BankEntity
				});

			return new ResultBase<PaymentMediaMobileGetAllResult> { Data = result };
		}
		#endregion ExecuteAsync
	}
}
