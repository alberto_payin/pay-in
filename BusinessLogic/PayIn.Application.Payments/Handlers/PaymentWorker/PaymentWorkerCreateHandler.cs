﻿using PayIn.Application.Dto.Payments.Arguments.PaymentWorker;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xp.Domain;
using PayIn.Common;
using Xp.Application.Dto;
using PayIn.Domain.Payments;
using PayIn.BusinessLogic.Common;
using PayIn.Domain.Public;
using PayIn.Application.Public.Handlers;
using PayIn.Application.Dto.Arguments.Notification;
using PayIn.Common.Resources;

namespace PayIn.Application.Payments.Handlers
{
	public class PaymentWorkerCreateHandler :
		IServiceBaseHandler<PaymentWorkerCreateArguments>
	{
		private readonly IEntityRepository<PaymentWorker> Repository;
		private readonly IEntityRepository<PaymentConcession> ConcessionRepository;
		private readonly ISessionData SessionData;
		private readonly IUnitOfWork UnitOfWork;
		private readonly ServiceNotificationCreateHandler ServiceNotificationCreate;

		#region Constructors
		public PaymentWorkerCreateHandler(
			IEntityRepository<PaymentWorker> repository,
			IEntityRepository<PaymentConcession> concessionRepository,
			ISessionData sessionData,
			IUnitOfWork unitOfWork,
			ServiceNotificationCreateHandler serviceNotificationCreate
		)
		{
			if (repository == null) throw new ArgumentNullException("repository");
			Repository = repository;

			if (concessionRepository == null) throw new ArgumentNullException("concessionRepository");
			ConcessionRepository = concessionRepository;

			if (sessionData == null) throw new ArgumentNullException("sessionData");
			SessionData = sessionData;

			if (unitOfWork == null) throw new ArgumentNullException("unitOfWork");
			UnitOfWork = unitOfWork;

			if (serviceNotificationCreate == null) throw new ArgumentNullException("serviceNotificationCreate");
			ServiceNotificationCreate = serviceNotificationCreate;
		}
		#endregion Constructors

		#region ExecuteAsync
		async Task<dynamic> IServiceBaseHandler<PaymentWorkerCreateArguments>.ExecuteAsync(PaymentWorkerCreateArguments arguments)
		{
			var PaymentConcession = (await ConcessionRepository.GetAsync("Concession.Supplier"))
			.Where(x => x.Concession.Supplier.Login == SessionData.Login)
			.FirstOrDefault();

			if (arguments.Login == SessionData.Login)
				throw new Exception(PaymentWorkerResources.EmailPaymentCommerce);

			var worker = (await Repository.GetAsync())
			.Where(x => x.Login == arguments.Login)
			.FirstOrDefault();

			if (worker != null && (worker.State == WorkerState.Active || worker.State == WorkerState.Pending))
				throw new Exception(ServiceWorkerResources.ExceptionWorkerMailAlreadyExists);

			if (worker != null && (worker.State == WorkerState.Deleted || worker.State == WorkerState.Unsuscribed))
			{
				worker.State = WorkerState.Pending;
				await ServiceNotificationCreate.ExecuteAsync(new ServiceNotificationCreateArguments(
				type: NotificationType.ConcessionVinculation,
				message: PaymentWorkerResources.AcceptAssociationMessage.FormatString(PaymentConcession.Concession.Supplier.Name),
				referenceId: worker.Id,
				referenceClass: "PaymentWorker",
				senderLogin: PaymentConcession.Concession.Supplier.Login,
				receiverLogin: arguments.Login
				));
				return worker;
			}
			var item = new PaymentWorker
			{
				Name = arguments.Name,
				Login = arguments.Login,
				ConcessionId = PaymentConcession.Id,
				Tickets = PaymentConcession.Tickets.ToList(),
				State = WorkerState.Pending
			};
			await Repository.AddAsync(item);
			await UnitOfWork.SaveAsync();

			await ServiceNotificationCreate.ExecuteAsync(new ServiceNotificationCreateArguments(
				type: NotificationType.ConcessionVinculation,
				message: PaymentWorkerResources.AcceptAssociationMessage.FormatString(PaymentConcession.Concession.Supplier.Name),
				referenceId: item.Id, 
				referenceClass: "PaymentWorker",
				senderLogin: PaymentConcession.Concession.Supplier.Login,
				receiverLogin: arguments.Login
			));

			return item;
		}
		#endregion ExecuteAsync
	}
}
