﻿using PayIn.Application.Dto.Payments.Arguments.Ticket;
using PayIn.Common;
using System;
using System.Threading.Tasks;
using Xp.Application.Dto;
using Xp.Domain;

namespace PayIn.Application.Public.Handlers
{
	public class TicketDeleteHandler :
			IServiceBaseHandler<TicketDeleteArguments>
	{
		private readonly IEntityRepository<PayIn.Domain.Payments.Ticket> _Repository;

		#region Constructors
		public TicketDeleteHandler(
			IEntityRepository<PayIn.Domain.Payments.Ticket> repository
			)
		{
			if (repository == null)
				throw new ArgumentNullException("repository");
			_Repository = repository;

		}
		#endregion Constructors

		#region TicketDelete
		async Task<dynamic> IServiceBaseHandler<TicketDeleteArguments>.ExecuteAsync(TicketDeleteArguments arguments)
		{
			var item = await _Repository.GetAsync(arguments.Id);

			item.State = TicketStateType.Cancelled;
			//await _Repository.DeleteAsync(item);

			return null;
		}
		#endregion TicketDelete
	}
}
