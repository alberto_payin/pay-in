﻿using PayIn.Application.Dto.Payments.Arguments.Ticket;
using PayIn.Application.Dto.Payments.Results.Ticket;
using PayIn.BusinessLogic.Common;
using PayIn.Common;
using PayIn.Domain.Payments;
using PayIn.Domain.Security;
using PayIn.Infrastructure.Security;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xp.Application.Dto;
using Xp.Domain;

namespace PayIn.Application.Public.Handlers
{
	public class TicketGetAllHandler :
		IQueryBaseHandler<TicketGetAllArguments, TicketGetAllResult>
	{
		private readonly IEntityRepository<Ticket> _Repository;
		private readonly ISessionData SessionData;

		#region Constructors
		public TicketGetAllHandler(
			ISessionData sessionData,
			IEntityRepository<PayIn.Domain.Payments.Ticket> repository
		)
		{
			if (sessionData == null) throw new ArgumentNullException("sessionData");
			SessionData = sessionData;

			if (repository == null)
				throw new ArgumentNullException("repository");
			_Repository = repository;
		}
		#endregion Constructors

		#region ExecuteAsync
		async Task<ResultBase<TicketGetAllResult>> IQueryBaseHandler<TicketGetAllArguments, TicketGetAllResult>.ExecuteAsync(TicketGetAllArguments arguments)
		{
			if (arguments.Since.Value > arguments.Until.Value)
				return new ResultBase<TicketGetAllResult>();

			var until = arguments.Until.AddDays(1);
			var items = (await _Repository.GetAsync())
				.Where(x =>
					(x.Concession.Concession.Supplier.Login == SessionData.Email ||
			        x.PaymentWorker.Login == SessionData.Login ||
					(x.Payments.Count() == 0 && (x.Concession.PaymentWorkers.Any(y => y.Login == SessionData.Login))) ||
					x.Payments.Any(a => a.State == PaymentState.Pending)) &&
					x.Date >= arguments.Since && 
					x.Date < until &&
					!((x.Payments.Any(z => z.State != PaymentState.Active)) && x.Amount == 0) 
				);
			
			if (!arguments.Filter.IsNullOrEmpty())
				items = items.Where(x => (
					x.Reference.Contains(arguments.Filter) ||
					x.Title.Contains(arguments.Filter)
				));

			var result = items
				.Select(x => new
				{
					Id = x.Id,
					Amount = x.Amount,
					State = x.State,
					Reference = x.Reference,
					Title = x.Title,
					Date = x.Date,
					PayedAmount = x.Payments.Where(y => y.State == PaymentState.Active).Sum(y => (decimal?) y.Amount) ?? 0
				})
				.OrderBy(x => x.Date)
				.ToList()
				.Select(x => new TicketGetAllResult
				{
					Id = x.Id,
					Amount = x.Amount,
					Date = x.Date, // Needs to be calculated in memory
					Reference = x.Reference,
					PayedAmount = x.PayedAmount,
					State = x.State,
					Title = x.Title
				});

			return new ResultBase<TicketGetAllResult> { Data = result };
		}
		#endregion ExecuteAsync
	}
 }
