﻿using PayIn.Application.Dto.Payments.Arguments.Ticket;
using PayIn.Application.Dto.Payments.Results.Ticket;
using PayIn.BusinessLogic.Common;
using PayIn.Common;
using PayIn.Domain.Payments;
using PayIn.Domain.Security;
using PayIn.Infrastructure.Security;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xp.Application.Dto;
using Xp.Domain;

namespace PayIn.Application.Public.Handlers
{
	public class TicketGetDetailsHandler :
		IQueryBaseHandler<TicketGetDetailsArguments, TicketGetDetailsResult>
	{
		private readonly IEntityRepository<Ticket> Repository;
		private readonly ISessionData SessionData;

		#region Constructors
		public TicketGetDetailsHandler(
			ISessionData sessionData,
			IEntityRepository<PayIn.Domain.Payments.Ticket> repository
		)
		{
			if (sessionData == null) throw new ArgumentNullException("sessionData");
			if (repository == null) throw new ArgumentNullException("repository");

			SessionData = sessionData;
			Repository = repository;
		}
		#endregion Constructors

		#region ExecuteAsync
		public async Task<ResultBase<TicketGetDetailsResult>> ExecuteAsync(TicketGetDetailsArguments arguments)
		{
			var items = (await Repository.GetAsync())
				.Where(x => x.Id == arguments.Id);

			var result = items
				.Select(x => new
				{
					Id = x.Id,
					Date = x.Date,
					State = x.State,
					Reference = x.Reference,
					Title = x.Title,
					Amount = x.Amount,
					PayedAmount = x.Payments
						.Where(y => y.State == PaymentState.Active)
						.Sum(y => (decimal?)y.Amount) ?? 0m,
					Total = x.Amount,
					SupplierName = x.SupplierName,
					SupplierTaxAddress = x.TaxAddress,
					SupplierTaxNumber = x.TaxNumber,
					SupplierLogin = x.Concession.Concession.Supplier.Login,
					WorkerName = (x.PaymentWorker != null)? x.PaymentWorker.Name : x.SupplierName ,
					Payments = x.Payments
						.Select(y => new
						{
							Id = y.Id,
							Date = y.Date,
							State = y.State,
							TaxName = y.TaxName,
							NumberHash = y.PaymentMedia.NumberHash,
							Amount = y.Amount,
							Payin = y.Payin
						})
				})
				.ToList()
				.Select(x => new TicketGetDetailsResult
				{
					Id = x.Id,
					Date = x.Date, // Needs to be calculated in memory
					State = x.State,
					Reference = x.Reference,
					Title = x.Title,
					Amount = x.Amount,
					Total = x.Total,
					SupplierName = x.SupplierName,
					SupplierTaxAddress = x.SupplierTaxAddress,
					SupplierTaxNumber = x.SupplierTaxNumber,
					SupplierLogin = x.SupplierLogin,
					PayedAmount = x.PayedAmount,
					WorkerName = x.WorkerName,
					Payments = x.Payments
						.Select(y => new TicketGetDetailsResult.Payment
						{
							Id = y.Id,
							Date = y.Date, // Needs to be calculated in memory
							State = y.State,
							StateAlias = y.State.ToEnumAlias(),
							TaxName = y.TaxName,
							NumberHash = y.NumberHash,
							Amount = y.Amount,
							Payin = y.Payin,
							Total = y.Amount - y.Payin
						})
				});

			var securityRepository = new SecurityRepository();
			foreach (var ticket in result)
			{
				var user = await securityRepository.GetUserAsync(ticket.SupplierLogin);
				if (user != null)
					ticket.SupplierFotoUrl = user.PhotoUrl;
			}
			return new ResultBase<TicketGetDetailsResult> { Data = result };
		}
		#endregion ExecuteAsync
	}
 }
