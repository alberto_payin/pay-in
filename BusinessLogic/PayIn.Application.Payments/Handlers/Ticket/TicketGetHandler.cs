﻿using PayIn.Application.Dto.Payments.Arguments.Ticket;
using PayIn.Application.Dto.Payments.Results.Ticket;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xp.Application.Dto;
using Xp.Domain;

namespace PayIn.Application.Payments.Handlers
{
	public class TicketGetHandler :
		IQueryBaseHandler<TicketGetArguments, TicketGetResult>
	{
		private readonly IEntityRepository<PayIn.Domain.Payments.Ticket> _Repository;

		#region Constructors
		public TicketGetHandler(
			IEntityRepository<PayIn.Domain.Payments.Ticket> repository,
			IEntityRepository<PayIn.Domain.Payments.PaymentConcession> concessionRepository
		)
		{
			if (repository == null)
				throw new ArgumentNullException("repository");
			_Repository = repository;
		}
		#endregion Constructors

		#region ExecuteAsync
		public async Task<ResultBase<TicketGetResult>> ExecuteAsync(TicketGetArguments arguments)
		{
			var items = await _Repository.GetAsync();

			var result = items
				.Where(x => x.Id == arguments.Id)
				.Select(x => new
				{
					Id = x.Id,
					Reference = x.Reference,
					Title = x.Title,
					Amount = x.Amount,
					SupplierName = x.SupplierName,
					State = x.State,
					Date = x.Date
				})
				.ToList()
				.Select(x => new TicketGetResult
				{
					Id = x.Id,
					Reference = x.Reference,
					Title = x.Title,
					Amount = x.Amount,
					SupplierName = x.SupplierName,
					State = x.State,
					Date = x.Date.ToUTC() // It's needed to calculate in memory
				});

			return new ResultBase<TicketGetResult> { Data = result };
		}
		#endregion ExecuteAsync
	}
}
