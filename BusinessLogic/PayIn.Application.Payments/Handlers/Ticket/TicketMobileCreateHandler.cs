﻿using PayIn.Application.Dto.Payments.Arguments.Ticket;
using PayIn.BusinessLogic.Common;
using PayIn.Common;
using PayIn.Common.Resources;
using PayIn.Domain.Payments;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xp.Application.Attributes;
using Xp.Application.Dto;
using Xp.Domain;

namespace PayIn.Application.Payments.Handlers
{
	[XpLog("Ticket", "Create")]
	[XpAnalytics("Ticket", "Create")]
	public class TicketMobileCreateHandler :
		IServiceBaseHandler<TicketMobileCreateArguments>
	{
		private readonly ISessionData SessionData;
		private readonly IEntityRepository<Ticket> Repository;
		private readonly IEntityRepository<PaymentConcession> PaymentConcessionRepository;
		private readonly IEntityRepository<PaymentWorker> PaymentWorkerRepository;

		#region Contructors
		public TicketMobileCreateHandler(
			ISessionData sessionData,
            IEntityRepository<Ticket> repository,
			IEntityRepository<PaymentConcession> paymentConcessionRepository,
			IEntityRepository<PaymentWorker> paymentWorkerRepository
		)
		{
			if (sessionData == null) throw new ArgumentNullException("sessionData");
			if (repository == null) throw new ArgumentNullException("repository");
			if (paymentConcessionRepository == null) throw new ArgumentNullException("paymentsConcessionRepository");
			if (paymentWorkerRepository == null) throw new ArgumentNullException("paymentsWorkerRepository");

			SessionData = sessionData;
			Repository = repository;
			PaymentConcessionRepository = paymentConcessionRepository;
			PaymentWorkerRepository = paymentWorkerRepository;
		}
		#endregion Contructors

		#region ExecuteAsync
		public async Task<dynamic> ExecuteAsync(TicketMobileCreateArguments arguments)
		{
			var concession = (await PaymentConcessionRepository.GetAsync("Concession.Supplier"))
				.Where(x =>
					x.Id == arguments.ConcessionId &&
					x.Concession.State == ConcessionState.Active
				)
				.FirstOrDefault();

			if (concession.Concession.State != ConcessionState.Active)
				throw new ArgumentException(TicketResources.CreateNonActiveConcessionException, "concessionId");

			var worker = (await PaymentWorkerRepository.GetAsync())
				.Where(x => x.Login == SessionData.Login && x.State == WorkerState.Active)
				.FirstOrDefault();

			var ticket = new Ticket
			{
				Title = arguments.Title.IsNullOrEmpty() ? TicketResources.Several : arguments.Title,
				Date = arguments.Date.Value.ToUTC(),
				Amount = arguments.Amount,
				Reference = arguments.Reference,
				ConcessionId = concession.Id,
				Concession = concession,
				PaymentWorkerId = (worker!=null)? (int?)worker.Id : null,
				PaymentWorker = (worker==null)? null : worker ,
				SupplierName = concession.Concession.Supplier.Name,
				TaxName = concession.Concession.Supplier.TaxName,
				TaxNumber = concession.Concession.Supplier.TaxNumber,
				TaxAddress = concession.Concession.Supplier.TaxAddress,
				State = TicketStateType.Active
			};
			await Repository.AddAsync(ticket);

			return ticket;
		}
		#endregion ExecuteAsync
	}
}
