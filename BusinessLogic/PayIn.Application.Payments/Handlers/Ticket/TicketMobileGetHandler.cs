﻿using PayIn.Application.Dto.Payments.Arguments.Ticket;
using PayIn.Application.Dto.Payments.Results.Ticket;
using PayIn.BusinessLogic.Common;
using PayIn.Common;
using PayIn.Domain.Payments;
using PayIn.Infrastructure.Security;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xp.Application.Dto;
using Xp.Domain;

namespace PayIn.Application.Payments.Handlers
{
	public class TicketMobileGetHandler :
		IQueryBaseHandler<TicketMobileGetArguments, TicketMobileGetResult>
	{
		private readonly ISessionData SessionData;
		private readonly IEntityRepository<Ticket> Repository;
		private readonly IEntityRepository<PaymentConcession> ConcessionRepository;

		#region Constructors
		public TicketMobileGetHandler(
			ISessionData sessionData,
			IEntityRepository<Ticket> repository,
			IEntityRepository<PaymentConcession> concessionRepository
		)
		{
			if (sessionData == null) throw new ArgumentNullException("sessionData");
			if (repository == null) throw new ArgumentNullException("repository");
			if (concessionRepository == null) throw new ArgumentNullException("concessionRepository");

			SessionData = sessionData;
			Repository = repository;
			ConcessionRepository = concessionRepository;
		}
		#endregion Constructors

		#region ExecuteAsync
		async Task<ResultBase<TicketMobileGetResult>> IQueryBaseHandler<TicketMobileGetArguments, TicketMobileGetResult>.ExecuteAsync(TicketMobileGetArguments arguments)
		{
			var items = await Repository.GetAsync("PaymentWorker");

			var ticket = items
				.Where(x => x.Id == arguments.Id)
				.FirstOrDefault();
			var result = items
				.Where(x => x.Id == arguments.Id)
				.Select(x => new
				{
					Id = x.Id,
					Reference = x.Reference,
					Title = x.Title,
					Amount = x.Amount,
					PayedAmount = x.Payments.Where(y => y.State != PaymentState.Error).Sum(y => (decimal?) y.Amount) ?? 0m,
					Date = x.Date,
					State = x.State,
					CanReturn = ((x.Concession.Concession.Supplier.Login == SessionData.Login) || (x.PaymentWorker.Login == SessionData.Login)),
					SupplierName = x.TaxName,
					SupplierAddress = x.TaxAddress,
					SupplierNumber = x.TaxNumber,
					SupplierPhone = x.Concession.Phone,
					WorkerName = (x.PaymentWorker != null) ? x.PaymentWorker.Name : x.SupplierName,
					Payments = x.Payments
						.Where(y => ((x.Concession.Concession.Supplier.Login == SessionData.Login || 
									 (x.PaymentWorker.Login == SessionData.Login &&
									  x.PaymentWorker.State == WorkerState.Active)) && 
									  y.State != PaymentState.Error) || 
									  y.UserLogin == SessionData.Login)
						.Select(y => new
							{
								Id = y.Id,
								Amount = y.Amount,
								UserName = y.TaxName,
								PaymentMediaName = y.PaymentMedia.NumberHash,
								Date = y.Date,
								State = y.State,
								CanBeReturned = ((y.RefundTo.Sum(z => (decimal?)z.Amount) ?? 0) < y.Amount) && (y.RefundFromId == null) && (y.LiquidationId == null)
							})
				})
				.ToList()
				.Select(x => new TicketMobileGetResult
				{
					Id = x.Id,
					Reference = x.Reference,
					Title = x.Title,
					Amount = x.Amount,
					PayedAmount = x.PayedAmount,
					Date = x.Date.ToUTC(),
					State = x.State,
					CanReturn = x.CanReturn,
					SupplierName = x.SupplierName,
					SupplierAddress = x.SupplierAddress,
					SupplierNumber = x.SupplierNumber,
					SupplierPhone = x.SupplierPhone,
					WorkerName = x.WorkerName,
                    Payments = x.Payments.Select(y => new TicketMobileGetResult.Payment 
						{
							Id               = y.Id,
							Amount           = y.Amount,
							UserName         = y.UserName,
							PaymentMediaName = y.PaymentMediaName,
							Date             = y.Date.ToUTC(),
							State            = y.State,
							CanBeReturned    = y.CanBeReturned
						})
						.ToList()
				})
				.ToList();

			return new ResultBase<TicketMobileGetResult> { Data = result };
		}
		#endregion ExecuteAsync
	}
}
