﻿using PayIn.Application.Dto.Payments.Arguments.Ticket;
using PayIn.Application.Dto.Payments.Results.Ticket;
using PayIn.BusinessLogic.Common;
using PayIn.Common;
using PayIn.Domain.Payments;
using PayIn.Domain.Payments.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xp.Application.Dto;
using Xp.Domain;

namespace PayIn.Application.Payments.Handlers
{
	public class TicketMobileGetWithPaymentMediasHandler :
		IQueryBaseHandler<TicketMobileGetWithPaymentMediasArguments, TicketMobileGetWithPaymentMediasResult>
	{
		private readonly ISessionData SessionData;
		private readonly IEntityRepository<Ticket> Repository;
		private readonly IEntityRepository<PaymentConcession> ConcessionRepository;
		private readonly IEntityRepository<PaymentMedia> PaymentMediaRepository;
		private readonly IInternalService InternalService;

		#region Constructors
		public TicketMobileGetWithPaymentMediasHandler(
			ISessionData sessionData,
			IEntityRepository<Ticket> repository,
			IEntityRepository<PaymentConcession> concessionRepository,
			IEntityRepository<PaymentMedia> paymentMediaRepository,
			IInternalService internalService
		)
		{
			if (sessionData == null) throw new ArgumentNullException("sessionData");
			if (repository == null) throw new ArgumentNullException("repository");
			if (concessionRepository == null) throw new ArgumentNullException("concessionRepository");
			if (paymentMediaRepository == null) throw new ArgumentNullException("paymentMediaRepository");
			if (internalService == null) throw new ArgumentNullException("internalService");

			SessionData = sessionData;
			Repository = repository;
			ConcessionRepository = concessionRepository;
			PaymentMediaRepository = paymentMediaRepository;
			InternalService = internalService;
		}
		#endregion Constructors

		#region ExecuteAsync
		async Task<ResultBase<TicketMobileGetWithPaymentMediasResult>> IQueryBaseHandler<TicketMobileGetWithPaymentMediasArguments, TicketMobileGetWithPaymentMediasResult>.ExecuteAsync(TicketMobileGetWithPaymentMediasArguments arguments)
		{
			var hasPayment = await InternalService.UserHasPaymentAsync();

			var ticket = (await Repository.GetAsync("PaymentWorker"))
				.Where(x => x.Id == arguments.Id)
				.Select(x => new
				{
					Id = x.Id,
					Reference = x.Reference,
					Title = x.Title,
					Amount = x.Amount,
					PayedAmount = x.Payments.Where(y => y.State != PaymentState.Error).Sum(y => (decimal?)y.Amount) ?? 0m,
					Date = x.Date,
					State = x.State,
					CanReturn = ((x.Concession.Concession.Supplier.Login == SessionData.Login) || (x.PaymentWorker.Login == SessionData.Login)),
					SupplierName = x.TaxName,
					SupplierAddress = x.TaxAddress,
					SupplierNumber = x.TaxNumber,
					SupplierPhone = x.Concession.Phone,
					WorkerName = (x.PaymentWorker != null) ? x.PaymentWorker.Name : x.SupplierName,
					Payments = x.Payments
						.Where(y => ((x.Concession.Concession.Supplier.Login == SessionData.Login ||
									 (x.PaymentWorker.Login == SessionData.Login &&
									  x.PaymentWorker.State == WorkerState.Active)) &&
									  y.State != PaymentState.Error) ||
									  y.UserLogin == SessionData.Login)
						.Select(y => new
						{
							Id = y.Id,
							Amount = y.Amount,
							UserName = y.TaxName,
							PaymentMediaName = y.PaymentMedia.NumberHash,
							Date = y.Date,
							State = y.State,
							CanBeReturned = ((y.RefundTo.Sum(z => (decimal?)z.Amount) ?? 0) < y.Amount) && (y.RefundFromId == null) && (y.LiquidationId == null)
						})
				})
				.ToList()
				.Select(x => new TicketMobileGetWithPaymentMediasResult.TicketResult
				{
					Id = x.Id,
					Reference = x.Reference,
					Title = x.Title,
					Amount = x.Amount,
					PayedAmount = x.PayedAmount,
					Date = x.Date.ToUTC(),
					State = x.State,
					CanReturn = x.CanReturn,
					SupplierName = x.SupplierName,
					SupplierAddress = x.SupplierAddress,
					SupplierNumber = x.SupplierNumber,
					SupplierPhone = x.SupplierPhone,
					WorkerName = x.WorkerName,
					Payments = x.Payments.Select(y => new TicketMobileGetWithPaymentMediasResult.Payment
					{
						Id = y.Id,
						Amount = y.Amount,
						UserName = y.UserName,
						PaymentMediaName = y.PaymentMediaName,
						Date = y.Date.ToUTC(),
						State = y.State,
						CanBeReturned = y.CanBeReturned
					})
					.ToList()
				})
				.FirstOrDefault();
			
			var paymentMedias = (await PaymentMediaRepository.GetAsync())
				.Where(x =>
					x.State != PaymentMediaState.Error &&
					x.State != PaymentMediaState.Delete
				)
				.Select(x => new TicketMobileGetWithPaymentMediasResult.PaymentMedia
				{
					Id = x.Id,
					Title = x.Name,
					Subtitle = x.Type.ToString(),
					VisualOrder = x.VisualOrder,
					NumberHash = x.NumberHash,
					ExpirationMonth = x.ExpirationMonth,
					ExpirationYear = x.ExpirationYear,
					Type = x.Type,
					State = x.State,
					BankEntity = x.BankEntity
				});

			var result = new TicketMobileGetWithPaymentMediasResult
			{
				Ticket = ticket,
				HasPayment = hasPayment,
				PaymentMedias = paymentMedias
			};

			List<TicketMobileGetWithPaymentMediasResult> data = new List<TicketMobileGetWithPaymentMediasResult>();
			data.Add(result);

			return new ResultBase<TicketMobileGetWithPaymentMediasResult> { Data = data };
		}
		#endregion ExecuteAsync
	}
}
