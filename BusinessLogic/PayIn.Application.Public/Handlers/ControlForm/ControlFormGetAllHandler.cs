﻿using PayIn.Application.Dto.Arguments.ControlForm;
using PayIn.Application.Dto.Results.ControlForm;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xp.Application.Dto;
using Xp.Domain;
using PayIn.Common;

namespace PayIn.Application.Public.Handlers
{
	public class ControlFormGetAllHandler :
		IQueryBaseHandler<ControlFormGetAllArguments, ControlFormGetAllResult>
	{
		private readonly IEntityRepository<PayIn.Domain.Public.ControlForm> Repository;

		#region Constructors
		public ControlFormGetAllHandler(
			IEntityRepository<PayIn.Domain.Public.ControlForm> repository
		)
		{
			if (repository == null)
				throw new ArgumentNullException("repository");
			Repository = repository;
		}
		#endregion Constructors


		#region ExecuteAsync
		async Task<ResultBase<ControlFormGetAllResult>> IQueryBaseHandler<ControlFormGetAllArguments, ControlFormGetAllResult>.ExecuteAsync(ControlFormGetAllArguments arguments)
		{
			var forms = await Repository.GetAsync();

			if (!arguments.Filter.IsNullOrEmpty())
				forms = forms.Where(x => (
					x.Name.Contains(arguments.Filter) ||
					x.Observations.Contains(arguments.Filter)
				));

			var result = forms
				.Select(x => new
				{
					Id = x.Id,
					Name = x.Name,
					Observations = x.Observations,
					Arguments = x.Arguments.Select(y => new { 
						Id = y.Id,
						Name = y.Name,
						Observations = y.Observations,
						Type = y.Type,
						Target = y.Target,
						IsRequired = y.IsRequired
					})
				})
				.ToList()
				.Select(x => new ControlFormGetAllResult
				{
					Id = x.Id,
					Name = x.Name,
					Observations = x.Observations,
					Arguments = x.Arguments.Select(y => new ControlFormGetAllResult.ControlFormGetAllResult_Arguments
					{
						Id = y.Id,
						Name = y.Name,
						Observations = y.Observations,
						Type = y.Type,
						Target = y.Target,
						IsRequired = y.IsRequired
					})
				})
				.OrderBy(x => x.Id);

			return new ResultBase<ControlFormGetAllResult> { Data = result };
		}
		#endregion ExecuteAsync
	
	}
}
