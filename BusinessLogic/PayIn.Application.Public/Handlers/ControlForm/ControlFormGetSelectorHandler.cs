﻿using PayIn.Application.Dto.Arguments.ControlForm;
using PayIn.Application.Dto.Results.ControlForm;
using PayIn.Domain.Public;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xp.Application.Dto;
using Xp.Domain;

namespace PayIn.Application.Public.Handlers
{
	public class ControlFormGetSelectorHandler :
		IQueryBaseHandler<ControlFormGetSelectorArguments, ControlFormGetSelectorResult>
	{
		private readonly IEntityRepository<ControlForm> _Repository;

		#region Constructors
		public ControlFormGetSelectorHandler(IEntityRepository<ControlForm> repository)
		{
			if (repository == null)
				throw new ArgumentNullException("repository");
			_Repository = repository;
		}
		#endregion Constructors

		#region ExecuteAsync
		public async Task<ResultBase<ControlFormGetSelectorResult>> ExecuteAsync(ControlFormGetSelectorArguments arguments)
		{
			var items = await _Repository.GetAsync();

			var result = items
				.Where(x => x.Name.Contains(arguments.Filter))
				.Select(x => new ControlFormGetSelectorResult
				{
					Id = x.Id,
					Value = x.Name
				});

			return new ResultBase<ControlFormGetSelectorResult> { Data = result };
		}
		#endregion ExecuteAsync
	}
}
