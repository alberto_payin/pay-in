﻿using PayIn.Application.Dto;
using PayIn.Application.Dto.Arguments.ControlFormArgument;
using PayIn.BusinessLogic.Common;
using PayIn.Common;
using PayIn.Domain.Public;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xp.Application.Dto;
using Xp.Domain;

namespace PayIn.Application.Public.Handlers
{
	public class ControlFormArgumentUpdateHandler :
		IServiceBaseHandler<ControlFormArgumentUpdateArguments>
	{
		private readonly IEntityRepository<ControlFormArgument> _Repository;

		#region Contructors
		public ControlFormArgumentUpdateHandler(
			IEntityRepository<ControlFormArgument> repository
		)
		{
			if (repository == null) throw new ArgumentNullException("repository");

			_Repository = repository;
		}
		#endregion Contructors

		#region ExecuteAsync
		async Task<dynamic> IServiceBaseHandler<ControlFormArgumentUpdateArguments>.ExecuteAsync(ControlFormArgumentUpdateArguments arguments)
		{
			var formArgument = await _Repository.GetAsync(arguments.Id);

			formArgument.Name = arguments.Name;
			formArgument.Observations = arguments.Observations;
			formArgument.Type = arguments.Type;
			formArgument.Target = arguments.Target;
			formArgument.IsRequired = arguments.IsRequired;

			return formArgument;
		}
		#endregion ExecuteAsync
	}
}
