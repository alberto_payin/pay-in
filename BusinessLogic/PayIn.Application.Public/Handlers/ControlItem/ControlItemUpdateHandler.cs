﻿using PayIn.Application.Dto.Arguments.ControlItem;
using PayIn.Domain.Public;
using System;
using System.Threading.Tasks;
using Xp.Application.Dto;
using Xp.Common;
using Xp.Domain;

namespace PayIn.Application.Public
{
	public class ControlItemUpdateHandler :
		IServiceBaseHandler<ControlItemUpdateArguments>
	{
		private readonly IEntityRepository<ControlItem> _Repository;

		#region Constructors
		public ControlItemUpdateHandler(IEntityRepository<ControlItem> repository)
		{
			if (repository == null)
				throw new ArgumentNullException("repository");
			_Repository = repository;
		}
		#endregion Constructors

		#region ExecuteAsync
		async Task<dynamic> IServiceBaseHandler<ControlItemUpdateArguments>.ExecuteAsync(ControlItemUpdateArguments arguments)
		{

			var trackFrecuency = arguments.TrackFrecuency;

			var trackFrecuencyMinutes = trackFrecuency.Value.Hours;
			var trackFrecuencySeconds = trackFrecuency.Value.Minutes;
			var frecuency = new DateTime();
			if (trackFrecuencyMinutes == 0 && trackFrecuencySeconds < 10)
			{
				frecuency = new DateTime(1900, 1, 1, 0, 0, 10);
			}
			else
			{
				frecuency = new DateTime(1900, 1, 1, 0, trackFrecuencyMinutes, trackFrecuencySeconds);

			}

			var trackFrecuencyResult = new XpDuration(frecuency);



			var item = await _Repository.GetAsync(arguments.Id);
			item.Name = arguments.Name ?? "";
			item.Observations = arguments.Observations ?? "";
			item.SaveTrack = arguments.SaveTrack;
			item.SaveFacialRecognition = arguments.SaveFacialRecognition;
			item.CheckTimetable = arguments.CheckTimetable;
			item.TrackFrecuency = trackFrecuencyResult;

			return item;
		}
		#endregion ExecuteAsync
	}
}