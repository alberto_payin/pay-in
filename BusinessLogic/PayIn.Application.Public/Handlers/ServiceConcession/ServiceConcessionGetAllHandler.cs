﻿using PayIn.Application.Dto.Arguments.ServiceConcession;
using PayIn.Application.Dto.Results.ServiceConcession;
using PayIn.Domain.Public;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xp.Application.Dto;
using Xp.Domain;

namespace PayIn.Application.Public.Handlers
{
	public class ServiceConcessionGetAllHandler :
		IQueryBaseHandler<ServiceConcessionGetAllArguments, ServiceConcessionGetAllResult>
	{
		private readonly IEntityRepository<ServiceConcession> Repository;

		#region Constructors
		public ServiceConcessionGetAllHandler(IEntityRepository<ServiceConcession> repository)
		{
			if (repository == null)
				throw new ArgumentNullException("repository");
			Repository = repository;
		}
		#endregion Constructors

		#region ExecuteAsync
		async Task<ResultBase<ServiceConcessionGetAllResult>> IQueryBaseHandler<ServiceConcessionGetAllArguments, ServiceConcessionGetAllResult>.ExecuteAsync(ServiceConcessionGetAllArguments arguments)
		{
			var items = await Repository.GetAsync();
			if (!arguments.Filter.IsNullOrEmpty())
				items = items
					.Where(x => 
						x.Name.Contains(arguments.Filter)
					);

			var result = items
				.Select(x => new ServiceConcessionGetAllResult
				{
					Id = x.Id,
					Name = x.Name,
					SupplierId = x.Id,
					Zones = x.Zones.Select(y => new ServiceConcessionGetAllResult.Zone
					{
						Id = y.Id,
						Name = y.Name,
						CancelationAmount = y.CancelationAmount
					})
				});

			return new ResultBase<ServiceConcessionGetAllResult> { Data = result };
		}
		#endregion ExecuteAsync
	}
}
