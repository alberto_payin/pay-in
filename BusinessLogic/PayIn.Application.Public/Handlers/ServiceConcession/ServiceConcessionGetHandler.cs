﻿using PayIn.Application.Dto.Arguments.ServiceConcession;
using PayIn.Application.Dto.Results.ServiceConcession;
using PayIn.Common;
using PayIn.Domain.Payments;
using PayIn.Domain.Public;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xp.Application.Dto;
using Xp.Domain;

namespace PayIn.Application.Public.Handlers
{
	public class ServiceConcessionGetHandler :
		IQueryBaseHandler<ServiceConcessionGetArguments, ServiceConcessionGetResult>
	{
		private readonly IEntityRepository<ServiceConcession> Repository;
		private readonly IEntityRepository<PaymentConcession> PaymentRepository;


		#region Constructors
		public ServiceConcessionGetHandler(IEntityRepository<ServiceConcession> repository, IEntityRepository<PaymentConcession> paymentRepository)
		{
			if (repository == null) throw new ArgumentNullException("repository");
			Repository = repository;

			if (paymentRepository == null) throw new ArgumentNullException("paymentRepository");
			PaymentRepository = paymentRepository;
		}
		#endregion Constructors

		#region ExecuteAsync
		public async Task<ResultBase<ServiceConcessionGetResult>> ExecuteAsync(ServiceConcessionGetArguments arguments)
		{
			var paymentConcesion = (await PaymentRepository.GetAsync())
				.Where(x => x.ConcessionId == (arguments.Id))
				.FirstOrDefault();

			if (paymentConcesion == null)
			{

				var items = await Repository.GetAsync();
				var result = items
					.Where(x => x.Id.Equals(arguments.Id))
					.Select(x => new ServiceConcessionGetResult
					{
						Id = x.Id,
						Name = x.Name,
						Type = x.Type,
						MaxWorkers = x.MaxWorkers,
						State = x.State
					});

				return new ResultBase<ServiceConcessionGetResult> { Data = result };				
			}
			else
			{

				var items = await Repository.GetAsync();
				var result = items
					.Where(x => x.Id.Equals(arguments.Id))
					.Select(x => new ServiceConcessionGetResult
					{
						Id = x.Id,
						Name = x.Name,
						Type = x.Type,
						MaxWorkers = x.MaxWorkers,
						State = x.State,
						PayinComission = x.Type == ServiceType.Charge ? paymentConcesion.PayinCommision : 0m,
						LiquidationAmountMin = x.Type == ServiceType.Charge ? paymentConcesion.LiquidationAmountMin : 0m,
					});


				return new ResultBase<ServiceConcessionGetResult> { Data = result };
			}
		}
		#endregion ExecuteAsync
	}
}
