﻿using PayIn.Application.Dto.Arguments.ServiceConcession;
using PayIn.Application.Dto.Results.ServiceConcession;
using PayIn.BusinessLogic.Common;
using PayIn.Domain.Public;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xp.Application.Dto;
using Xp.Domain;

namespace PayIn.Application.Public.Handlers
{
	public class ServiceConcessionGetSelectorHandler :
		IQueryBaseHandler<ServiceConcessionGetSelectorArguments, ServiceConcessionGetSelectorResult>
	{
		private readonly ISessionData                         SessionData;
		private readonly IEntityRepository<ServiceConcession> Repository;

		#region Constructors
		public ServiceConcessionGetSelectorHandler(IEntityRepository<ServiceConcession> repository, ISessionData sessionData)
		{
			if (sessionData == null) throw new ArgumentNullException("sessionData");
			if (repository == null)  throw new ArgumentNullException("repository");

			SessionData = sessionData;
			Repository  = repository;
		}
		#endregion Constructors

		#region ExecuteAsync
		async Task<ResultBase<ServiceConcessionGetSelectorResult>> IQueryBaseHandler<ServiceConcessionGetSelectorArguments, ServiceConcessionGetSelectorResult>.ExecuteAsync(ServiceConcessionGetSelectorArguments arguments)
		{
			var items = (await Repository.GetAsync())
				.Where(x => x.Type == Common.ServiceType.Control);
			if (!arguments.Filter.IsNullOrEmpty())
				items = items.Where(x => 
					x.Name.Contains(arguments.Filter)
				);

			var result = items
				.Select(x => new ServiceConcessionGetSelectorResult
				{
					Id = x.Id,
					Value = x.Name
				});

			return new ResultBase<ServiceConcessionGetSelectorResult> { Data = result };
		}
		#endregion ExecuteAsync
	}
}
