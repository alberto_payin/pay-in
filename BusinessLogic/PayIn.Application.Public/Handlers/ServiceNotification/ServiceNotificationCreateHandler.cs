﻿using PayIn.Application.Dto.Arguments.Notification;
using PayIn.Domain.Public;
using PayIn.BusinessLogic.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xp.Application.Dto;
using Xp.Domain;
using Xp.Infrastructure.Repositories;
using PayIn.Common;

namespace PayIn.Application.Public.Handlers
{
	public class ServiceNotificationCreateHandler :
		IServiceBaseHandler<ServiceNotificationCreateArguments>
	{
		private readonly ISessionData SessionData;
		private readonly IEntityRepository<ServiceNotification> Repository;
		private readonly IPushRepository RepositoryPush;

		#region Constructors
		public ServiceNotificationCreateHandler(
			ISessionData sessionData,
			IEntityRepository<ServiceNotification> repository,
			IPushRepository repositoryPush
		) 	
		{
			if (sessionData == null) throw new ArgumentNullException("sessionData");
			if (repository == null) throw new ArgumentNullException("repository");
			if (repositoryPush == null) throw new ArgumentNullException("repositoryPush");

			SessionData = sessionData;
			Repository = repository;
			RepositoryPush = repositoryPush;
		}
		#endregion Constructors
		
		#region ExecuteAsync
		public async Task<dynamic> ExecuteAsync(ServiceNotificationCreateArguments arguments)
		{
			var now = DateTime.Now;

			var Notification = new ServiceNotification
			{
				CreatedAt = now,
				Type = arguments.Type,
				State = NotificationState.Actived,
				ReferenceId = arguments.ReferenceId,
				ReferenceClass = arguments.ReferenceClass,
				SenderLogin = arguments.SenderLogin,
				ReceiverLogin = arguments.ReceiverLogin,
				IsRead = false 
			};
			await Repository.AddAsync(Notification);

			var logins = new List<string>
			{
				//arguments.SenderLogin,
				arguments.ReceiverLogin
			};

			await RepositoryPush.SendNotification(logins, Notification.Type, Notification.State, arguments.Message, arguments.ReferenceClass, arguments.ReferenceId.ToString(), 0 );

			return null;
		}			
		#endregion ExecuteAsync
	}
}
