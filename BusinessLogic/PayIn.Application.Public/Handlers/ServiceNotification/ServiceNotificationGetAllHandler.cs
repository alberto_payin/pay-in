﻿using PayIn.Application.Dto.Arguments.Notification;
using PayIn.Application.Dto.Results.Notification;
using PayIn.Domain.Public;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xp.Application.Dto;
using Xp.Domain;

namespace PayIn.Application.Public.Handlers
{
	public class ServiceNotificationGetAllHandler :
		IQueryBaseHandler<ServiceNotificationGetAllArguments, ServiceNotificationGetAllResult>
	{
		private readonly IEntityRepository<ServiceNotification> Repository;

		#region Constructors
		public ServiceNotificationGetAllHandler(IEntityRepository<ServiceNotification> repository)
		{
			if (repository == null) throw new ArgumentNullException("repository");
			Repository = repository;
		}
		#endregion Constructors

		#region ExecuteAsync
		async Task<ResultBase<ServiceNotificationGetAllResult>> IQueryBaseHandler<ServiceNotificationGetAllArguments, ServiceNotificationGetAllResult>.ExecuteAsync(ServiceNotificationGetAllArguments arguments)
		{
			var items = await Repository.GetAsync();

			//if (!arguments.Filter.IsNullOrEmpty())
			//	items = items
			//		.Where(x => 
			//			x.Estado.Contains(arguments.Filter) ||
			//		);
			/*
			var result = items
				.Select(x => new
				{
					Id = x.Id,
					State = x.State,
					SupplierId = x.SupplierId,
					Type = x.Type,
					WorkerId = x.WorkerId
				})
				.ToList()
				.Select(x => new ServiceNotificationsGetAllResult
				{
					Id = x.Id,
					Estado = x.Estado,
					NotificationsType = x.Type,
					SupplierId = x.SupplierId
				})
				.OrderBy(x => x.Id);

			return new ResultBase<ServiceNotificationsGetAllResult> { Data = result };*/
			return null;
		}
		#endregion ExecuteAsync
	}
}

