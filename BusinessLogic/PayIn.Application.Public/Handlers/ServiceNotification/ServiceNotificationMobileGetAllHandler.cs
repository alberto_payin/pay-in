﻿using PayIn.Application.Dto.Arguments.ServiceNotification;
using PayIn.Application.Dto.Results.ServiceNotification;
using PayIn.BusinessLogic.Common;
using PayIn.Common;
using PayIn.Common.Resources;
using PayIn.Domain.Payments;
using PayIn.Domain.Public;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xp.Application.Dto;
using Xp.Domain;

namespace PayIn.Application.Public.Handlers
{
	public class ServiceNotificationMobileGetAllHandler :
		IQueryBaseHandler<ServiceNotificationMobileGetAllArguments, ServiceNotificationMobileGetAllResult>
	{
		private readonly ISessionData SessionData;
		private readonly IEntityRepository<ServiceNotification> Repository;
		private readonly IEntityRepository<PaymentWorker> PaymentWorkerRepository;
		private readonly IEntityRepository<PaymentConcession> PaymentConcessionRepository;
		private readonly IEntityRepository<ServiceWorker> ServiceWorkerRepository;
		private readonly IEntityRepository<Ticket> TicketRepository;
		private readonly IUnitOfWork UnitOfWork;

		#region Constructors
		public ServiceNotificationMobileGetAllHandler(
			ISessionData sessionData,
			IEntityRepository<ServiceNotification> repository,
			IEntityRepository<PaymentWorker> paymentWorkerRepository,
			IEntityRepository<PaymentConcession> paymentConcessionRepository,
			IEntityRepository<ServiceWorker> serviceworkerRepository,
			IEntityRepository<Ticket> ticketRepository,
			IUnitOfWork unitOfWork
		)
		{
			if (sessionData == null) throw new ArgumentNullException("sessionData");
			SessionData = sessionData;

			if (unitOfWork == null) throw new ArgumentNullException("unitOfWork");
			UnitOfWork = unitOfWork;

			if (repository == null)
				throw new ArgumentNullException("repository");
			Repository = repository;

			if (paymentWorkerRepository == null) throw new ArgumentNullException("PaymentWorkerRepository");
			PaymentWorkerRepository = paymentWorkerRepository;

			if (paymentConcessionRepository == null) throw new ArgumentNullException("paymentConcessionRepository");
			PaymentConcessionRepository = paymentConcessionRepository;

			if (serviceworkerRepository == null) throw new ArgumentNullException("serviceworkerRepository");
			ServiceWorkerRepository = serviceworkerRepository;


			if (ticketRepository == null) throw new ArgumentNullException("ticketRepository");
			TicketRepository = ticketRepository;
		}
		#endregion Constructors

		#region ExecuteAsync
		public async Task<ResultBase<ServiceNotificationMobileGetAllResult>> ExecuteAsync(ServiceNotificationMobileGetAllArguments arguments)
		{
			var items = (await Repository.GetAsync())
				.Where(x => x.ReceiverLogin == SessionData.Login);
				
			
			var paymentWorkers = await PaymentWorkerRepository.GetAsync();
			var controlWorkers = await ServiceWorkerRepository.GetAsync();
			var tickets = await TicketRepository.GetAsync();

			var result = items
				.Select(x => new
				{
					Id = x.Id,
					Date = x.CreatedAt,
					State = x.State,
					Type = x.Type,
					ReferenceId = x.ReferenceId,
					ReferenceClass = x.ReferenceClass,
					SenderLogin = x.SenderLogin,
					IsRead = true,
					ControlWorker = controlWorkers
						.Where(y =>
							x.ReferenceClass == "ServiceWorker" &&
							y.Id == x.ReferenceId
						)
						.Select(y => y.Name)
						.FirstOrDefault() ?? "",
					ControlWorkerSupplier = controlWorkers
						.Where(y =>
							x.ReferenceClass == "ServiceWorker" &&
							y.Id == x.ReferenceId
						)
						.Select(y => y.Supplier.Name)
						.FirstOrDefault() ?? "",
					PaymentWorker = paymentWorkers
						.Where(y =>
							x.ReferenceClass == "PaymentWorker" &&
							y.Id == x.ReferenceId
						)
						.Select(y => y.Name)
						.FirstOrDefault() ?? "",
					PaymentWorkerSupplier = paymentWorkers
						.Where(y =>
							x.ReferenceClass == "PaymentWorker" &&
							y.Id == x.ReferenceId
						)
						.Select(y => y.Concession.Concession.Supplier.Name)
						.FirstOrDefault() ?? "",
					TicketAmount = tickets
						.Where(y =>
							x.ReferenceClass == "Ticket" &&
							y.Id == x.ReferenceId
						)
						.Select(y => (decimal?) y.Amount)
						.FirstOrDefault() ?? 0m,
					TicketPayer = tickets
						.Where(y =>
							x.ReferenceClass == "Ticket" &&
							y.Id == x.ReferenceId
						)
						.SelectMany(y => y.Payments)
						.Where(y => y.Amount > 0)
						.Select(y => y.UserName)
						.FirstOrDefault() ?? "",
					TicketSupplier = tickets
						.Where(y =>
							x.ReferenceClass == "Ticket" &&
							y.Id == x.ReferenceId
						)
						.Select(y => y.Concession.Concession.Supplier.Name)
						.FirstOrDefault() ?? "",
					TicketErrorCode = tickets
						.Where(y =>
							x.ReferenceClass == "Ticket" && 
							y.Id == x.ReferenceId
						)
						.SelectMany(y => y.Payments)
						.Select(y => y.ErrorCode)
						.FirstOrDefault() ?? "",
					TicketErrorText = tickets
						.Where(y =>
							x.ReferenceClass == "Ticket" && 
							y.Id == x.ReferenceId
						)
						.SelectMany(y => y.Payments)
						.Select(y => y.ErrorText)
						.FirstOrDefault() ?? ""
				})
				.ToList()
				.OrderByDescending(x => x.Date)
				.Select(x => new ServiceNotificationMobileGetAllResult
				{
					Id = x.Id,
					Date = x.Date.ToUTC(),
					State = x.State,
					Type = x.Type,
					Message = 
						(x.Type == NotificationType.ConcessionVinculation) ?
							PaymentWorkerResources.AcceptAssociationMessage.FormatString(x.PaymentWorkerSupplier) :
						(x.Type == NotificationType.ConcessionVinculationAccepted && x.ReferenceClass == "PaymentWorker") ?
							PaymentWorkerResources.AssociationAccepted.FormatString(x.PaymentWorker, x.PaymentWorkerSupplier) :
						(x.Type == NotificationType.ConcessionVinculationRefused && x.ReferenceClass == "PaymentWorker") ?
							PaymentWorkerResources.AssociationRefused.FormatString(x.PaymentWorker, x.PaymentWorkerSupplier) :
						(x.Type == NotificationType.ConcessionVinculationAccepted && x.ReferenceClass == "ServiceWorker") ?
							ServiceWorkerResources.AssociationAccepted.FormatString(x.ControlWorker, x.ControlWorkerSupplier) :
						(x.Type == NotificationType.ConcessionVinculationRefused && x.ReferenceClass == "ServiceWorker") ?
							ServiceWorkerResources.AssociationRefused.FormatString( x.ControlWorker, x.ControlWorkerSupplier) :
						(x.Type == NotificationType.PaymentMediaCreateError) ?
							PaymentMediaResources.PaymentMediaCreateExceptionRefund :
						(x.Type == NotificationType.PaymentMediaCreateSucceed) ?
							PaymentMediaResources.PaymentMediaCreateSuccess :
						(x.Type == NotificationType.PaymentSucceed && x.SenderLogin == "info@pay-in.es") ?
							PaymentResources.PaymentMediaCreationPaymentPushMessage.FormatString(x.TicketAmount) :
						(x.Type == NotificationType.PaymentSucceed) ?
							PaymentResources.PaidPushMessage.FormatString(x.TicketAmount, x.TicketPayer) :
						(x.Type == NotificationType.RefundSucceed && x.SenderLogin == "info@pay-in.es") ? 
							PaymentResources.PaymentMediaCreationRefundPushMessage.FormatString(x.TicketAmount) :
						(x.Type == NotificationType.RefundSucceed) ? 
							PaymentResources.RefundedPushMessage.FormatString(x.TicketAmount, x.TicketSupplier) :
						(x.Type == NotificationType.RefundError) ?
							PaymentResources.PaymentMediaCreationRefundErrorPushMessage.FormatString(x.TicketAmount,
								ServiceNotificationResources.GatewayError.FormatString(x.TicketErrorCode, x.TicketErrorText)
							) :
						(x.Type == NotificationType.PaymentError) ? 
							PaymentResources.PaymentMediaCreationPaymentErrorPushMessage.FormatString(x.TicketAmount, 
								ServiceNotificationResources.GatewayError.FormatString(x.TicketErrorCode, x.TicketErrorText)
							) :
						(x.Type == NotificationType.ConcessionDissociation) ? 
							PaymentWorkerResources.DissociationMessage.FormatString(x.PaymentWorkerSupplier) :
						(x.Type == NotificationType.PaymentWorkerConcessionDissociation) ? 
							PaymentWorkerResources.WorkerDissociationMessage.FormatString(x.PaymentWorker, x.PaymentWorkerSupplier) :
						"",
					ReferenceId = x.ReferenceId,
					ReferenceClass = x.ReferenceClass,
					IsRead = x.IsRead
				});

			foreach (ServiceNotification item in items)
			{
				item.IsRead = true;
			}
			await UnitOfWork.SaveAsync();

			return new ResultBase<ServiceNotificationMobileGetAllResult> { Data = result };
		}

		#endregion ExecuteAsync
	}
}

