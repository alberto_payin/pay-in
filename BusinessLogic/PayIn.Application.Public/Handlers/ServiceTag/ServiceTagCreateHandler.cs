﻿using PayIn.Application.Dto.Arguments.ServiceTag;
using PayIn.Domain.Public;
using System;
using System.Threading.Tasks;
using Xp.Application.Dto;
using Xp.Domain;
namespace PayIn.Application.Public
{
	public class ServiceTagCreateHandler :
		IServiceBaseHandler<ServiceTagCreateArguments>
	{
		private readonly IEntityRepository<ServiceTag> Repository;
		#region Constructors
		public ServiceTagCreateHandler(IEntityRepository<ServiceTag> repository)
		{
			if (repository == null) throw new ArgumentNullException("repository");
			Repository = repository;
		}
		#endregion Constructors
		#region ExecuteAsync
		async Task<dynamic> IServiceBaseHandler<ServiceTagCreateArguments>.ExecuteAsync(ServiceTagCreateArguments arguments)
		{
			var itemServiceTag = new ServiceTag
			{
				Type = arguments.Type,
				SupplierId = arguments.SupplierId,
				Reference = arguments.Reference,

			};
			await Repository.AddAsync(itemServiceTag);
			return itemServiceTag;
		}
		#endregion ExecuteAsync
	}
}