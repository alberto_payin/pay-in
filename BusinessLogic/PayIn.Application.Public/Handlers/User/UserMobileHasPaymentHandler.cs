﻿using Microsoft.Practices.Unity;
using PayIn.Application.Dto.Arguments.User;
using PayIn.BusinessLogic.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xp.Application.Dto;
using PayIn.Domain.Public.Infrastructure;

namespace PayIn.Application.Public.Handlers.User
{
	public class UserMobileHasPaymentHandler :
		IServiceBaseHandler<UserMobileHasPaymentArguments>
	{
		public ISessionData SessionData { get; set; }
		public IInternalService InternalService { get; set; }

		#region Constructors
		public UserMobileHasPaymentHandler(
			ISessionData sessionData,
			IInternalService internalService
		)
		{
			if (sessionData == null)
				throw new ArgumentNullException("sessionData");
			SessionData = sessionData;

			if (internalService == null)
				throw new ArgumentNullException("internalService");
			InternalService = internalService;
		}
		#endregion Constructors

		#region ExecuteAsync
		public async Task<dynamic> ExecuteAsync(UserMobileHasPaymentArguments arguments)
		{
			var result = await InternalService.HasPayment(SessionData.Login);
			return result;
		}
		#endregion ExecuteAsync
	}
}
