﻿using Microsoft.Practices.Unity;
using PayIn.Application.Dto.Payments.Arguments.Ticket;
using PayIn.Application.Payments.Decorators;
using PayIn.Application.Payments.Handlers;
using PayIn.Application.Public.Handlers.Main;
using PayIn.BusinessLogic.Common;
using PayIn.Domain.Payments;
using PayIn.Domain.Payments.Infrastructure;
using PayIn.Infrastructure.Payments.Repositories;
using PayIn.Infrastructure.Payments.Services;
using PayIn.Infrastructure.Public;
using PayIn.Infrastructure.Public.Db;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Web.Http;
using Unity.WebApi;
using Xp.Application.Attributes;
using Xp.Application.Decorators;
using Xp.Application.Dto;
using Xp.Application.Handlers;
using Xp.Common.Dto.Arguments;
using Xp.Domain;
using Xp.Infrastructure;
using Xp.Infrastructure.Repositories;
using Xp.Infrastructure.Services;

namespace PayIn.Common.DI.Public
{
	public static class DIConfig
	{
		#region Container
		private static IUnityContainer _Container;
		private static IUnityContainer Container
		{
			get
			{
				if (_Container == null)
					_Container = CreateContainer();

				return _Container;
			}
		}
		#endregion Container

		#region GetAssignsFromInterface
		private static IEnumerable<KeyValuePair<Type, Type>> GetAssignsFromInterface(Type type)
		{
			var assemblies = new List<Assembly>() {
				typeof(MainMobileGetAllHandler).Assembly,     // PayIn.Application
				typeof(TicketMobileCreateHandler).Assembly,   // PayIn.Application.Payments
				typeof(TicketMobileCreateArguments).Assembly, // PayIn.Common.Dto.Payments
				typeof(TicketRepository).Assembly,            // PayIn.Infrastructure.Payments
				typeof(PublicUnitOfWork).Assembly//,                // PayIn.Infrastructure.Public
			};

			var types = assemblies
				.SelectMany(x => x.DefinedTypes
					.Where(t =>
						!t.IsAbstract &&
						!t.IsGenericType &&
						t.IsNested != true
					)
					.Select(t => t.AsType())
				)
				.ToList()
			;

			if (type.IsGenericTypeDefinition)
			{
				var result = types
					.SelectMany(t => t.GetTypeInfo().ImplementedInterfaces
						.Select(t2 => new { Type = t, Interfaz = t2 })
						.Where(i => i.Interfaz.IsConstructedGenericType && i.Interfaz.GetGenericTypeDefinition() == type)
					)
					.Select(x => new KeyValuePair<Type, Type>(x.Interfaz, x.Type))
					.ToList()
				;
				return result;
			}
			else
			{
				var result = types
					.SelectMany(t => t.GetTypeInfo().ImplementedInterfaces
						.Where(i => i == type)
						.Select(t2 => new { Type = t, Interfaz = t2 })
					)
					.Select(x => new KeyValuePair<Type, Type>(x.Interfaz, x.Type))
					.ToList()
				;
				return result;
			}
		}
		#endregion GetAssignsFromInterface

		#region CreateContainer
		private static IUnityContainer CreateContainer()
		{
			var container = new UnityContainer();

			container

			// Container
			.RegisterInstance<IUnityContainer>(container)
			.RegisterType<IUnitOfWork, PublicUnitOfWork>(new PerResolveLifetimeManager())
			.RegisterType<IPublicContext, PublicContextAdapter>(new PerResolveLifetimeManager())
			.RegisterType<ISessionData, SessionData>()
			.RegisterType<IBlobRepository, AzureBlobRepository>()
			.RegisterType<IPushRepository, PushRepository>()
			.RegisterType<IInternalService, InternalService>()
			.RegisterType<ILogService, ApplicationInsightsLogService>()
			.RegisterType<IAnalyticsService, MixPanelService>()
			;

			foreach (var item in GetAssignsFromInterface(typeof(IEntityRepository<>)))
				container.RegisterType(item.Key, item.Value);
			foreach (var item in GetAssignsFromInterface(typeof(IQueueRepository<>)))
				container.RegisterType(item.Key, item.Value);

			#region Gets
			{
				var types = GetAssignsFromInterface(typeof(IQueryBaseHandler<,>));
				foreach (var item in types)
				{
					var interfaz = item.Key;
					var clase = item.Value;
					var needSave = false;

					container.RegisterType(interfaz, clase, "GetHandlers");
					var handlerName = "GetHandlers";

					{
						var attribute = clase.GetCustomAttribute<XpAnalyticsAttribute>();
						if (attribute != null)
						{
							container.RegisterType(
								interfaz,
								typeof(AnalyticsQueryHandlerDecorator<,>).MakeGenericType(interfaz.GenericTypeArguments),
								"AnalyticsHandler",
								new InjectionConstructor(
									new ResolvedParameter(interfaz, handlerName),
									new ResolvedParameter<ISessionData>(),
									new ResolvedParameter<IUnitOfWork>(),
									new ResolvedParameter<IAnalyticsService>(),
									new InjectionParameter(attribute.RelatedClass),
									new InjectionParameter(attribute.RelatedMethod)
								));
							handlerName = "AnalyticsHandler";

							needSave = true;
						}
					}

					{
						var attribute = clase.GetCustomAttribute<XpLogAttribute>();
						if (attribute != null)
						{
							container.RegisterType(
								interfaz,
								typeof(LogQueryHandlerDecorator<,>).MakeGenericType(interfaz.GenericTypeArguments),
								"LogHandler",
								new InjectionConstructor(
									new ResolvedParameter(interfaz, handlerName),
									new ResolvedParameter<ISessionData>(),
									new ResolvedParameter<IUnitOfWork>(),
									new ResolvedParameter<IEntityRepository<Log>>(),
									new InjectionParameter(attribute.RelatedClass),
									new InjectionParameter(attribute.RelatedMethod)
								));
							handlerName = "LogHandler";

							needSave = true;
						}
					}

					if (needSave)
					{
						container.RegisterType(
							interfaz,
							typeof(ContextQueryHandlerDecorator<,>).MakeGenericType(interfaz.GenericTypeArguments),
							"ContextHandler",
							new InjectionConstructor(
								new ResolvedParameter(interfaz, handlerName),
								new ResolvedParameter<IUnitOfWork>()
							));
						handlerName = "ContextHandler";
					}

					container.RegisterType(
						interfaz,
						typeof(ErrorQueryHandlerDecorator<,>).MakeGenericType(interfaz.GenericTypeArguments),
						new InjectionConstructor(
							new ResolvedParameter(interfaz, handlerName)
						));
				}
				var precessedArguments = types
					.Select(x => x.Key.GetGenericArguments())
					.Select(x => new KeyValuePair<string, string>(x[0].FullName, x[1].FullName));

				// Implicit GetId
				var getIdArguments = GetAssignsFromInterface(typeof(IGetIdArgumentsBase<,>))
					.Where(x => !precessedArguments.Select(y => y.Key).Contains(x.Value.FullName));
				foreach (var item in getIdArguments)
				{
					var classArguments = item.Value;
					var classResult = item.Key.GetGenericArguments()[0];
					var classEntity = item.Key.GetGenericArguments()[1];

					var interfaz = typeof(IQueryBaseHandler<,>).MakeGenericType(classArguments, classResult);
					var clase = typeof(GetIdImplicitHandler<,,>).MakeGenericType(classArguments, classResult, classEntity);

					container.RegisterType(interfaz, clase, "GetHandlers");
					container.RegisterType(
						interfaz,
						typeof(ErrorQueryHandlerDecorator<,>).MakeGenericType(interfaz.GenericTypeArguments),
						new InjectionConstructor(
							new ResolvedParameter(interfaz, "GetHandlers")
						));
				}
			}
			#endregion Gets

			#region Services
			{
				var types = GetAssignsFromInterface(typeof(IServiceBaseHandler<>));
				foreach (var item in types)
				{
					var interfaz = item.Key;
					var clase = item.Value;

					container.RegisterType(interfaz, clase, "CommandHandlers");
					var handlerName = "CommandHandlers";

					{
						var attribute = clase.GetCustomAttribute<XpAnalyticsAttribute>();
						if (attribute != null)
						{
							container.RegisterType(
								interfaz,
								typeof(AnalyticsServiceHandlerDecorator<>).MakeGenericType(interfaz.GenericTypeArguments),
								"AnalyticsHandler",
								new InjectionConstructor(
									new ResolvedParameter(interfaz, handlerName),
									new ResolvedParameter<ISessionData>(),
									new ResolvedParameter<IUnitOfWork>(),
									new ResolvedParameter<IAnalyticsService>(),
									new InjectionParameter(attribute.RelatedClass),
									new InjectionParameter(attribute.RelatedMethod)
								));
							handlerName = "AnalyticsHandler";
						}
					}

					{
						var attribute = clase.GetCustomAttribute<XpLogAttribute>();
						if (attribute != null)
						{
							container.RegisterType(
								interfaz,
								typeof(LogServiceHandlerDecorator<>).MakeGenericType(interfaz.GenericTypeArguments),
								"LogHandler",
								new InjectionConstructor(
									new ResolvedParameter(interfaz, handlerName),
									new ResolvedParameter<ISessionData>(),
									new ResolvedParameter<IUnitOfWork>(),
									new ResolvedParameter<IEntityRepository<Log>>(),
									new InjectionParameter(attribute.RelatedClass),
									new InjectionParameter(attribute.RelatedMethod)
								));
							handlerName = "LogHandler";
						}
					}

					container.RegisterType(
						interfaz,
						typeof(ContextServiceHandlerDecorator<>).MakeGenericType(interfaz.GenericTypeArguments),
						"ServiceHandler",
						new InjectionConstructor(
							new ResolvedParameter(interfaz, handlerName),
							new ResolvedParameter<IUnitOfWork>()
						));
					handlerName = "ServiceHandler";

					container.RegisterType(
						interfaz,
						typeof(ErrorServiceHandlerDecorator<>).MakeGenericType(interfaz.GenericTypeArguments),
						new InjectionConstructor(
							new ResolvedParameter(interfaz, handlerName)
						));
				}
				var precessedArguments = types
					.SelectMany(x => x.Key.GetGenericArguments()
						.Select(y => y.FullName)
					);

				// Implicit Create
				var createArguments = GetAssignsFromInterface(typeof(ICreateArgumentsBase<>))
					.Where(x => !precessedArguments.Contains(x.Value.FullName));
				foreach (var item in createArguments)
				{
					var interfaz = typeof(IServiceBaseHandler<>).MakeGenericType(item.Value);
					var clase = typeof(CreateImplicitHandler<,>).MakeGenericType(item.Value, item.Key.GetGenericArguments().FirstOrDefault());

					container.RegisterType(interfaz, clase, "CommandHandlers");
					container.RegisterType(
						interfaz,
						typeof(ContextServiceHandlerDecorator<>).MakeGenericType(interfaz.GenericTypeArguments),
						"ServiceHandler",
						new InjectionConstructor(
							new ResolvedParameter(interfaz, "CommandHandlers"),
							new ResolvedParameter<IUnitOfWork>()
						));
					container.RegisterType(
						interfaz,
						typeof(ErrorServiceHandlerDecorator<>).MakeGenericType(interfaz.GenericTypeArguments),
						new InjectionConstructor(
							new ResolvedParameter(interfaz, "ServiceHandler")
						));
				}

				// Implicit Update
				var updateArguments = GetAssignsFromInterface(typeof(IUpdateArgumentsBase<>))
					.Where(x => !precessedArguments.Contains(x.Value.FullName));
				foreach (var item in updateArguments)
				{
					var interfaz = typeof(IServiceBaseHandler<>).MakeGenericType(item.Value);
					var clase = typeof(UpdateImplicitHandler<,>).MakeGenericType(item.Value, item.Key.GetGenericArguments().FirstOrDefault());

					container.RegisterType(interfaz, clase, "CommandHandlers");
					container.RegisterType(
						interfaz,
						typeof(ContextServiceHandlerDecorator<>).MakeGenericType(interfaz.GenericTypeArguments),
						"ServiceHandler",
						new InjectionConstructor(
							new ResolvedParameter(interfaz, "CommandHandlers"),
							new ResolvedParameter<IUnitOfWork>()
						));
					container.RegisterType(
						interfaz,
						typeof(ErrorServiceHandlerDecorator<>).MakeGenericType(interfaz.GenericTypeArguments),
						new InjectionConstructor(
							new ResolvedParameter(interfaz, "ServiceHandler")
						));
				}

				// Implicit Delete
				var deleteArguments = GetAssignsFromInterface(typeof(IDeleteArgumentsBase<>))
					.Where(x => !precessedArguments.Contains(x.Value.FullName));
				foreach (var item in deleteArguments)
				{
					var interfaz = typeof(IServiceBaseHandler<>).MakeGenericType(item.Value);
					var clase = typeof(DeleteImplicitHandler<,>).MakeGenericType(item.Value, item.Key.GetGenericArguments().FirstOrDefault());

					container.RegisterType(interfaz, clase, "CommandHandlers");
					container.RegisterType(
						interfaz,
						typeof(ContextServiceHandlerDecorator<>).MakeGenericType(interfaz.GenericTypeArguments),
						"ServiceHandler",
						new InjectionConstructor(
							new ResolvedParameter(interfaz, "CommandHandlers"),
							new ResolvedParameter<IUnitOfWork>()
						));
					container.RegisterType(
						interfaz,
						typeof(ErrorServiceHandlerDecorator<>).MakeGenericType(interfaz.GenericTypeArguments),
						new InjectionConstructor(
							new ResolvedParameter(interfaz, "ServiceHandler")
						));
				}
			}
			#endregion Services

			return container;
		}
		#endregion CreateContainer

		#region Resolve
		public static T Resolve<T>()
		{
			try
			{
				var result = Container.Resolve<T>();
				return result;
			}
			catch (Exception ex)
			{
				Debug.WriteLine(string.Format("DI Server: {0}", ex.Message));
				throw;
			}
		}
		#endregion Resolve

		#region InitializeWebApi
		public static void InitializeWebApi(HttpConfiguration config)
		{
			config.DependencyResolver = new UnityDependencyResolver(Container);
		}
		#endregion InitializeWebApi

		#region Initialize
		public static void Initialize()
		{
			_Container = CreateContainer();
		}
		#endregion Initialize
	}
}
