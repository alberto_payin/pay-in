﻿	using PayIn.Application.Dto.Internal.Arguments.PaymentMedia;
using PayIn.Domain.Security;
using PayIn.Web.Security;
using System.Threading.Tasks;
using System.Web.Http;
using Xp.Application.Dto;
using Xp.DistributedServices.ModelBinder;

namespace PayIn.DistributedServices.Internal.Controllers.Internal
{
	[RoutePrefix("Internal/PaymentMedia")]
	[XpAuthorize(
		ClientIds = AccountClientId.AndroidNative,
		Roles = AccountRoles.User
	)]
	public class PaymentMediaController : ApiController
	{
		#region POST /WebCard
		[HttpPost]
		[Route("WebCard")]
		public async Task<dynamic> CreateWebCard(
			PaymentMediaCreateWebCardArguments arguments,
			[Injection] IServiceBaseHandler<PaymentMediaCreateWebCardArguments> handler
		)
		{
			var item = await handler.ExecuteAsync(arguments);
			return item;
		}
		#endregion POST /WebCard

		#region POST /WebCardRefundSabadell
		[HttpPost]
		[Route("WebCardRefundSabadell")]
		public async Task<dynamic> WebCardRefundSabadell(
			PaymentMediaWebCardRefundSabadellArguments arguments,
			[Injection] IServiceBaseHandler<PaymentMediaWebCardRefundSabadellArguments> handler
		)
		{
			var item = await handler.ExecuteAsync(arguments);
			return item;
		}
		#endregion POST /WebCardRefundSabadell

		#region POST /Pay
		[HttpPost]
		[Route("Pay")]
		public async Task<dynamic> Pay(
			PaymentMediaPayArguments arguments,
			[Injection] IServiceBaseHandler<PaymentMediaPayArguments> handler
		)
		{
			var item = await handler.ExecuteAsync(arguments);
			return item;
		}
		#endregion POST /Pay

		#region POST /PaySabadell
		[HttpPost]
		[Route("PaySabadell")]
		public async Task<dynamic> PaySabadell(
			PaymentMediaPaySabadellArguments arguments,
			[Injection] IServiceBaseHandler<PaymentMediaPaySabadellArguments> handler
		)
		{
			var item = await handler.ExecuteAsync(arguments);
			return item;
		}
		#endregion POST /PaySabadell

		#region POST /Refund
		[HttpPost]
		[Route("Refund")]
		public async Task<dynamic> Refund(
			PaymentMediaRefundArguments arguments,
			[Injection] IServiceBaseHandler<PaymentMediaRefundArguments> handler
		)
		{
			var item = await handler.ExecuteAsync(arguments);
			return item;
		}
		#endregion POST /Refund

		#region DELETE /{id:int}
		[HttpDelete]
		[Route("{id:int}")]
		public async Task<dynamic> Delete(
				int id,
				[FromUri] PaymentMediaDeleteArguments command,
				[Injection] IServiceBaseHandler<PaymentMediaDeleteArguments> handler
		)
		{
			command.PublicId = id;

			var result = await handler.ExecuteAsync(command);
			return result;
		}
		#endregion DELETE /{id:int}
	}
}
