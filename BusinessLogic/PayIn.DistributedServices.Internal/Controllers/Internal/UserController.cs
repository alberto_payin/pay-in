﻿using PayIn.Application.Dto.Internal.Arguments.User;
using PayIn.Domain.Security;
using PayIn.Web.Security;
using System.Threading.Tasks;
using System.Web.Http;
using Xp.Application.Dto;
using Xp.DistributedServices.ModelBinder;

namespace PayIn.DistributedServices.Internal.Controllers.Internal
{
	[RoutePrefix("Internal/User")]
	[XpAuthorize(
		ClientIds = AccountClientId.AndroidNative + "," + AccountClientId.Web,
		Roles = AccountRoles.User
	)]
	public class UserController : ApiController
	{
		#region POST /
		[HttpPost]
		[Route("")]
		public async Task<dynamic> Create(
			UserCreateArguments arguments,
			[Injection] IServiceBaseHandler<UserCreateArguments> handler
		)
		{
			var item = await handler.ExecuteAsync(arguments);
			return null;
		}
		#endregion POST /

		#region POST /Pin
		[HttpPost]
		[Route("Pin")]
		[XpAuthorize(
			ClientIds = AccountClientId.AndroidNative,
			Roles = AccountRoles.User
		)]
		public async Task<dynamic> UpdatePin(
			UserUpdatePinArguments arguments,
			[Injection] IServiceBaseHandler<UserUpdatePinArguments> handler
		)
		{
			var item = await handler.ExecuteAsync(arguments);
			return null;
		}
		#endregion POST /Pin

		#region GET /CheckPin
		[HttpGet]
		[Route("CheckPin")]
		public async Task<dynamic> CheckPin(
			[FromUri] UserCheckPinArguments arguments,
			[Injection] IServiceBaseHandler<UserCheckPinArguments> handler
		)
		{
			var item = await handler.ExecuteAsync(arguments);
			return new { CheckPin = item };
		}
		#endregion GET /CheckPin

		#region GET /HasPayment
		[HttpGet]
		[Route("HasPayment")]
		public async Task<dynamic> HasPayment(
			[FromUri]   UserHasPaymentArguments arguments,
			[Injection] IServiceBaseHandler<UserHasPaymentArguments> handler
		)
		{
			var item = await handler.ExecuteAsync(arguments);
			return new { HasPayment = item };
		}
		#endregion GET /HasPayment
	}
}
