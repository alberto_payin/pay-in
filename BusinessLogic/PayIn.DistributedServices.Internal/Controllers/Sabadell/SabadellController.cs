﻿using PayIn.Application.Dto.Internal.Arguments.PaymentMedia;
using PayIn.Domain.Security;
using PayIn.Web.Security;
using System.Threading.Tasks;
using System.Web.Http;
using Xp.Application.Dto;
using Xp.DistributedServices.ModelBinder;

namespace PayIn.DistributedServices.Internal.Controllers.Sabadell
{
	[RoutePrefix("Sabadell")]
	public class SabadellController : ApiController
	{
		#region POST /WebCard
		[HttpPost]
		[Route("WebCard")]
		public async Task<dynamic> WebCard(
			PaymentMediaCreateWebCardSabadellArguments arguments,
			[Injection] IServiceBaseHandler<PaymentMediaCreateWebCardSabadellArguments> handler
		)
		{
			await handler.ExecuteAsync(arguments);
			return new { };
		}
		#endregion POST /WebCard

		#region POST /WebCardRefund
		[HttpPost]
		[Route("WebCardRefund")]
		public async Task<dynamic> WebCardRefund(
			PaymentMediaCreateWebCardRefundSabadellArguments arguments,
			[Injection] IServiceBaseHandler<PaymentMediaCreateWebCardRefundSabadellArguments> handler
		)
		{
			var result = await handler.ExecuteAsync(arguments);
			return result;
		}
		#endregion POST /WebCardRefund
	}
}
