﻿using PayIn.Application.Dto.Payments.Arguments.Liquidation;
using PayIn.Application.Dto.Payments.Results.Liquidation;
using PayIn.Domain.Security;
using PayIn.Web.Security;
using System.Threading.Tasks;
using System.Web.Http;
using Xp.Application.Dto;
using Xp.DistributedServices.ModelBinder;

namespace PayIn.DistributedServices.Payments.Controllers.Api
{
	[RoutePrefix("Api/Liquidation")]
	[XpAuthorize(
		ClientIds = AccountClientId.Web,
		Roles = AccountRoles.Superadministrator + "," + AccountRoles.CommercePayment
	)]
	public class LiquidationController : ApiController
	{
		#region GET /
		[HttpGet]
		[Route("")]
		public async Task<ResultBase<LiquidationGetAllResult>> GetAll(
			[FromUri] LiquidationGetAllArguments arguments,
			[Injection] IQueryBaseHandler<LiquidationGetAllArguments, LiquidationGetAllResult> handler
		)
		{
			var result = await handler.ExecuteAsync(arguments);
					return result;
		}
		#endregion GET /

		#region GET /Liquidations
		[HttpGet]
		[Route("Liquidations/")]
		public async Task<ResultBase<LiquidationPayResult>> LiquidationPay(
			[FromUri] LiquidationPayArguments arguments,
			[Injection] IQueryBaseHandler<LiquidationPayArguments, LiquidationPayResult> handler
		)
		{
			var result = await handler.ExecuteAsync(arguments);
			return result;
		}
		#endregion GET /Liquidations

		#region PUT /Change
		[HttpPut]
		[Route("Change/")]
		public async Task<dynamic> Put(
			[FromUri] LiquidationChangeArguments arguments,
			[Injection] IServiceBaseHandler<LiquidationChangeArguments> handler
		)
		{
			var item = await handler.ExecuteAsync(arguments);
			return new { Id = item.Id };
		}
		#endregion PUT /Change

		#region PUT /Open
		[HttpPut]
		[Route("Open/")]
		public async Task<dynamic> Put(
			[FromUri] LiquidationOpenArguments arguments,
			[Injection] IServiceBaseHandler<LiquidationOpenArguments> handler
		)
		{
			var item = await handler.ExecuteAsync(arguments);
			return new { Id = item.Id };
		}
		#endregion PUT /Open

		#region PUT /ConfirmPay
		[HttpPut]
		[Route("ConfirmPay/{id:int}")]
		public async Task<dynamic> Put(
			[FromUri] LiquidationConfirmPayArguments arguments,
			[Injection] IServiceBaseHandler<LiquidationConfirmPayArguments> handler
		)
		{
			var item = await handler.ExecuteAsync(arguments);
			return new { Id = item.Id };
		}
		#endregion PUT /ConfirmPay

		#region PUT /ConfirmUnpay
		[HttpPut]
		[Route("ConfirmUnpay/{id:int}")]
		public async Task<dynamic> Put(
			[FromUri] LiquidationConfirmUnpayArguments arguments,
			[Injection] IServiceBaseHandler<LiquidationConfirmUnpayArguments> handler
		)
		{
			var item = await handler.ExecuteAsync(arguments);
			return new { Id = item.Id };
		}
		#endregion PUT /ConfirmUnpay
	}
}
