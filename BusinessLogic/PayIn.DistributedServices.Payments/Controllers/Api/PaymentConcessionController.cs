﻿using PayIn.Application.Dto.Payments.Arguments.PaymentConcession;
using PayIn.Application.Dto.Payments.Results.PaymentConcession;
using PayIn.Domain.Security;
using PayIn.Web.Security;
using System.Threading.Tasks;
using System.Web.Http;
using Xp.Application.Dto;
using Xp.DistributedServices.ModelBinder;

namespace PayIn.DistributedServices.Payments.Controllers.Api
{
	[RoutePrefix("Api/PaymentConcession")]
	[XpAuthorize(
		ClientIds = AccountClientId.Web,
		Roles = AccountRoles.User + "," + AccountRoles.CommercePayment + "," + AccountRoles.Superadministrator
	)]
	public class PaymentConcessionController : ApiController
	{
		#region GET /{filter?}
		//[HttpGet]
		//[Route("{filter?}")]
		//[XpAuthorize(
		//ClientIds = AccountClientId.Web,
		//Roles = AccountRoles.Superadministrator
		//)]
		//public async Task<dynamic> GetAll(
		//[FromUri] PaymentConcessionGetAllArguments arguments,
		//[Injection] IQueryBaseHandler<PaymentConcessionGetAllArguments, PaymentConcessionGetAllResult> handler
		//)
		//{
		//	var result = await handler.ExecuteAsync(arguments);
		//	return result;
		//}		
		#endregion GET /

		#region POST /Create
		[HttpPost]
		[Route("Create")]
		public async Task<dynamic> Create(
			PaymentConcessionCreateArguments arguments,
			[Injection] IServiceBaseHandler<PaymentConcessionCreateArguments> handler
		)
		{
			var result = (await handler.ExecuteAsync(arguments));
			return new { Id = result.Id };
		}
		#endregion POST /Create

		#region GET /{id:int}
		[HttpGet]
		[Route("{id:int}")]		
		public async Task<ResultBase<PaymentConcessionGetResult>> Get(
			[FromUri] PaymentConcessionGetArguments argument,
			[Injection] IQueryBaseHandler<PaymentConcessionGetArguments, PaymentConcessionGetResult> handler)
		{
			var result = await handler.ExecuteAsync(argument);
			return result;
		}
		#endregion GET /{id:int}

		#region PUT /{id:int}
		[HttpPut]
		[Route("{id:int}")]
		[XpAuthorize(Roles = AccountRoles.Superadministrator)]
		public async Task<dynamic> Put(
			PaymentConcessionUpdateArguments command,
		[Injection] IServiceBaseHandler<PaymentConcessionUpdateArguments> handler
		)
		{
			var item = await handler.ExecuteAsync(command);
			return new { Id = item.Id };

		}
		#endregion PUT /{id:int}

		#region PUT /UpdateCommerce/{id:int}
		[HttpPut]
		[Route("UpdateCommerce/{id:int}")]
		[XpAuthorize(Roles = AccountRoles.CommercePayment)]
		public async Task<dynamic> Put(
			PaymentConcessionUpdateCommerceArguments command,
		[Injection] IServiceBaseHandler<PaymentConcessionUpdateCommerceArguments> handler
		)
		{
			var item = await handler.ExecuteAsync(command);
			return new { Id = item.Id };

		}
		#endregion PUT /UpdateCommerce/{id:int}

		#region GET /GetCommerce/{id:int}
		[HttpGet]
		[Route("GetCommerce/{id:int}")]
		[XpAuthorize(
			ClientIds = AccountClientId.Web,
			Roles = AccountRoles.CommercePayment
		)]
		public async Task<ResultBase<PaymentConcessionGetCommerceResult>> Get(
			[FromUri] PaymentConcessionGetCommerceArguments argument,
			[Injection] IQueryBaseHandler<PaymentConcessionGetCommerceArguments, PaymentConcessionGetCommerceResult> handler)
		{
			var result = await handler.ExecuteAsync(argument);
			return result;
		}
		#endregion GET /GetCommerce/{id:int}

	}
}
