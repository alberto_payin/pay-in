﻿using PayIn.Application.Dto.Payments.Arguments.Payments;
using PayIn.Application.Dto.Payments.Results.Payments;
using PayIn.Domain.Security;
using PayIn.Web.Security;
using System.Threading.Tasks;
using System.Web.Http;
using Xp.Application.Dto;
using Xp.DistributedServices.ModelBinder;

namespace PayIn.DistributedServices.Payments.Controllers.Api
{
	[RoutePrefix("Api/Payment")]
	[XpAuthorize(
		ClientIds = AccountClientId.Web,
		Roles = AccountRoles.User + "," + AccountRoles.CommercePayment + "," + AccountRoles.Superadministrator + "," + AccountRoles.PaymentWorker
	)]
	public class PaymentController : ApiController
	{
		#region GET /
		[HttpGet]
		[Route("")]
		public async Task<ResultBase<PaymentsGetAllResult>> GetAll(
			[FromUri] PaymentGetAllArguments arguments,
			[Injection] IQueryBaseHandler<PaymentGetAllArguments, PaymentsGetAllResult> handler
		)
		{
			var result = await handler.ExecuteAsync(arguments);
			return result;
		}
		#endregion GET /

		#region GET /Charges
		[HttpGet]
		[Route("Charges")]
		public async Task<ResultBase<PaymentGetChargesResult>> GetCharges(
			[FromUri] PaymentGetChargesArguments arguments,
			[Injection] IQueryBaseHandler<PaymentGetChargesArguments, PaymentGetChargesResult> handler
		)
		{
			var result = await handler.ExecuteAsync(arguments);
			return result;
		}
		#endregion GET /Charges

		#region GET /Unliquidated
		[HttpGet]
		[Route("Unliquidated")]
		public async Task<ResultBase<UnliquidatedPaymentResult>> UnliquidatedPayment(
			[FromUri] UnliquidatedPaymentArguments arguments,
			[Injection] IQueryBaseHandler<UnliquidatedPaymentArguments, UnliquidatedPaymentResult> handler
		)
		{
			var result = await handler.ExecuteAsync(arguments);
			return result;
		}
		#endregion GET /Unliquidated

		#region GET /Graph
		[HttpGet]
		[Route("Graph/")]
		public async Task<ResultBase<PaymentsGetGraphResult>> Graph(
			[FromUri] PaymentGetGraphArguments arguments,
			[Injection] IQueryBaseHandler<PaymentGetGraphArguments, PaymentsGetGraphResult> handler
		)
		{
			var result = await handler.ExecuteAsync(arguments);
			return result;
		}
		#endregion GET /Graph

		#region GET /LiquidationPayments
		[HttpGet]
		[Route("LiquidationPayments/{liquidationId?}")]
		public async Task<ResultBase<PaymentsGetAllByLiquidationResult>> LiquidationPayments(
			[FromUri] PaymentGetAllByLiquidationArguments arguments,
			[Injection] IQueryBaseHandler<PaymentGetAllByLiquidationArguments, PaymentsGetAllByLiquidationResult> handler
		)
		{
			var result = await handler.ExecuteAsync(arguments);
			return result;
		}
		#endregion GET /LiquidationPayments
	}
}
