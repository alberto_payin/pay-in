﻿using PayIn.Application.Dto.Payments.Arguments.Ticket;
using PayIn.Application.Dto.Payments.Results.Ticket;
using PayIn.Domain.Security;
using PayIn.Web.Security;
using System;
using System.Threading.Tasks;
using System.Web.Http;
using Xp.Application.Dto;
using Xp.DistributedServices.ModelBinder;

namespace PayIn.DistributedServices.Payments.Controllers.Api
{
	[RoutePrefix("Api/Ticket")]
	[XpAuthorize(
		ClientIds = AccountClientId.Web,
		Roles = AccountRoles.User + "," + AccountRoles.CommercePayment + "," + AccountRoles.Superadministrator + "," + AccountRoles.PaymentWorker
	)]
	public class TicketController : ApiController
	{
		#region GET /{id:int}
		[HttpGet]
		[Route("{id:int}")]
		public async Task<ResultBase<TicketGetResult>> Get(
			int id,
			[FromUri] TicketGetArguments arguments,
			[Injection] IQueryBaseHandler<TicketGetArguments, TicketGetResult> handler
		)
		{
			arguments.Id = id;

			var result = await handler.ExecuteAsync(arguments);
			return result;
		}
		#endregion GET /{id:int}

		#region GET /
		[HttpGet]
		[Route("")]
		public async Task<ResultBase<TicketGetAllResult>> GetAll(
			[FromUri] TicketGetAllArguments arguments,
			[Injection] IQueryBaseHandler<TicketGetAllArguments, TicketGetAllResult> handler
		)
		{
			var result = await handler.ExecuteAsync(arguments);
			return result;
		}
		#endregion GET /

		#region GET /Details
		[HttpGet]
		[Route("Details/{id}")]
		public async Task<ResultBase<TicketGetDetailsResult>> GetAll(
			[FromUri] TicketGetDetailsArguments arguments,
			[Injection] IQueryBaseHandler<TicketGetDetailsArguments, TicketGetDetailsResult> handler
		)
		{
			var result = await handler.ExecuteAsync(arguments);
			return result;
		}
		#endregion GET /Details

		#region PUT /
		[HttpPut]
		[Route("{id:int}")]
		public async Task<dynamic> Put(
				TicketUpdateArguments command,
				[Injection] IServiceBaseHandler<TicketUpdateArguments> handler
		)
		{
			var item = await handler.ExecuteAsync(command);
			return item;
		}
		#endregion PUT /

		#region DELETE /
		[HttpDelete]
		[Route("{id:int}")]
		public async Task<dynamic> Delete(
				int id,
				[FromUri] TicketDeleteArguments command,
				[Injection] IServiceBaseHandler<TicketDeleteArguments> handler
		)
		{
			var result = await handler.ExecuteAsync(command);
			return result;
		}
		#endregion DELETE /

		#region GET /Graph
		[HttpGet]
		[Route("Graph/")]
		public async Task<ResultBase<TicketGraphResult>> Graph(
			[FromUri] TicketGraphArguments arguments,
			[Injection] IQueryBaseHandler<TicketGraphArguments, TicketGraphResult> handler
		)
		{
			var result = await handler.ExecuteAsync(arguments);
			return result;
		}
		#endregion GET /Graph
	}
}
