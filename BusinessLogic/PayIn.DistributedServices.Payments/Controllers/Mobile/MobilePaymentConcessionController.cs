﻿using PayIn.Application.Dto.Payments.Arguments.PaymentConcession;
using PayIn.Application.Dto.Payments.Results.PaymentConcession;
using PayIn.Domain.Security;
using PayIn.Web.Security;
using System.Threading.Tasks;
using System.Web.Http;
using Xp.Application.Dto;
using Xp.DistributedServices.ModelBinder;

namespace PayIn.DistributedServices.Payments.Controllers.Mobile
{
	[RoutePrefix("Mobile/PaymentConcession")]
	[XpAuthorize(
		ClientIds = AccountClientId.AndroidNative,
		Roles = AccountRoles.CommercePayment + "," + AccountRoles.PaymentWorker
	)]
	public class MobilePaymentConcessionController : ApiController
	{
		#region GET /v1
		[HttpGet]
		[Route("v1")]
		public async Task<ResultBase<PaymentConcessionMobileGetAllResult>> Get(
			[FromUri] PaymentConcessionMobileGetAllArguments arguments,
			[Injection] IQueryBaseHandler<PaymentConcessionMobileGetAllArguments, PaymentConcessionMobileGetAllResult> handler
		)
		{
			var result = await handler.ExecuteAsync(arguments);
			return result;
		}
		#endregion GET /v1
	}
}
