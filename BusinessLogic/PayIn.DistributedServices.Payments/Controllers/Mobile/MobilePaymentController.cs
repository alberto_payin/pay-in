﻿using PayIn.Application.Dto.Payments.Arguments.Ticket;
using PayIn.Domain.Security;
using PayIn.Web.Security;
using System.Threading.Tasks;
using System.Web.Http;
using Xp.Application.Dto;
using Xp.DistributedServices.ModelBinder;

namespace PayIn.DistributedServices.Payments.Controllers
{
	[RoutePrefix("Mobile/Payment")]
	[XpAuthorize(
		ClientIds = AccountClientId.AndroidNative,
		Roles = AccountRoles.CommercePayment + "," + AccountRoles.PaymentWorker
	)]
	public class MobilePaymentController : ApiController
	{
		#region POST /v1/Refund
		[HttpPost]
		[Route("v1/Refund")]
		public async Task<dynamic> Refund(
			PaymentRefundArguments arguments,
			[Injection] IServiceBaseHandler<PaymentRefundArguments> handler
		)
		{
			var item = await handler.ExecuteAsync(arguments);
			return new { Id = item.Id };
		}
		#endregion POST /v1/Refund
	}
}
