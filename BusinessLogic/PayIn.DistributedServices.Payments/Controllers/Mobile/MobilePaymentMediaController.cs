﻿using PayIn.Application.Dto.Payments.Arguments.PaymentMedia;
using PayIn.Application.Dto.Payments.Results.PaymentMedia;
using PayIn.Domain.Security;
using PayIn.Web.Security;
using System.Threading.Tasks;
using System.Web.Http;
using Xp.Application.Dto;
using Xp.DistributedServices.ModelBinder;

namespace PayIn.DistributedServices.Payments.Controllers
{
	[RoutePrefix("Mobile/PaymentMedia")]
	[XpAuthorize(
		ClientIds = AccountClientId.AndroidNative,
		Roles = AccountRoles.User
	)]
	public class MobilePaymentMediaController : ApiController
	{
		#region POST /v1/WebCard
		[HttpPost]
		[Route("v1/WebCard")]
		public async Task<dynamic> CreateWebCard(
			PaymentMediaMobileCreateWebCardArguments command,
			[Injection] IServiceBaseHandler<PaymentMediaMobileCreateWebCardArguments> handler
		)
		{
			var result = await handler.ExecuteAsync(command);
			return result;
		}
		#endregion POST /v1/WebCard

		#region GET /
		[HttpGet]
		[Route("v1")]
		public async Task<ResultBase<PaymentMediaMobileGetAllResult>> Selector(
			[FromUri] PaymentMediaMobileGetAllArguments arguments,
			[Injection] IQueryBaseHandler<PaymentMediaMobileGetAllArguments, PaymentMediaMobileGetAllResult> handler
		)
		{
			var result = await handler.ExecuteAsync(arguments);
			return result;
		}
		#endregion GET /

		#region DELETE /{id:int}
		[HttpDelete]
		[Route("v1/{id:int}")]
		public async Task<dynamic> Delete(
				int id,
				[FromUri] PaymentMediaMobileDeleteArguments command,
				[Injection] IServiceBaseHandler<PaymentMediaMobileDeleteArguments> handler
		)
		{
			command.Id = id;

			var result = await handler.ExecuteAsync(command);
			return result;
		}
		#endregion DELETE /{id:int}
	}
}
