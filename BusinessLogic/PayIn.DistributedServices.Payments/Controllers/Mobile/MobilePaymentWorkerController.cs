﻿using PayIn.Application.Dto.Payments.Arguments.PaymentWorker;
using PayIn.Domain.Security;
using PayIn.Web.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Xp.Application.Dto;
using Xp.DistributedServices.ModelBinder;

namespace PayIn.DistributedServices.Payments.Controllers.Mobile
{
	[RoutePrefix("Mobile/PaymentWorker")]
	[XpAuthorize(
		ClientIds = AccountClientId.AndroidNative,
		Roles = AccountRoles.User
	)]
	public class MobilePaymentWorkerController: ApiController
	{
		#region PUT /v1/AcceptAssignment/{id:int}
		[HttpPut]
		[Route("v1/{id:int}")]
		public async Task<dynamic> AcceptAssignment(
			int Id,
			PaymentWorkerMobileAcceptAssignmentArguments command,
			[Injection] IServiceBaseHandler<PaymentWorkerMobileAcceptAssignmentArguments> handler
		)
		{
			command.Id = Id;
			var item = await handler.ExecuteAsync(command);
			return new { Id = item.Id };
		}
		#endregion PUT /v1/AcceptAssignment/{id:int}

		#region PUT /v1/RejectAssignment/{id:int}
		[HttpPut]
		[Route("v1/RejectAssignment/{id:int}")]
		public async Task<dynamic> RejectAssignment(
			int id,
			PaymentWorkerMobileRejectAssignmentArguments arguments,
			[Injection] IServiceBaseHandler<PaymentWorkerMobileRejectAssignmentArguments> handler
		)
		{
			arguments.Id = id;
			var result = await handler.ExecuteAsync(arguments);
			return new { Id = result.Id };
		}
		#endregion PUT /v1/RejectAssignment/{id:int}
	}
}
