﻿using PayIn.Application.Dto.Payments.Arguments.Ticket;
using PayIn.Application.Dto.Payments.Results.Ticket;
using PayIn.Domain.Security;
using PayIn.Web.Security;
using System.Threading.Tasks;
using System.Web.Http;
using Xp.Application.Dto;
using Xp.DistributedServices.ModelBinder;

namespace PayIn.DistributedServices.Payments.Controllers
{
	[RoutePrefix("Mobile/Ticket")]
	[XpAuthorize(
		ClientIds = AccountClientId.AndroidNative,
		Roles = AccountRoles.User + "," + AccountRoles.CommercePayment  + "," +  AccountRoles.PaymentWorker
	)]
	public class MobileTicketController : ApiController
	{
		#region GET /v1/{id:int}
		[HttpGet]
		[Route("v1/{id:int}")]
		public async Task<ResultBase<TicketMobileGetResult>> Get(
			int id,
			[FromUri] TicketMobileGetArguments arguments,
			[Injection] IQueryBaseHandler<TicketMobileGetArguments, TicketMobileGetResult> handler
		)
		{
			arguments.Id = id;
			var result = await handler.ExecuteAsync(arguments);
			return result;
		}
		#endregion GET /v1/{id:int}

		#region GET /v1/WithPaymentMedias/{id:int}
		[HttpGet]
		[Route("v1/WithPaymentMedias/{id:int}")]
		public async Task<ResultBase<TicketMobileGetWithPaymentMediasResult>> WithPaymentMedias(
			int id,
			[FromUri] TicketMobileGetWithPaymentMediasArguments arguments,
			[Injection] IQueryBaseHandler<TicketMobileGetWithPaymentMediasArguments, TicketMobileGetWithPaymentMediasResult> handler
		)
		{
			arguments.Id = id;
			var result = await handler.ExecuteAsync(arguments);
			return result;
		}
		#endregion GET /v1/WithPaymentMedias/{id:int}

		#region POST /v1
		[HttpPost]
		[Route("v1")]
		public async Task<dynamic> Post(
			TicketMobileCreateArguments arguments,
			[Injection] IServiceBaseHandler<TicketMobileCreateArguments> handler
		)
		{
			var item = await handler.ExecuteAsync(arguments);
			return new { Id = item.Id };
		}
		#endregion POST /v1

		#region POST /v1/Pay
		[HttpPost]
		[Route("v1/Pay")]
		public async Task<dynamic> Pay(
			TicketMobilePayArguments arguments,
			[Injection] IServiceBaseHandler<TicketMobilePayArguments> handler
		)
		{
			var item = await handler.ExecuteAsync(arguments);
			return new { Id = item.Id };
		}
		#endregion POST /v1/Pay

		#region DELETE /
		[HttpDelete]
		[Route("v1")]
		public async Task<dynamic> Delete(
				int id,
				[FromUri] TicketMobileDeleteArguments command,
				[Injection] IServiceBaseHandler<TicketMobileDeleteArguments> handler
		)
		{
			var result = await handler.ExecuteAsync(command);
			return result;
		}
		#endregion DELETE /
	}
}
