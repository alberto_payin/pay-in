﻿using PayIn.Application.Dto.Payments.Arguments.PaymentMedia;
using System.Threading.Tasks;
using System.Web.Http;
using Xp.Application.Dto;
using Xp.DistributedServices.ModelBinder;

namespace PayIn.DistributedServices.Payments.Controllers.Sabadell
{
	[RoutePrefix("Sabadell")]
	public class SabadellController : ApiController
	{
		#region POST /WebCard
		[HttpPost]
		[Route("WebCard")]
		public async Task<dynamic> WebCard(
			[FromBody] PaymentMediaCreateWebCardSabadellArguments arguments,
			[Injection] IServiceBaseHandler<PaymentMediaCreateWebCardSabadellArguments> handler
		)
		{
			var item = await handler.ExecuteAsync(arguments);
			return new { Id = item.Id };
		}
		#endregion POST /WebCard
	}
}
