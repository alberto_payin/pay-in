﻿using PayIn.Application.Dto.Payments.Arguments.Ticket;
using PayIn.Application.Dto.Payments.Results.Ticket;
using PayIn.Domain.Security;
using PayIn.Web.Security;
using System.Threading.Tasks;
using System.Web.Http;
using Xp.Application.Dto;
using Xp.DistributedServices.ModelBinder;

namespace PayIn.DistributedServices.Payments.Controllers
{
	[RoutePrefix("Tpv/Ticket")]
	[XpAuthorize(
		ClientIds = AccountClientId.Tpv,
		Roles = AccountRoles.PaymentWorker + "," + AccountRoles.CommercePayment
	)]
	public class TpvTicketController : ApiController
	{
		#region POST /v1
		[HttpPost]
		[Route("v1")]
		public async Task<dynamic> Post(
			TicketMobileCreateArguments arguments,
			[Injection] IServiceBaseHandler<TicketMobileCreateArguments> handler
		)
		{
			var item = await handler.ExecuteAsync(arguments);
			return new { Id = item.Id };
		}
		#endregion POST /v1
	}
}
