﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json.Linq;
using PayIn.Application.Dto.Security.Arguments;
using PayIn.Domain.Security;
using PayIn.Infrastructure.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Xp.Application.Dto;
using PayIn.Application.Dto.Arguments.Account;
using PayIn.Application.Dto.Results.Account;

namespace PayIn.DistributedServices.Security.Controllers
{
	[RoutePrefix("api/Account")]
	public class AccountController : ApiController
	{
		private readonly SecurityRepository repository = null;

		#region Authentication
		private IAuthenticationManager Authentication
		{
			get { return Request.GetOwinContext().Authentication; }
		}
		#endregion Authentication

		#region Constructors
		public AccountController()
		{
			repository = new SecurityRepository();
		}
		#endregion Constructors

		#region GET Current
		//[Authorize]
		[HttpGet]
		[Route("Current")]
		public async Task<ResultBase<AccountGetCurrentResult>> Current()
		{
			var result = await repository.GetUserIdentityAsync();
			var res = new AccountGetCurrentResult
			{
				Name = result.Name,
				Mobile = result.Mobile,
				Sex = result.Sex,
				TaxNumber = result.TaxNumber,
				TaxName = result.TaxName,
				TaxAddress = result.TaxAddress,
				Birthday = result.Birthday,
				PhotoUrl = result.PhotoUrl + (result.PhotoUrl == "" ? "" : "?now=" + DateTime.Now.ToString())
			};

			var list = new List<AccountGetCurrentResult>();
			list.Add(res);

			return new ResultBase<AccountGetCurrentResult> { Data = list };
		}
		#endregion GET Current

		#region POST api/Account
		[AllowAnonymous]
		[HttpPost]
		[Route("")]
		public async Task Post(
			AccountRegisterArguments userModel
		)
		{		
			await repository.CreateUserAsync(userModel, AccountRoles.User);
		}
		#endregion POST api/Account

		#region POST api/Account/ConfirmEmail
		[AllowAnonymous]
		[HttpPost]
		[Route("ConfirmEmail")]
		public async Task ConfirmEmail(
			AccountConfirmArguments arguments
		)
		{
			await repository.ConfirmEmail(arguments);
		}
		#endregion Post api/Account/ConfirmEmail

		#region POST api/Account/ForgotPassword
		[AllowAnonymous]
		[HttpPost]
		[Route("ForgotPassword")]
		public async Task<IHttpActionResult> ForgotPassword(AccountForgotPasswordArguments arguments)
		{
			await repository.ForgotPassword(arguments);

			return Ok();
		}
		#endregion POST api/Account/ForgotPassword

		#region POST api/Account/ConfirmForgotPassword
		[AllowAnonymous]
		[HttpPost]
		[Route("ConfirmForgotPassword")]
		public async Task<IHttpActionResult> ConfirmForgotPassword(AccountConfirmForgotPasswordArguments arguments)
		{
			await repository.ConfirmForgotPassword(arguments);

			return Ok();
		}
		#endregion POST api/Account/ConfirmForgotPassword

		#region POST api/Account/UpdatePassword
		[AllowAnonymous]
		[HttpPost]
		[Route("UpdatePassword")]
		public async Task UpdatePassword(AccountChangePasswordArguments arguments)
		{
			await repository.UpdatePassword(arguments);
		}
		#endregion POST api/Account/UpdatePassword

		#region PUT api/Account
		[AllowAnonymous]
		[HttpPut]
		[Route("")]
		public async Task Update(AccountUpdateArguments arguments)
		{
			await repository.UpdateProfileAsync(arguments);

		}
		#endregion PUT api/Account

		#region POST api/Account/ImageCrop
		[AllowAnonymous]
		[HttpPut]
		[Route("ImageCrop")]
		public async Task UserAvatar(AccountUpdatePhotoArguments arguments)
		{
			await repository.UpdateImageUrlAsync(arguments.Avatar);
		}
		#endregion POST api/Account/ImageCrop

		#region POST api/Account/DeleteImage
		[AllowAnonymous]
		[HttpDelete]
		[Route("DeleteImage")]
		public async Task DeleteImage()
		{
			await repository.DeleteImageAsync();
		}
		#endregion POST api/Account/DeleteImage

		#region POST api/Account/OverridePassword
		[AllowAnonymous]
		[HttpPost]
		[Route("OverridePassword")]
		public async Task OverridePassword(AccountOverridePasswordArguments arguments)
		{
			await repository.OverridePassword(arguments);
		}
		#endregion POST api/Account/OverridePassword

		#region PUT api/Account/Logout
		[AllowAnonymous]
		[HttpPut]
		[Route("Logout")]
		public async Task<IHttpActionResult> Logout(AccountLogoutArguments arguments)
		{
			var result = await repository.DeleteRefreshTokenAsync();
			if (result)
				return Ok();

			return BadRequest("Token Id does not exist");
		}
		#endregion PUT api/Account/Logout

		#region Dispose
		protected override void Dispose(bool disposing)
		{
			if (disposing)
				if (repository != null)
					repository.Dispose();

			base.Dispose(disposing);
		}
		#endregion Dispose
	}
}
