﻿using PayIn.Domain.Security;
using PayIn.Infrastructure.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace PayIn.DistributedServices.Security.Controllers
{
	[RoutePrefix("api/RefreshToken")]
	public class RefreshTokenController : ApiController
	{
		private readonly SecurityRepository repository;

		#region Constructors
		public RefreshTokenController()
		{
			repository = new SecurityRepository();
		}
		#endregion Constructors

		#region DELETE /
		[AllowAnonymous]
		[Route("")]
		public async Task<IHttpActionResult> Delete(string tokenId)
		{
			var result = await repository.DeleteRefreshTokenAsync(tokenId);
			if (result)
				return Ok();

			return BadRequest("Token Id does not exist");
		}
		#endregion DELETE /

		#region Dispose
		protected override void Dispose(bool disposing)
		{
			if (disposing)
				if (repository != null)
					repository.Dispose();

			base.Dispose(disposing);
		}
		#endregion Dispose
	}
}
