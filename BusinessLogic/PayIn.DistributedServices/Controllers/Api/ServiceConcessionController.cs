﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using PayIn.Web.Security;
using PayIn.Domain.Security;
using PayIn.Application.Dto.Arguments.ServiceConcession;
using PayIn.Application.Dto;
using Xp.Application.Dto;
using Xp.DistributedServices.ModelBinder;
using PayIn.Application.Dto.Results.ServiceConcession;
using PayIn.Domain.Public;
using Xp.Application.Handlers;

namespace PayIn.DistributedServices.Controllers.Api
{
	[RoutePrefix("Api/ServiceConcession")]
	[XpAuthorize(
		ClientIds = AccountClientId.Web,
		Roles = AccountRoles.Superadministrator + "," + AccountRoles.Operator + "," + AccountRoles.Commerce + "," + AccountRoles.CommercePayment

	)]
	public class ServiceConcessionController : ApiController
	{
		#region GET /{filter?}
		[HttpGet]
		[Route("{filter?}")]
		[XpAuthorize(
			ClientIds = AccountClientId.Web,
			Roles = AccountRoles.Commerce + "," + AccountRoles.CommercePayment
		)]
		public async Task<ResultBase<ServiceConcessionGetAllCommerceResult>> GetAll(
			[FromUri] ServiceConcessionGetAllCommerceArguments arguments,
			[Injection] IQueryBaseHandler<ServiceConcessionGetAllCommerceArguments, ServiceConcessionGetAllCommerceResult> handler
		)
		{
			var result = await handler.ExecuteAsync(arguments);
			return result;
		}
		#endregion GET /{filter?}

		#region GET /States
		[HttpGet]
		[Route("States")]
		public async Task<ResultBase<ServiceConcessionGetStateResult>> States(
			[FromUri] ServiceConcessionGetStateArguments arguments,
			[Injection] IQueryBaseHandler<ServiceConcessionGetStateArguments, ServiceConcessionGetStateResult> handler
		)
		{
			var result = await handler.ExecuteAsync(arguments);
			return result;
		}
		#endregion GET /ConcessionState

		#region GET /{id:int}
		[HttpGet]
		[Route("{id:int}")]
		public async Task<ResultBase<ServiceConcessionGetResult>> Get(
			[FromUri] ServiceConcessionGetArguments argument,
			[Injection] IQueryBaseHandler<ServiceConcessionGetArguments, ServiceConcessionGetResult> handler)
		{
			var result = await handler.ExecuteAsync(argument);
			return result;
		}
		#endregion GET /{id:int}

		#region GET /GetCommerce/{id:int}
		[HttpGet]
		[Route("GetCommerce/{id:int}")]
		[XpAuthorize(
			ClientIds = AccountClientId.Web,
			Roles = AccountRoles.Commerce + "," + AccountRoles.CommercePayment)]
		public async Task<ResultBase<ServiceConcessionGetCommerceResult>> GetCommerce(
			[FromUri] ServiceConcessionGetCommerceArguments argument,
			[Injection] IQueryBaseHandler<ServiceConcessionGetCommerceArguments, ServiceConcessionGetCommerceResult> handler)
		{
			var result = await handler.ExecuteAsync(argument);
			return result;
		}
		#endregion GET /GetCommerce/{id:int}

		#region PUT /{id:int}
		[HttpPut]
		[Route("{id:int}")]
		[XpAuthorize(Roles = AccountRoles.Superadministrator)]
		public async Task<dynamic> Put(
			ServiceConcessionUpdateArguments command,
		[Injection] IServiceBaseHandler<ServiceConcessionUpdateArguments> handler
		)
		{
			var item = await handler.ExecuteAsync(command);
			return new { Id = item.Id };	
			
		}
		#endregion PUT /{id:int}

		#region PUT /UpdateCommerce/{id:int}
		[HttpPut]
		[Route("UpdateCommerce/{id:int}")]
		[XpAuthorize(
			ClientIds = AccountClientId.Web,
			Roles = AccountRoles.Commerce + "," + AccountRoles.CommercePayment)]
		public async Task<dynamic> PutCommerce(
			ServiceConcessionUpdateCommerceArguments command,
		[Injection] IServiceBaseHandler<ServiceConcessionUpdateCommerceArguments> handler
		)
		{
			var item = await handler.ExecuteAsync(command);
			return new { Id = item.Id };
		}
		#endregion PUT /UpdateCommerce/{id:int}

		#region POST /UpdateState
		[HttpPost]
		[Route("UpdateState")]
		[XpAuthorize(
			ClientIds = AccountClientId.Web,
			Roles = AccountRoles.Superadministrator)]
		public async Task<dynamic> Post(
		   ServiceConcessionUpdateStateArguments arguments,
		   [Injection] IServiceBaseHandler<ServiceConcessionUpdateStateArguments> handler
		)
		{
			var result = (await handler.ExecuteAsync(arguments));
			return result;
		}
		#endregion POST /UpdateState

		#region GET /RetrieveSelector
		[HttpGet]
		[Route("RetrieveSelector/{filter?}")]
		public async Task<ResultBase<ServiceConcessionGetSelectorResult>> RetrieveSelector(
			[FromUri] ServiceConcessionGetSelectorArguments command,
			[Injection] IQueryBaseHandler<ServiceConcessionGetSelectorArguments, ServiceConcessionGetSelectorResult> handler
		)
		{
			var result = await handler.ExecuteAsync(command);
			return result;
		}
		#endregion GET /RetrieveSelector

		#region DELETE /{id:int}
		[HttpDelete]
		[Route("{id:int}")]
		[XpAuthorize(
		ClientIds = AccountClientId.Web,
		Roles = AccountRoles.Commerce + "," + AccountRoles.CommercePayment
		)]
		public async Task<dynamic> Delete(
				int id,
				[FromUri] ServiceConcessionDeleteArguments command,
				[Injection] IServiceBaseHandler<ServiceConcessionDeleteArguments> handler
		)
		{
			var result = await handler.ExecuteAsync(command);
			return result;
		}
		#endregion DELETE /{id:int}

	}
}
