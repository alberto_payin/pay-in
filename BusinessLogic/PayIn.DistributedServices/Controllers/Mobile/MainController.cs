﻿using PayIn.Application.Dto.Arguments.Main;
using PayIn.Application.Dto.Results.Main;
using PayIn.Domain.Security;
using PayIn.Web.Security;
using System.Threading.Tasks;
using System.Web.Http;
using Xp.Application.Dto;
using Xp.DistributedServices.ModelBinder;

namespace PayIn.DistributedServices.Controllers
{
	[RoutePrefix("Mobile/Main")]
	[XpAuthorize(
		ClientIds = AccountClientId.AndroidNative,
		Roles = AccountRoles.User + "," + AccountRoles.Commerce
	)]
	public class MainController : ApiController
	{
		#region GET /v1
		[HttpGet]
		[Route("v1")]
		public async Task<ResultBase<MainMobileGetAllResult>> Get(
			[FromUri] MainMobileGetAllArguments command,
			[Injection] IQueryBaseHandler<MainMobileGetAllArguments, MainMobileGetAllResult> handler
		)
		{
			var result = await handler.ExecuteAsync(command);
			return result;
		}
		#endregion GET /v1
	}
}
