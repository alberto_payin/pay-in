﻿using PayIn.Application.Dto.Arguments.Notification;
using PayIn.Application.Dto.Arguments.ServiceNotification;
using PayIn.Application.Dto.Results.ServiceNotification;
using PayIn.Domain.Security;
using PayIn.Web.Security;
using System.Threading.Tasks;
using System.Web.Http;
using Xp.Application.Dto;
using Xp.DistributedServices.ModelBinder;

namespace PayIn.DistributedServices.Controllers.Mobile
{
	[RoutePrefix("Mobile/ServiceNotification")]  
	[XpAuthorize(
		ClientIds = AccountClientId.AndroidNative,
		Roles = AccountRoles.User
	)]
	public class MobileServiceNotificationController : ApiController
	{
		#region GET /v1
		[HttpGet]
		[Route("v1/")]
		public async Task<ResultBase<ServiceNotificationMobileGetAllResult>> GetAll(
			[FromUri] ServiceNotificationMobileGetAllArguments command,
			[Injection] IQueryBaseHandler<ServiceNotificationMobileGetAllArguments, ServiceNotificationMobileGetAllResult> handler
		)
		{
			var result = await handler.ExecuteAsync(command);
			return result;
		}
		#endregion GET /v1

		#region POST /v1/
		[HttpPost]
		[Route("v1/")]
		public async Task<dynamic> Post(
			ServiceNotificationCreateArguments arguments,
			[Injection] IServiceBaseHandler<ServiceNotificationCreateArguments> handler
		)
		{
			var result = await handler.ExecuteAsync(arguments);
			return result;
		}
		#endregion POST /v1/
	}
}
