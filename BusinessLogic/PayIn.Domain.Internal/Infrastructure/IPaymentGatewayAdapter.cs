﻿using System.Threading.Tasks;

namespace PayIn.Domain.Internal.Infrastructure
{
	public interface IPaymentGatewayAdapter
	{
		Task<string> WebCardRequestAsync(int paymentMediaId, int publicPaymentMediaId, int publicTicketId, int publicPaymentId, int order);
		Task<string> PayAsync(int paymentMediaId, int publicPaymentMediaId, int publicTicketId, int publicPaymentId, string cardIdentifier, int order, decimal amount);
		Task<string> RefundAsync(int paymentMediaId, int publicPaymentMediaId, int publicTicketId, int publicPaymentId, string cardIdentifier, int order, decimal amount);
		bool VerifyResponse(string data, string signature);
		bool VerifyResponse(decimal amount, int orderId, string commerceCode, string currency, string response, string cardNumberHash, string transactionType, bool securePayment, string signature);
		bool VerifyCommerceCode(string commerceCode);
	}
}
