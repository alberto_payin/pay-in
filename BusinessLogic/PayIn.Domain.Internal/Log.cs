﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xp.Domain;

namespace PayIn.Domain.Internal
{
	public class Log : Entity
	{
		[Required]                            public DateTime DateTime      { get; set; }
		[Required]                            public TimeSpan Duration      { get; set; }
		[Required(AllowEmptyStrings = true)]  public string   Login         { get; set; } // Caso de conexiones anónimas como Sabadell, ...
		[Required(AllowEmptyStrings = true)]  public string   RelatedClass  { get; set; }
		[Required(AllowEmptyStrings = true)]  public string   RelatedMethod { get; set; }
		[Required]                            public int      RelatedId     { get; set; }

		#region Arguments
		[InverseProperty("Log")]
		public ICollection<LogArgument> Arguments { get; set; }
		#endregion Arguments

		#region Constructors
		public Log()
		{
			Arguments = new List<LogArgument>();
		}
		#endregion Constructors
	}
}
