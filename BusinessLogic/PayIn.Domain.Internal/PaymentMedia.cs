﻿using PayIn.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Xp.Domain;

namespace PayIn.Domain.Internal
{
	public class PaymentMedia : Entity
	{
		// Attributes
		                                      public PaymentMediaType    Type            { get; set; }
		[Required(AllowEmptyStrings = false)] public string              Name            { get; set; }
		[Required(AllowEmptyStrings = false)] public string              Number          { get; set; }
		                                      public int?                ExpirationMonth { get; set; }
		                                      public int?                ExpirationYear  { get; set; }
		                                      public int                 PublicId        { get; set; }
		                                      public PaymentMediaState   State           { get; set; }
		[Required(AllowEmptyStrings = false)] public string              BankEntity      { get; set; }
		[Required(AllowEmptyStrings = true)]  public string              Reference       { get; set; }

		#region User
		public int  UserId { get; set; }
		public User	User	 { get; set; }
		#endregion User
	}
}
