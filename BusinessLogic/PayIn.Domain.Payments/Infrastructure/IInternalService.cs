﻿using PayIn.Application.Dto.Internal.Arguments.PaymentMedia;
using PayIn.Application.Dto.Internal.Results.PaymentMedia;
using System.Threading.Tasks;

namespace PayIn.Domain.Payments.Infrastructure
{
	public interface IInternalService
	{
		// User
		Task UserCreateAsync(string pin);
		Task<bool> UserHasPaymentAsync();
		Task UserUpdatePinAsync(string oldPin, string pin, string confirmPin);
		Task<bool> UserCheckPinAsync(string pin);

		// Payments
		Task<dynamic> PaymentMediaCreateWebCardAsync(string pin, string name, int orderId, int publicPaymentMediaId, int publicTicketId, int publicPaymentId, string bankEntity, 
			string deviceManufacturer, string deviceModel, string deviceName, string deviceSerial, string deviceId, string deviceOperator, string deviceImei, string deviceMac, string operatorSim, string operatorMobile);
		Task PaymentMediaCreateWebCardSabadellAsync(PaymentMediaCreateWebCardSabadellArguments arguments);
		Task<PaymentMediaPayResult> PaymentMediaCreateWebCardRefundSabadellAsync(int publicPaymentMediaId, int publicTicketId, int publicPaymentId, decimal amount, string currency, int orderId, int terminal);
		Task<PaymentMediaPayResult> PaymentMediaPayAsync(string pin, int publicPaymentMediaId, int publicTicketId, int publicPaymentId, int order, decimal amount);
		Task<PaymentMediaPayResult> PaymentMediaRefundAsync(string pin, int publicPaymentMediaId, int publicTicketId, int publicPaymentId, int order, decimal amount);
		Task PaymentMediaDeleteAsync(int id);
	}
}
