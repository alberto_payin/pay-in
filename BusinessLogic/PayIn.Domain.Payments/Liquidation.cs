﻿using PayIn.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xp.Domain;

namespace PayIn.Domain.Payments
{
	public class Liquidation : IEntity
	{
		public int                Id             { get; set; }
		public decimal            TotalQuantity  { get; set; }
		public decimal            PayinQuantity  { get; set; }
		public decimal            PaidQuantity   { get; set; }
		public LiquidationState   State          { get; set; }
		public DateTime           Since          { get; set; }
		public DateTime           Until          { get; set; }
		public DateTime?          PaymentDate    { get; set; }
		public DateTime?          RequestDate    { get; set; }

		#region Payments
		[InverseProperty("Liquidation")]
		public ICollection<Payment> Payments { get; set; }
		#endregion Payments

		#region Concession
		public int ConcessionId { get; set; }
		public PaymentConcession Concession { get; set; }
		#endregion Concession

		#region Constructors
		public Liquidation()
		{
			Payments = new List<Payment>();
		}
		#endregion Constructors
	}
}
