﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Xp.Domain;

namespace PayIn.Domain.Payments
{
	public class Log : Entity
	{
		[Required]                            public DateTime DateTime      { get; set; }
		[Required]                            public TimeSpan Duration      { get; set; }
		[Required(AllowEmptyStrings = true)]  public string   Login         { get; set; }
		[Required(AllowEmptyStrings = true)]  public string   RelatedClass  { get; set; }
		[Required(AllowEmptyStrings = true)]  public string   RelatedMethod { get; set; }
		[Required]                            public int      RelatedId     { get; set; }
		[Required(AllowEmptyStrings = true)]  public string   Error         { get; set; }

		#region Arguments
		[InverseProperty("Log")]
		public ICollection<LogArgument> Arguments { get; set; }
		#endregion Arguments

		#region Constructors
		public Log()
		{
			Arguments = new List<LogArgument>();
		}
		#endregion Constructors
	}
}
