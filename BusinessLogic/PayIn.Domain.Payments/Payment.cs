﻿using PayIn.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Xp.Domain;

namespace PayIn.Domain.Payments
{
	public class Payment : IEntity
	{
		                                      public int          Id                { get; set; }
		[Required]                            public decimal      Amount            { get; set; }
		                                      public decimal      Payin             { get; set; }
		                                      public PaymentState State             { get; set; }
		                                      public DateTime     Date              { get; set; }
		[Required(AllowEmptyStrings = true)]  public string       UserName          { get; set; }
		[Required(AllowEmptyStrings = false)] public string       UserLogin         { get; set; }
		                                      public int?         Order             { get; set; }
		[Required(AllowEmptyStrings = true)]  public string       ErrorCode         { get; set; }
		[Required(AllowEmptyStrings = true)]  public string       ErrorText         { get; set; }
		[Required(AllowEmptyStrings = true)]  public string       ErrorPublic       { get; set; }
		[Required(AllowEmptyStrings = true)]  public string       AuthorizationCode { get; set; }


		#region PaymentMedia
		public int PaymentMediaId { get; set; }
		public PaymentMedia PaymentMedia { get; set; }
		#endregion PaymentMedia

		#region TaxName
		[Required(AllowEmptyStrings = false)]
		public string TaxName { get; set; }
		#endregion TaxName

		#region TaxAddress
		[Required(AllowEmptyStrings = false)]
		public string TaxAddress { get; set; }
		#endregion TaxAddress

		#region TaxNumber
		[Required(AllowEmptyStrings = false)]
		public string TaxNumber { get; set; }
		#endregion TaxNumber

		#region Ticket
		public int TicketId { get; set; }
		public Ticket Ticket { get; set; }
		#endregion Ticket

		#region Liquidation
		public int? LiquidationId { get; set; }
		public Liquidation Liquidation { get; set; }
		#endregion Liquidation

		#region RefundFrom
		public int? RefundFromId { get; set; }
		public Payment RefundFrom { get; set; }
		#endregion RefundFrom

		#region RefundTo
		[InverseProperty("RefundFrom")]
		public ICollection<Payment> RefundTo { get; set; }
		#endregion RefundTo

		#region Constructors
		public Payment()
		{
			RefundTo = new List<Payment>();
		}
		#endregion Constructors
	}
}
