﻿using PayIn.Common;
using PayIn.Domain.Public;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Xp.Domain;

namespace PayIn.Domain.Payments
{
	public class PaymentConcession : IEntity
	{
		                                      public int         Id                     { get; set; }
		                                      public decimal     Vat                    { get; set; }
		                                      public decimal?    IncomeTax              { get; set; }																					       
		                                      public string      Observations           { get; set; }
		[Required(AllowEmptyStrings = true)]  public string      Phone                  { get; set; }
		[Required(AllowEmptyStrings = false)] public string      BankAccountNumber      { get; set; }
		[Required(AllowEmptyStrings = true)]  public string      FormUrl                { get; set; }
		[Required(AllowEmptyStrings = false)] public string      Address                { get; set; }
		                                      public DateTime    CreateConcessionDate   { get; set; }
		                                      public DateTime?   LiquidationRequestDate { get; set; }
		[Precision(9,6)]                      public decimal     LiquidationAmountMin   { get; set; }
		                                      public decimal     PayinCommision         { get; set; }

		#region Concession
		public int ConcessionId { get; set; }
		public ServiceConcession Concession { get; set; }
		#endregion Concession

		#region Tickets
		[InverseProperty("Concession")]
		public ICollection<Ticket> Tickets { get; set; }
		#endregion Tickets

		#region Liquidations
		[InverseProperty("Concession")]
		public ICollection<Liquidation> Liquidations { get; set; }
		#endregion Liquidations

		#region PaymentWorkers 
		[InverseProperty("Concession")]
		public ICollection<PaymentWorker> PaymentWorkers { get; set; } 
		#endregion PaymentWorkers

		#region Constructors
		public PaymentConcession()
		{
			Tickets = new List<Ticket>();
			Liquidations = new List<Liquidation>();
			PaymentWorkers = new List<PaymentWorker>();
		}
		#endregion Constructors
	}
}
