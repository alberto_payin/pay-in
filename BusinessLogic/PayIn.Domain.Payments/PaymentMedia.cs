﻿using PayIn.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Xp.Domain;

namespace PayIn.Domain.Payments
{
	public class PaymentMedia : IEntity
	{
		                                      public int               Id                  { get; set; }
		[Required(AllowEmptyStrings = false)] public string            Name                { get; set; }
		[Required(AllowEmptyStrings = false)] public string            NumberHash          { get; set; }
		                                      public int?              ExpirationMonth     { get; set; }
		                                      public int?              ExpirationYear      { get; set; }
		                                      public int               VisualOrder         { get; set; }
		                                      public int?              VisualOrderFavorite { get; set; }
		[Required(AllowEmptyStrings = false)] public string            Login               { get; set; }
		                                      public PaymentMediaState State               { get; set; }
		[Required(AllowEmptyStrings = false)] public string            BankEntity          { get; set; }

		#region Type
		public PaymentMediaType Type { get; set; }
		#endregion Type

		#region Payments
		[InverseProperty("PaymentMedia")]
		public ICollection<Payment> Payments { get; set; }
		#endregion Payments

		#region Constructors
		public PaymentMedia()
		{
			Payments = new List<Payment>();
		}
		#endregion Constructors
	}
}
