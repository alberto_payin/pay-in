﻿using PayIn.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Xp.Domain;

namespace PayIn.Domain.Payments
{
	public class Ticket : Entity
	{
		[Required(AllowEmptyStrings = true)]  public string          Reference    { get; set; }
		[Required(AllowEmptyStrings = false)] public string          Title        { get; set; }
		                                      public DateTime        Date         { get; set; }
		                                      public decimal         Amount       { get; set; }
		                                      public decimal         Vat          { get; set; }
		                                      public decimal?        IncomeTax    { get; set; }
		[Required(AllowEmptyStrings = false)] public string          SupplierName { get; set; }
		[Required(AllowEmptyStrings = false)] public string          TaxName      { get; set; }
		[Required(AllowEmptyStrings = false)] public string          TaxAddress   { get; set; }
		[Required(AllowEmptyStrings = false)] public string          TaxNumber    { get; set; }
		                                      public TicketStateType State        { get; set; }

		//[Required(AllowEmptyStrings = false)] [MinLength(3)] [MaxLength(3)] public string      Currency           { get; set; } // http://es.wikipedia.org/wiki/ISO_4217
		
		//#region Details
		//[InverseProperty("Ticket")]
		//public ICollection<TicketDetail> Details { get; set; }
		//#endregion Details

		#region Payments
		[InverseProperty("Ticket")]
		public ICollection<Payment> Payments { get; set; }
		#endregion Payments

		#region Concession
		public int ConcessionId { get; set; }
		public PaymentConcession Concession { get; set; }
		#endregion Concession

		#region PaymentWorker
		public int? PaymentWorkerId { get; set; }
		public PaymentWorker PaymentWorker { get; set; }
		#endregion PaymentWorker

		#region Constructors
		public Ticket()
		{
			Payments = new List<Payment>();
		}
		#endregion Constructors
	}
}