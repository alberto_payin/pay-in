﻿using PayIn.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xp.Domain;


namespace PayIn.Domain.Public
{
	public class ControlFormValue : IEntity
	{
		                                      public int                       Id            { get; set; }
		[Required(AllowEmptyStrings = false)] public string                    Name          { get; set; }
		                                      public string                    Observations  { get; set; }
		                                      public ControlFormArgumentType   Type          { get; set; }
		                                      public ControlFormArgumentTarget Target        { get; set; }
		                                      public bool                      IsRequired    { get; set; }
		                                      public string                    ValueString   { get; set; }
		                                      public decimal?                  ValueNumeric  { get; set; }
		                                      public bool?                     ValueBool     { get; set; }
		                                      public DateTime?                 ValueDateTime { get; set; }

		#region Assign
		public int AssignId { get; set; }
		public ControlFormAssign Assign { get; set; }
		#endregion Assign

		#region Argument
		public int ArgumentId { get; set; }
		public ControlFormArgument Argument { get; set; }
		#endregion Argument
	}
}
