﻿using PayIn.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Xp.Domain;

namespace PayIn.Domain.Public
{
	public class ServiceConcession : IEntity
	{
		                                      public int             Id         { get; set; }
		[Required(AllowEmptyStrings = false)] public string          Name       { get; set; }
		                                      public ServiceType     Type       { get; set; }
		                                      public int             MaxWorkers { get; set; }
		                                      public ConcessionState State      { get; set; } 

		#region Zones
		[InverseProperty("Concession")]
		public ICollection<ServiceZone> Zones { get; set; }
		#endregion Zones

		#region Supplier
		public int SupplierId { get; set; }
		public ServiceSupplier Supplier { get; set; }
		#endregion Supplier

		#region FreeDays
		[InverseProperty("Concession")]
		public ICollection<ServiceFreeDays> FreeDays { get; set; }
		#endregion FreeDays

		#region Interval
		public TimeSpan Interval { get; set; }
		#endregion Interval

		#region Items
		[InverseProperty("Concession")]
		public ICollection<ControlItem> Items { get; set; }
		#endregion Items

		#region Forms
		[InverseProperty("Concession")]
		public ICollection<ControlForm> Forms { get; set; }
		#endregion Forms

		#region Constructors
		public ServiceConcession()
		{
			Zones = new List<ServiceZone>();
			FreeDays = new List<ServiceFreeDays>();
			Items = new List<ControlItem>();
			//Addresses = new List<ServiceAddress>();
			//Prices = new List<ServicePrice>();
			//TimeTables = new List<ServiceTimeTable>();
			State = ConcessionState.Active;
		}
		#endregion Constructors
	}
}
