﻿using PayIn.Common;
using System;
using Xp.Domain;

namespace PayIn.Domain.Public
{
	public class ServiceNotification : IEntity
	{
		public int               Id             { get; set; }
		public DateTime          CreatedAt      { get; set; }
		public NotificationType  Type	        { get; set; }
		public NotificationState State	        { get; set; }
		public int               ReferenceId    { get; set; }
		public string            ReferenceClass { get; set; }
		public string            SenderLogin    { get; set; }
		public string            ReceiverLogin  { get; set; }
		public bool				 IsRead		    { get; set; }
	}
}
