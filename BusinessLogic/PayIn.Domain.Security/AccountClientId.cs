﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayIn.Domain.Security
{
	public static class AccountClientId
	{
		public const string Web = "PayInWebApp";
		public const string Ios = "PayInIosApp";
		public const string Android = "PayInAndroidApp";
		public const string Wp = "PayInWpApp";
		public const string Api = "PayInApi";
		public const string AndroidNative = "PayInAndroidNativeApp";
		public const string Tpv = "PayInTpv";
	}
}
