﻿namespace PayIn.Domain.Security
{
	public static class AccountRoles
	{
		public const string Superadministrator = "Superadministrator";
		public const string Administrator = "Administrator";
		public const string User = "User";
		public const string Commerce = "Commerce";
		public const string CommercePayment = "CommercePayment";
		public const string PaymentWorker = "PaymentWorker";
		public const string Operator = "Operator";
		public const string Tester = "Tester";
		public const string ControlApi = "ControlApi";
	}
}
