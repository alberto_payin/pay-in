namespace PayIn.Infrastructure.Internal.Db.Migrations
{
	using PayIn.Domain.Internal;
	using PayIn.Common;
	using System;
	using System.Data.Entity;
	using System.Data.Entity.Migrations;
	using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<InternalContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "PayIn.Infrastructure.Internal.InternalContext";
        }

        protected override void Seed(InternalContext context)
        {
					context.User.AddOrUpdate(x => x.Id,
						new User { Id = 1, Login = "user@pay-in.es", Pin = "1234".ToHash(), PinRetries = 0, State = UserState.Active }
					);
        }
    }
}
