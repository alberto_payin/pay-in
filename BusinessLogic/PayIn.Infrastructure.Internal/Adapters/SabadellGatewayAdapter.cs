﻿using PayIn.Application.Dto.Internal.Arguments.PaymentMedia;
using PayIn.Domain.Internal;
using PayIn.Domain.Internal.Infrastructure;
using PayIn.Infrastructure.Sabadell;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Xp.Domain;

namespace PayIn.Infrastructure.Internal.Adapters
{
	public class SabadellGatewayAdapter : IPaymentGatewayAdapter
	{
		public const string CommerceName = "Payment Innovation Network S.L.";
#if TEST
		public const string CommerceCode = "327624508"; // Pruebas del Sabadell
		public const string CommerceSecretKey = "qwertyasdf0123456789";
		public const string Uri = "https://sis-t.redsys.es:25443/sis/operaciones";
		public const string CommerceUrl = "http://payin-test.cloudapp.net";
#elif DEBUG || EMULATOR
		public const string CommerceCode = "327624508"; // Pruebas del Sabadell
		public const string CommerceSecretKey = "qwertyasdf0123456789";
		public const string Uri = "https://sis-t.redsys.es:25443/sis/operaciones";
		public const string CommerceUrl = "http://95.19.70.123:8080";
#else
		public const string CommerceCode = "327624508"; // Real
		public const string CommerceSecretKey = "";
		public const string Uri = "https://sis.REDSYS.es/sis/operaciones";
		public const string CommerceUrl = "https://control.pay-in.es";
#endif

		private readonly IEntityRepository<PaymentMedia> PaymentMediaRepository;
		private readonly IEntityRepository<PaymentGateway> PaymentGatewayRepository;

		#region Constructors
		public SabadellGatewayAdapter(IEntityRepository<PaymentMedia> paymentMediaRepository, IEntityRepository<PaymentGateway> paymentGatewayRepository)
		{
			if (paymentMediaRepository == null)
				throw new ArgumentNullException("paymentMediaRepository");
			PaymentMediaRepository = paymentMediaRepository;
			
			if (paymentGatewayRepository == null)
				throw new ArgumentNullException("paymentGatewayRepository");
			PaymentGatewayRepository = paymentGatewayRepository;
		}
		#endregion Constructors

		#region WebCardRequestAsync
		public async Task<string> WebCardRequestAsync(int paymentMediaId, int publicPaymentMediaId, int publicTicketId, int publicPaymentId, int orderId)
		{
			return await Task.Run(() => SabadellGatewayFunctions.CreateWebPaymentRequest(
				paymentMediaId: paymentMediaId,
				publicPaymentMediaId: publicPaymentMediaId,
				publicTicketId: publicTicketId,
				publicPaymentId: publicPaymentId,
				orderId: orderId,
				transactionType: "0",
				commerceUrl: CommerceUrl + "/Sabadell/WebCard", 
				commerceCode: CommerceCode, 
				commerceSecretKey: CommerceSecretKey, 
				commerceName: CommerceName, 
				amount: 1
			));
		}
		#endregion WebCardRequestAsync

		#region PayAsync
		public async Task<string> PayAsync(int paymentMediaId, int publicPaymentMediaId, int publicTicketId, int publicPaymentId, string cardIdentifier, int orderId, decimal amount)
		{
			var xml = SabadellGatewayFunctions.CreatePaymentRequest(
				cardIdentifier: cardIdentifier,
				paymentMediaId: paymentMediaId,
				publicPaymentMediaId: publicPaymentMediaId,
				publicTicketId: publicTicketId,
				publicPaymentId: publicPaymentId,
				transactionType: "0",
				commerceCode: CommerceCode,
				commerceSecretKey: CommerceSecretKey,
				commerceName: CommerceName,
				amount: amount,
				orderId: orderId
			);

			var result = await SendAsync("entrada=" + xml);
			return result;
		}
		#endregion PayAsync

		#region RefundAsync
		public async Task<string> RefundAsync(int paymentMediaId, int publicPaymentMediaId, int publicTicketId, int publicPaymentId, string cardIdentifier, int orderId, decimal amount)
		{
			var xml = SabadellGatewayFunctions.CreatePaymentRequest(
				cardIdentifier: cardIdentifier,
				paymentMediaId: paymentMediaId,
				publicPaymentMediaId: publicPaymentMediaId,
				publicTicketId: publicTicketId,
				publicPaymentId: publicPaymentId,
				transactionType: "3",
				commerceCode: CommerceCode,
				commerceSecretKey: CommerceSecretKey,
				commerceName: CommerceName,
				amount: amount,
				orderId: orderId
			);

			var result = await SendAsync("entrada=" + xml);
			return result;
		}
		#endregion RefundAsync

		#region VerifyCommerceCode
		public bool VerifyCommerceCode(string commerceCode)
		{
			return commerceCode == CommerceCode;
		}
		#endregion VerifyCommerceCode

		#region VerifyResponse
		public bool VerifyResponse(decimal amount, int orderId, string commerceCode, string currency, string response, string cardNumberHash, string transactionType, bool securePayment, string signature)
		{
			var orderString = orderId.ToString("#0000");
			var securePaymentString = securePayment ? "1" : "0";

			return (SabadellGatewayFunctions.Sha(amount + orderString + commerceCode + currency + response + cardNumberHash + transactionType + securePaymentString + CommerceSecretKey) == signature);
			//return (SabadellGatewayFunctions.Sha(amount + orderId + commerceCode + currency + response + CommerceSecretKey) == signature);
			//return true;
		}
		public bool VerifyResponse(string data, string signature) {
			// Digest=SHA-1(Ds_ Amount + Ds_ Order + Ds_MerchantCode + Ds_ Currency + Ds _Response + CLAVE SECRETA)
			return (SabadellGatewayFunctions.Sha(data + CommerceSecretKey) == signature);
		}
		#endregion VerifyResponse

		#region SendAsync
		private async Task<string> SendAsync(string request)
		{
			using (var client = new HttpClient())
			{
				var content = new StringContent(request, Encoding.UTF8);
				content.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded"); // Content-type: application/x-www-form-urlencoded

				var response = (await client.PostAsync(new Uri(Uri), content))
					.ThrowException();

				var result = (await response.Content.ReadAsStringAsync());
				return result;
			}
		}
		#endregion SendAsync
	}
}
