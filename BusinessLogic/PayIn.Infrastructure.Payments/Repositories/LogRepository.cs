﻿using PayIn.Domain.Payments;
using PayIn.Infrastructure.Public.Db;

namespace PayIn.Infrastructure.Payments
{
	public class LogRepository : PublicRepository<Log>
	{
		#region Contructors
		public LogRepository(IPublicContext context)
			: base(context)
		{
		}
		#endregion Contructors
	}
}
