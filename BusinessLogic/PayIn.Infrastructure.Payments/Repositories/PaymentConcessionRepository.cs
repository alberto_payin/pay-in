﻿using PayIn.BusinessLogic.Common;
using PayIn.Infrastructure.Public.Db;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PayIn.Domain.Payments;
using PayIn.Domain.Security;
using PayIn.Common;

namespace PayIn.Infrastructure.Payments.Repositories
{
	public class PaymentConcessionRepository : PublicRepository<PaymentConcession>
	{
		public readonly ISessionData SessionData;

		#region Contructors
		public PaymentConcessionRepository(
			ISessionData sessionData,
			IPublicContext context
		)
			: base(context)
		{
			if (sessionData == null) throw new ArgumentNullException("sessionData");
			SessionData = sessionData;
		}
		#endregion Contructors

		#region CheckHorizontalVisibility
		public override IQueryable<PaymentConcession> CheckHorizontalVisibility(IQueryable<PaymentConcession> that)
		{
			var result = that
				.Where(x =>
					x.Concession.Type == ServiceType.Charge
					);

			if (!SessionData.Roles.Contains(AccountRoles.Superadministrator))
				result = result
					.Where(x =>
						(x.Concession.Supplier.Login == SessionData.Login) ||
						(x.PaymentWorkers.Any(y => y.Login == SessionData.Login)) ||
						(x.Concession.Supplier.Login == "info@pay-in.es")
					);

			return result;
		}
		#endregion CheckHorizontalVisibility
	}
}
