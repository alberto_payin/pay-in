﻿using PayIn.BusinessLogic.Common;
using PayIn.Common;
using PayIn.Domain.Payments;
using PayIn.Domain.Security;
using PayIn.Infrastructure.Public.Db;
using System;
using System.Linq;

namespace PayIn.Infrastructure.Payments.Repositories
{
	public class TicketRepository : PublicRepository<Ticket>
	{
		public readonly ISessionData SessionData;

		#region Constructors
		public TicketRepository(
			ISessionData sessionData, 
			IPublicContext context
		)
			: base(context)
		{
			if (sessionData == null) throw new ArgumentNullException("sessionData");
			SessionData = sessionData;
		}
		#endregion Constructors

		#region CheckHorizontalVisibility
		public override IQueryable<Ticket> CheckHorizontalVisibility(IQueryable<Ticket> that)
		{
			var now = DateTime.Now.AddHours(-6);

			var result = that
				.Where(x =>
					x.Concession.Concession.Type == Common.ServiceType.Charge &&
					x.Concession.Concession.State == Common.ConcessionState.Active && 
					(
						x.Date > now ||
						(x.Payments.Count() == 0 && (x.Concession.PaymentWorkers.Any(y => y.Login == SessionData.Login))) ||
						x.Concession.Concession.Supplier.Login == SessionData.Login ||
						(x.PaymentWorker.Login == SessionData.Login  && x.PaymentWorker.State == WorkerState.Active)||
						x.Payments.Any(y => y.UserLogin == SessionData.Login)|| 					
						(SessionData.Roles.Contains(AccountRoles.Superadministrator))
					)
				)
				;
			return result;
		}
		#endregion CheckHorizontalVisibility
	}
}
