namespace PayIn.Infrastructure.Public.Db.Migrations
{
	using PayIn.Domain.Public;
	using System;
	using System.Data.Entity;
	using System.Data.Entity.Migrations;
	using System.Linq;
	using PayIn.Common;
	using System.Transactions;

	internal sealed class Configuration : DbMigrationsConfiguration<PublicContext>
	{
		public Configuration()
		{
				AutomaticMigrationsEnabled = false;
				ContextKey = "PayIn.Infrastructure.Public.PublicContext";
		}

		protected override void Seed(PublicContext context)
		{
			if (!context.ServiceSupplier.Any(x => x.Login == "info@pay-in.es"))
			{
				context.ServiceSupplier.AddOrUpdate(x => x.Name,
					new PayIn.Domain.Public.ServiceSupplier
					{
						Login = "info@pay-in.es",
						Name = "Pay[in]",
						PaymentTest = false,
						TaxName = "Payment Innovation Network, S.L.",
						TaxNumber = "B-98.638.927",
						TaxAddress = "C/ P�rez Gald�s n.79 pta 16 46008 Valencia"
					}
				);
				context.SaveChanges();
			}
			var payinSupplier = context.ServiceSupplier.First(x => x.Login == "info@pay-in.es");

			if (!context.ServiceConcession.Any(x => x.SupplierId == payinSupplier.Id && x.Type == ServiceType.Charge))
			{
				context.ServiceConcession.AddOrUpdate(x => x.Name,
					new PayIn.Domain.Public.ServiceConcession
					{
						Name = "Payment Pay[in]",
						State = ConcessionState.Active,
						SupplierId = payinSupplier.Id,
						Type = ServiceType.Charge
					}
				);
				context.SaveChanges();
			}
			var payinConcession = context.ServiceConcession.First(x => x.SupplierId == payinSupplier.Id && x.Type == ServiceType.Charge);

			if (!context.PaymentConcession.Any(x => x.ConcessionId == payinConcession.Id))
			{
				context.PaymentConcession.AddOrUpdate(x => x.ConcessionId,
					new PayIn.Domain.Payments.PaymentConcession
					{
						BankAccountNumber = "BANK",
						ConcessionId = payinConcession.Id,
						FormUrl = "",
						Observations = "",
						PayinCommision = 0,
						Phone = "Phone",
						LiquidationAmountMin = 100,
						CreateConcessionDate = DateTime.Now,
						Address = "Valor predefinido",
						Vat = 0
					}
				);
				context.SaveChanges();
			}

			#region Control
			{
				if (!context.ServiceSupplier.Any(x => x.Login == "commerce@pay-in.es"))
				{
					context.ServiceSupplier.AddOrUpdate(x => x.Name,
						new PayIn.Domain.Public.ServiceSupplier { Name = "Logistica", Login = "commerce@pay-in.es", PaymentTest = true, TaxName = "Logistica SL", TaxNumber = "123456789X", TaxAddress = "C/ Mayor,1 Vilamarxant" }
					);
					context.SaveChanges();
				}
				var supplier = context.ServiceSupplier.First(x => x.Login == "commerce@pay-in.es");

				if (!context.ServiceConcession.Any(x => x.SupplierId == supplier.Id && x.Name == "Control Logistica"))
				{
					context.ServiceConcession.AddOrUpdate(x => x.Name,
						new PayIn.Domain.Public.ServiceConcession { Name = "Control Logistica", Type = ServiceType.Control, SupplierId = supplier.Id, }
					);
					context.SaveChanges();
				}
				var concession = context.ServiceConcession.First(x => x.Name == "Control Logistica");

				if (!context.ServiceWorker.Any(x => x.Login == "operator@pay-in.es"))
				{
					context.ServiceWorker.AddOrUpdate(x => x.Name,
						new PayIn.Domain.Public.ServiceWorker { State = WorkerState.Active, Name = "Operador", Login = "operator@pay-in.es", SupplierId = supplier.Id }
					);
				}

				if (!context.ServiceWorker.Any(x => x.Login == "user@pay-in.es"))
				{
					context.ServiceWorker.AddOrUpdate(x => x.Name,
						new PayIn.Domain.Public.ServiceWorker { State = WorkerState.Active, Name = "Usuario", Login = "user@pay-in.es", SupplierId = supplier.Id }
					);
				}
			}
			#endregion Control

			#region Payments
			{
				if (!context.ServiceSupplier.Any(x => x.Login == "payment@pay-in.es"))
				{
					context.ServiceSupplier.AddOrUpdate(x => x.Name,
						new ServiceSupplier { Name = "Bar APTC", Login = "payment@pay-in.es", PaymentTest = true, TaxName = "APTC SL", TaxNumber = "123456789X", TaxAddress = "C/ San Blas, 1 Vilamarxant" }
					);
					context.SaveChanges();
				}
				var supplier = context.ServiceSupplier.First(x => x.Login == "payment@pay-in.es");

				if (!context.ServiceConcession.Any(x => x.SupplierId == supplier.Id && x.Name == "Pagos APTC"))
				{
					context.ServiceConcession.AddOrUpdate(x => x.Name,
						new ServiceConcession { Name = "Pagos APTC", Type = ServiceType.Charge, SupplierId = supplier.Id }
					);
					context.SaveChanges();
				}
				var concession = context.ServiceConcession.First(x => x.Name == "Pagos APTC");

				if (!context.PaymentConcession.Any(x => x.ConcessionId == concession.Id))
				{
					context.PaymentConcession.AddOrUpdate(x => x.ConcessionId,
						new PayIn.Domain.Payments.PaymentConcession { ConcessionId = concession.Id, Phone = "600600600", FormUrl = "", LiquidationRequestDate = new DateTime(1900, 1, 1, 0, 0, 0, DateTimeKind.Local), LiquidationAmountMin = 100m, Address = "C/ San Blas, 1 Vilamarxant", CreateConcessionDate = new DateTime(1900, 1, 1, 0, 0, 0, DateTimeKind.Local), BankAccountNumber = "ES0000000000000000000000" }
					);
				}
			}
			#endregion Payments

			if (!context.ServiceOption.Any(x => x.Id == 1))
				context.ServiceOption.AddOrUpdate(x => x.Id, new ServiceOption
				{
					Id = 1,
					Name = "AndroidVersionCode",
					ValueType = "int",
					Value = "19"
				});
			if (!context.ServiceOption.Any(x => x.Id == 2))
				context.ServiceOption.AddOrUpdate(x => x.Id, new ServiceOption
				{
					Id = 2,
					Name = "AndroidVersionName",
					ValueType = "String",
					Value = "2.0.1"
				});
			if (!context.ServiceOption.Any(x => x.Id == 3))
				context.ServiceOption.AddOrUpdate(x => x.Id, new ServiceOption
				{
					Id = 3,
					Name = "LastOrderId",
					ValueType = "int",
					Value = "0"
				});

			context.Platform.AddOrUpdate(x => x.Type, new Platform
			{
				Id = 1,
				PushCertificate = "AIzaSyABrK580cO5WVbs6J7d3Zh1qYjSb_nz6S0",
				PushId = "849435164237",
				Type = DeviceType.Android
			});
		}
	}
}
