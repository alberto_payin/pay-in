﻿using PayIn.BusinessLogic.Common;
using PayIn.Infrastructure.Public.Db;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PayIn.Domain.Public;

namespace PayIn.Infrastructure.Public.Repositories
{
	public class ControlFormRepository : PublicRepository<ControlForm>
	{
		public readonly ISessionData SessionData;

		#region Constructors
		public ControlFormRepository(
			ISessionData sessionData, 
			IPublicContext context
		)
			: base(context)
		{
			if (sessionData == null) throw new ArgumentNullException("sessionData");
			SessionData = sessionData;
		}
		#endregion Constructors

		#region CheckHorizontalVisibility
		public override IQueryable<ControlForm> CheckHorizontalVisibility(IQueryable<ControlForm> that)
		{
			var result = that
				.Where(x =>
					x.Concession.Type == Common.ServiceType.Control && (
						x.Concession.Supplier.Login == SessionData.Login ||
						x.Concession.Supplier.Workers.Select(y => y.Login).Contains(SessionData.Login)
					)
				)
			;
			return result;
		}
		#endregion CheckHorizontalVisibility
	}
}
