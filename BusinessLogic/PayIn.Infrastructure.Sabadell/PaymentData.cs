﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayIn.Infrastructure.Sabadell
{
	public class PaymentData
	{
		public int PaymentMediaId { get; set; }
		public int PublicPaymentMediaId { get; set; }
		public int PublicTicketId { get; set; }
		public int PublicPaymentId { get; set; }
	}
}
