﻿using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Collections.Generic;
using System;
using System.Xml;
using System.IO;
using PayIn.Application.Dto.Internal.Arguments.PaymentMedia;
using System.Web;
using PayIn.Application.Dto.Internal.Results.PaymentMedia;

namespace PayIn.Infrastructure.Sabadell
{
	public static class SabadellGatewayFunctions
	{
		#region CreateWebPaymentRequest
		public static string CreateWebPaymentRequest(int orderId, string transactionType, string commerceUrl, string commerceCode, string commerceSecretKey, string commerceName, decimal amount, int paymentMediaId, int publicPaymentMediaId, int publicTicketId, int publicPaymentId, string currency = "EUR", string language = "es")
		{
			var orderString = orderId.ToString("#0000");
			var amountString = (amount * 100).ToString("#0");
			var currencyString = GetCurrencyToSabadell(currency);
			var commerceNameFull = "Payin - {0}".FormatString(commerceName);
			var cardIdentifier = "REQUIRED";
			var signature = Sha(amountString, orderString, commerceCode, currencyString, transactionType, commerceUrl, cardIdentifier, commerceSecretKey);

			var data = new PaymentData {
				PaymentMediaId = paymentMediaId,
				PublicPaymentMediaId = publicPaymentMediaId,
				PublicTicketId = publicTicketId,
				PublicPaymentId = publicPaymentId
			}.ToJson();

			var request = new List<string>();
			request.AddFormat("DS_Version=0.1");
			request.AddFormat("DS_MERCHANT_AMOUNT={0}", amountString);
			request.AddFormat("DS_MERCHANT_CURRENCY={0}", currencyString);
			request.AddFormat("DS_MERCHANT_ORDER={0}", orderString);
			request.AddFormat("DS_MERCHANT_MERCHANTCODE={0}", commerceCode);
			request.AddFormat("DS_MERCHANT_MERCHANTURL={0}", commerceUrl);
			request.AddFormat("DS_MERCHANT_MERCHANTNAME={0}", commerceNameFull);
			request.AddFormat("DS_MERCHANT_CONSUMERLANGUAGE={0}", GetLanguageToSabadell(language));
			request.AddFormat("DS_MERCHANT_MERCHANTSIGNATURE={0}", signature);
			request.AddFormat("DS_MERCHANT_TERMINAL={0}", 1);
			request.AddFormat("DS_MERCHANT_MERCHANTDATA={0}", data);
			request.AddFormat("DS_MERCHANT_TRANSACTIONTYPE={0}", transactionType);
			request.AddFormat("DS_MERCHANT_IDENTIFIER={0}", cardIdentifier);

			return request.JoinString("&");
		}
		#endregion CreateWebPaymentRequest

		#region GetWebPaymentResponse
		public static PaymentMediaCreateWebCardSabadellArguments GetWebPaymentResponse(string response)
		{
			var doc = new XmlDocument();
			doc.LoadXml(response);

			var nodes = doc.DocumentElement;
			var data = HttpUtility.UrlDecode(nodes.SelectSingleNode("/NOTIFICACIONXML/Ds_MerchantData").InnerText).FromJson<PaymentData>();
			var arguments = new PaymentMediaCreateWebCardSabadellArguments(
				version:                                   nodes.SelectSingleNode("/NOTIFICACIONXML/Ds_Version").Value,
				amount:            Convert.ToInt32(        nodes.SelectSingleNode("/NOTIFICACIONXML/Ds_Amount").InnerText) / 100,
				currency:          GetCurrencyFromSabadell(nodes.SelectSingleNode("/NOTIFICACIONXML/Ds_Currency").InnerText),
				orderId:           Convert.ToInt32(        nodes.SelectSingleNode("/NOTIFICACIONXML/Ds_Order").InnerText),
				commerceCode:                              nodes.SelectSingleNode("/NOTIFICACIONXML/Ds_MerchantCode").InnerText,
				terminal:          Convert.ToInt32(        nodes.SelectSingleNode("/NOTIFICACIONXML/Ds_Terminal").InnerText),
				response:                                  nodes.SelectSingleNode("/NOTIFICACIONXML/Ds_Response").InnerText,
				paymentMediaId:                            data.PaymentMediaId,
				publicPaymentMediaId:                      data.PublicPaymentMediaId,
				publicTicketId:                            data.PublicTicketId,
				publicPaymentId:                           data.PublicPaymentId,
				securePayment:                             nodes.SelectSingleNode("/NOTIFICACIONXML/Ds_SecurePayment").InnerText != "0",
				expirationMonth:   Convert.ToInt32(        nodes.SelectSingleNode("/NOTIFICACIONXML/Ds_ExpiryDate").InnerText.Substring(2)),
				expirationYear:    Convert.ToInt32(        nodes.SelectSingleNode("/NOTIFICACIONXML/Ds_ExpiryDate").InnerText.Substring(0, 2)),
				cardIdentifier:                            nodes.SelectSingleNode("/NOTIFICACIONXML/Ds_Merchant_Identifier").InnerText,
				transactionType:                           nodes.SelectSingleNode("/NOTIFICACIONXML/Ds_TransactionType").InnerText,
				authorizationCode:                         nodes.SelectSingleNode("/NOTIFICACIONXML/Ds_AuthorisationCode").InnerText,
				signature:                                 nodes.SelectSingleNode("/NOTIFICACIONXML/Ds_Signature").InnerText,
				cardNumberHash:                            nodes.SelectSingleNode("/NOTIFICACIONXML/Ds_Card_Number").InnerText,
				isError:                                   false
			);

			var error = CheckError(arguments.Response);
			var errorPublic = ErrorPublic(arguments.Response);
			if (error != null)
			{
				arguments.ErrorCode = error.Value.Key;
				arguments.ErrorText = error.Value.Value;
				arguments.ErrorPublic = errorPublic.Value.Value;
				arguments.IsError = true;
			}

			return arguments;
		}
		#endregion GetWebPaymentResponse

		#region CreatePaymentRequest
		public static string CreatePaymentRequest(string cardIdentifier, string transactionType, string commerceCode, string commerceSecretKey, string commerceName, decimal amount, int paymentMediaId, int publicPaymentMediaId, int publicTicketId, int publicPaymentId, int orderId, string currency = "EUR", string language = "es")
		{
			var orderString = orderId.ToString("#0000");
			var amountString = (amount * 100).ToString("#0");
			var currencyString = GetCurrencyToSabadell(currency);
			var commerceNameFull = "Payin - {0}".FormatString(commerceName);
			var signature = Sha(amountString, orderString, commerceCode, currencyString, transactionType, "", cardIdentifier, commerceSecretKey);

			var data = new PaymentData {
				PaymentMediaId = paymentMediaId,
				PublicPaymentMediaId = publicPaymentMediaId,
				PublicTicketId = publicTicketId,
				PublicPaymentId = publicPaymentId
			}.ToJson();

			var request = new StringBuilder();
			request.AppendFormat("<DATOSENTRADA>");
			request.AppendFormat("<DS_Version>{0}</DS_Version>", "0.1");
			request.AppendFormat("<DS_MERCHANT_AMOUNT>{0}</DS_MERCHANT_AMOUNT>", amountString);
			request.AppendFormat("<DS_MERCHANT_CURRENCY>{0}</DS_MERCHANT_CURRENCY>", currencyString);
			request.AppendFormat("<DS_MERCHANT_ORDER>{0}</DS_MERCHANT_ORDER>", orderString);
			request.AppendFormat("<DS_MERCHANT_MERCHANTCODE>{0}</DS_MERCHANT_MERCHANTCODE>", commerceCode);
			request.AppendFormat("<DS_MERCHANT_MERCHANTNAME>{0}</DS_MERCHANT_MERCHANTNAME>", commerceNameFull);
			request.AppendFormat("<DS_MERCHANT_CONSUMERLANGUAGE>{0}</DS_MERCHANT_CONSUMERLANGUAGE>", GetLanguageToSabadell(language));
			request.AppendFormat("<DS_MERCHANT_MERCHANTSIGNATURE>{0}</DS_MERCHANT_MERCHANTSIGNATURE>", signature);
			request.AppendFormat("<DS_MERCHANT_TERMINAL>{0}</DS_MERCHANT_TERMINAL>", 1);
			request.AppendFormat("<DS_MERCHANT_TRANSACTIONTYPE>{0}</DS_MERCHANT_TRANSACTIONTYPE>", transactionType);
			request.AppendFormat("<DS_MERCHANT_DATA>{0}</DS_MERCHANT_DATA>", data);
			request.AppendFormat("<DS_MERCHANT_IDENTIFIER>{0}</DS_MERCHANT_IDENTIFIER>", cardIdentifier);
			request.AppendFormat("</DATOSENTRADA>");

			return request.ToString();
		}
		#endregion CreatePaymentRequest

		#region GetPaymentResponse
		public static PaymentMediaPayResult GetPaymentResponse(string data)
		{
			var doc = new XmlDocument();
			doc.LoadXml(data);

			var nodes = doc.DocumentElement;
			var arguments = new PaymentMediaPayResult
			{
				Amount =                                  GetXmlInt   (nodes, "/RETORNOXML/OPERACION/Ds_Amount") / 100,
				Currency =        GetCurrencyFromSabadell(GetXmlString(nodes, "/RETORNOXML/OPERACION/Ds_Currency")),
				OrderId =                                 GetXmlInt   (nodes, "/RETORNOXML/OPERACION/Ds_Order"),
				Signature =                               GetXmlString(nodes, "/RETORNOXML/OPERACION/Ds_Signature"),
				CommerceCode =                            GetXmlString(nodes, "/RETORNOXML/OPERACION/Ds_MerchantCode"),
				Terminal =                                GetXmlInt   (nodes, "/RETORNOXML/OPERACION/Ds_Terminal"),
				Response =                                GetXmlString(nodes, "/RETORNOXML/OPERACION/Ds_Response"),
				Code =                                    GetXmlString(nodes, "/RETORNOXML/CODIGO"),
				AuthorizationCode =                       GetXmlString(nodes, "/RETORNOXML/OPERACION/Ds_AuthorisationCode"),
				TransactionType =                         GetXmlString(nodes, "/RETORNOXML/OPERACION/Ds_TransactionType"),
				SecurePayment =                           GetXmlString(nodes, "/RETORNOXML/OPERACION/Ds_SecurePayment") != "0",
				Language =                                GetXmlString(nodes, "/RETORNOXML/OPERACION/Ds_Language"),
				CardIdentifier =                          GetXmlString(nodes, "/RETORNOXML/OPERACION/Ds_Merchant_Identifier"),
				MerchantData =      HttpUtility.UrlDecode(GetXmlString(nodes, "/RETORNOXML/OPERACION/Ds_MerchantData")).FromJson<PaymentMediaPayResult.Data>(),
				CardCountry =                             GetXmlString(nodes, "/RETORNOXML/OPERACION/Ds_Card_Country"),
				IsError = false
			};

			var error = CheckError(arguments.Response, arguments.Code);
			var errorPublic = ErrorPublic(arguments.Response, arguments.Code);
			if (error != null)
			{
				arguments.ErrorCode = error.Value.Key;
				arguments.ErrorText = error.Value.Value;
				arguments.ErrorPublic = errorPublic.Value.Value;
				arguments.IsError = true;
			}

			return arguments;
		}
		#endregion GetPaymentResponse

		#region GetCurrencyToSabadell
		public static string GetCurrencyToSabadell(string currency)
		{
			switch (currency.ToLower())
			{
				case "eur": { return "978"; } // 978 – Euro
				// 978 – Euro
				// 840 – Dólar
				// 826 – Libra Esterlina
				// 392 – Yen
				// 032 – Peso Argentino
				// 124 – Dólar Canadiense
				// 152 – Peso Chileno
				// 170 – Peso Colombiano
				// 356 – Rupia India
				// 484 – Nuevo Peso Mejicano
				// 604 – Nuevos Soles
				// 756 – Franco Suizo
				// 986 – Real Brasileño
				// 937 – Bolívar Venezolano
				// 949 – Lira Turca
			}
			return "";
		}
		#endregion GetCurrencyToSabadell

		#region GetCurrencyFromSabadell
		public static string GetCurrencyFromSabadell(string currency)
		{
			switch (currency)
			{
				case "978": { return "EUR"; } // 978 – Euro
				// 978 – Euro
				// 840 – Dólar
				// 826 – Libra Esterlina
				// 392 – Yen
				// 032 – Peso Argentino
				// 124 – Dólar Canadiense
				// 152 – Peso Chileno
				// 170 – Peso Colombiano
				// 356 – Rupia India
				// 484 – Nuevo Peso Mejicano
				// 604 – Nuevos Soles
				// 756 – Franco Suizo
				// 986 – Real Brasileño
				// 937 – Bolívar Venezolano
				// 949 – Lira Turca
			}
			return "";
		}
		#endregion GetCurrencyFromSabadell

		#region GetLanguageToSabadell
		public static string GetLanguageToSabadell(string language)
		{
			switch (language.ToLower())
			{
				case "es": { return "1"; } // 1  – Castellano
				// 2  – Inglés
				// 3  – Catalán
				// 4  – Francés
				// 5  – Alemán
				// 6  – Holandés
				// 7  – Italiano
				// 8  – Sueco
				// 9  – Portugués
				// 10 – Valenciano
				// 11 – Polaco
				// 12 – Gallego
				// 13 – Euskera
				default: { return "0"; } // 0  – Cliente
			}
		}
		#endregion GetLanguageToSabadell

		#region Sha
		public static string Sha(string amountString, string orderString, string commerceCode, string currencyString, string transactionType, string commerceUrl, string cardIdentifier, string commerceSecretKey)
		{
			// Apartado 7.6
			var data = amountString + orderString + commerceCode + currencyString + transactionType + commerceUrl + cardIdentifier + commerceSecretKey;
			var sha = new SHA1CryptoServiceProvider();
			var result = sha.ComputeHash(Encoding.Default.GetBytes(data.ToCharArray()));

			var stringBuilder = new StringBuilder(40);
			for (int i = 0; i < result.Length; i++)
				stringBuilder.Append(result[i].ToString("x2"));

			return stringBuilder.ToString();
		}
		public static string Sha(string data)
		{
			var sha = new SHA1CryptoServiceProvider();
			var result = sha.ComputeHash(Encoding.Default.GetBytes(data.ToCharArray()));

			var stringBuilder = new StringBuilder(40);
			for (int i = 0; i < result.Length; i++)
				stringBuilder.Append(result[i].ToString("x2"));

			return stringBuilder.ToString();
		}
		#endregion Sha

		#region GetXmlString
		private static string GetXmlString(XmlNode node, string path)
		{
			var element = node.SelectSingleNode(path);
			return element == null ? "" : element.InnerText;
		}
		#endregion GetXmlString

		#region GetXmlInt
		private static int GetXmlInt(XmlNode node, string path)
		{
			var element = node.SelectSingleNode(path);
			return element == null ? 0 : Convert.ToInt32(element.InnerText);
		}
		#endregion GetXmlInt

		#region CheckError
		public static KeyValuePair<string, string>? CheckError(string response, string code = "0")
		{
			if (code != "0")
				return new KeyValuePair<string, string>(code, SabadellErrors.ResourceManager.GetString(code) ?? "");
			else
			{
				var resp = Convert.ToInt32(response);
				if ((resp > 99) && (resp != 900))
				{					
						return new KeyValuePair<string, string>(response, SabadellErrors.ResourceManager.GetString("Response_" + response) ?? "");					

				}
			}

			return null;
		}
		#endregion CheckError

		#region ErrorPublic
		public static KeyValuePair<string, string>? ErrorPublic(string response, string code = "0")
		{
			if (code != "0")
				return new KeyValuePair<string, string>(code, SabadellErrors.ResourceManager.GetString(code) ?? "");
			else
			{
				var resp = Convert.ToInt32(response);
				if ((resp > 99) && (resp != 900))
				{
					if ((resp == 101) || (resp == 201))
					{
						//Tarjeta caducada
						return new KeyValuePair<string, string>(response, SabadellErrors.ResourceManager.GetString("Response_" + response) ?? "");
					}
					else if ((resp == 116))
					{
						//Saldo insuficiente
						return new KeyValuePair<string, string>(response, "Saldo insuficiente");

					}
					else if ((resp == 129) || (resp == 280))
					{
						//CVV o CCV incorrecto
						return new KeyValuePair<string, string>(response, SabadellErrors.ResourceManager.GetString("Response_" + response) ?? "");

					}
					else
					{
						//Error general
						return new KeyValuePair<string, string>(response, "Error en la transacción");

					}

				}
			}

			return null;
		}
		#endregion ErrorPublic
	}
}
