namespace PayIn.Infrastructure.Security.Db.Migrations
{
	using Microsoft.AspNet.Identity;
	using Microsoft.AspNet.Identity.EntityFramework;
	using PayIn.Domain.Security;
	using PayIn.Infrastructure.Security.Db.UserManager;
	using System;
	using System.Data.Entity.Migrations;
	using System.Linq;
    using PayIn.Common;

	internal sealed class Configuration : DbMigrationsConfiguration<PayIn.Infrastructure.Security.Db.AuthContext>
	{
		#region Constructors
		public Configuration()
		{
			AutomaticMigrationsEnabled = false;
		}
		#endregion Constructors

		#region Seed
		protected override void Seed(PayIn.Infrastructure.Security.Db.AuthContext context)
		{
			context.Roles.AddOrUpdate(x => x.Name,
				new IdentityRole { Id = Guid.NewGuid().ToString(), Name = AccountRoles.Superadministrator },
				new IdentityRole { Id = Guid.NewGuid().ToString(), Name = AccountRoles.Administrator      },
				new IdentityRole { Id = Guid.NewGuid().ToString(), Name = AccountRoles.User               },
				new IdentityRole { Id = Guid.NewGuid().ToString(), Name = AccountRoles.Commerce           },
				new IdentityRole { Id = Guid.NewGuid().ToString(), Name = AccountRoles.CommercePayment    },
				new IdentityRole { Id = Guid.NewGuid().ToString(), Name = AccountRoles.Operator           },
				new IdentityRole { Id = Guid.NewGuid().ToString(), Name = AccountRoles.Tester             },
				new IdentityRole { Id = Guid.NewGuid().ToString(), Name = AccountRoles.ControlApi         },
				new IdentityRole { Id = Guid.NewGuid().ToString(), Name = AccountRoles.PaymentWorker      }
			);
			context.Clients.AddOrUpdate(
				new Client { Id = AccountClientId.Web,           Secret = "PayInWebApp@123456".ToHash(),           Name = "PayIn Web Application",            ApplicationType = ApplicationTypes.JavaScript,         Active = true, RefreshTokenLifeTime = Convert.ToInt32(TimeSpan.FromDays(360).TotalMinutes), AllowedOrigin = ""  },
				new Client { Id = AccountClientId.Ios,           Secret = "PayInIosApp@123456".ToHash(),           Name = "PayIn iOS Application",            ApplicationType = ApplicationTypes.NativeConfidential, Active = true, RefreshTokenLifeTime = Convert.ToInt32(TimeSpan.FromDays(360).TotalMinutes), AllowedOrigin = "*" },
				new Client { Id = AccountClientId.Android,       Secret = "PayInAndroidApp@123456".ToHash(),       Name = "PayIn Android Application",        ApplicationType = ApplicationTypes.NativeConfidential, Active = true, RefreshTokenLifeTime = Convert.ToInt32(TimeSpan.FromDays(360).TotalMinutes), AllowedOrigin = "*" },
				new Client { Id = AccountClientId.Wp,            Secret = "PayInWpApp@123456".ToHash(),            Name = "PayIn WP Application",             ApplicationType = ApplicationTypes.NativeConfidential, Active = true, RefreshTokenLifeTime = Convert.ToInt32(TimeSpan.FromDays(360).TotalMinutes), AllowedOrigin = "*" },
				new Client { Id = AccountClientId.Api,           Secret = "PayInApi@1492".ToHash(),                Name = "PayIn API",                        ApplicationType = ApplicationTypes.JavaScript        , Active = true, RefreshTokenLifeTime = Convert.ToInt32(TimeSpan.FromDays(360).TotalMinutes), AllowedOrigin = "" },
				new Client { Id = AccountClientId.AndroidNative, Secret = "PayInAndroidNativeApp@123456".ToHash(), Name = "PayIn Android Native Application", ApplicationType = ApplicationTypes.NativeConfidential, Active = true, RefreshTokenLifeTime = Convert.ToInt32(TimeSpan.FromDays(360).TotalMinutes), AllowedOrigin = "*" },
				new Client { Id = AccountClientId.Tpv,           Secret = "PayInTpv@1238".ToHash(),                Name = "PayIn TPV",                        ApplicationType = ApplicationTypes.NativeConfidential, Active = true, RefreshTokenLifeTime = Convert.ToInt32(TimeSpan.FromDays(360).TotalMinutes), AllowedOrigin = "*" }
			);
			context.SaveChanges();

			if (!context.Users.Any(x => x.UserName == "superadministrator@pay-in.es"))
			{
				var user = new ApplicationUser
				{
					UserName = "superadministrator@pay-in.es",
					Email = "superadministrator@pay-in.es",
					Name = "Superadministrador",
					Mobile = "000000000",
					TaxName = "Superadministrador Pay[in]",
					TaxNumber = "12345678A",
					TaxAddress = "Valencia",
                    Birthday = DateTime.Now,
                    Sex = SexType.Undefined,
                    PhotoUrl = ""
				};
				var role = context.Roles
					.FirstOrDefault(x => x.Name == AccountRoles.Superadministrator);
				user.Roles.Add(new IdentityUserRole { UserId = user.Id, RoleId = role.Id });

				var userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(context));
				userManager.UserValidator = new UserValidator<ApplicationUser>(userManager) { AllowOnlyAlphanumericUserNames = false };

				var result = userManager.Create(user, "Pa$$w0rd");
				if ((!result.Succeeded) && (result.Errors != null))
					throw new ApplicationException(string.Format("Identity system error: {0}", string.Join("\n", result.Errors))); 
				
				user.EmailConfirmed = true;
				result = userManager.SetLockoutEnabled(user.Id, false);
				if ((!result.Succeeded) && (result.Errors != null))
					throw new ApplicationException(string.Format("Identity system error: {0}", string.Join("\n", result.Errors)));
			}
#if DEBUG
			if (!context.Users.Any(x => x.UserName == "commerce@pay-in.es"))
			{
				var user = new ApplicationUser
				{
					UserName = "commerce@pay-in.es",
					Email = "commerce@pay-in.es",
					Name = "Comercio",
					Mobile = "000000000",
					TaxName = "Comercio Pay[in]",
					TaxNumber = "12345678A",
					TaxAddress = "Valencia",
					Birthday = DateTime.Now,
					Sex = SexType.Undefined,
					PhotoUrl = ""
				};
				var role = context.Roles
					.FirstOrDefault(x => x.Name == AccountRoles.Commerce);
				user.Roles.Add(new IdentityUserRole { UserId = user.Id, RoleId = role.Id });
				role = context.Roles
					.FirstOrDefault(x => x.Name == AccountRoles.User);
				user.Roles.Add(new IdentityUserRole { UserId = user.Id, RoleId = role.Id });

				var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
				userManager.UserValidator = new UserValidator<ApplicationUser>(userManager) { AllowOnlyAlphanumericUserNames = false };

				var result = userManager.Create(user, "Pa$$w0rd");
				if ((!result.Succeeded) && (result.Errors != null))
					throw new ApplicationException(string.Format("Identity system error: {0}", string.Join("\n", result.Errors)));

				user.EmailConfirmed = true;
				result = userManager.SetLockoutEnabled(user.Id, false);
				if ((!result.Succeeded) && (result.Errors != null))
					throw new ApplicationException(string.Format("Identity system error: {0}", string.Join("\n", result.Errors)));
			}
			if (!context.Users.Any(x => x.UserName == "payment@pay-in.es"))
			{
				var user = new ApplicationUser
				{
					UserName = "payment@pay-in.es",
					Email = "payment@pay-in.es",
					Name = "Comercio pagos",
					Mobile = "000000000",
					TaxName = "Comercio pagos Pay[in]",
					TaxNumber = "12345678A",
					TaxAddress = "Valencia",
					Birthday = DateTime.Now,
					Sex = SexType.Undefined,
					PhotoUrl = ""
				};
				var role = context.Roles
					.FirstOrDefault(x => x.Name == AccountRoles.User);
				user.Roles.Add(new IdentityUserRole { UserId = user.Id, RoleId = role.Id });
				role = context.Roles
					.FirstOrDefault(x => x.Name == AccountRoles.CommercePayment);
				user.Roles.Add(new IdentityUserRole { UserId = user.Id, RoleId = role.Id });

				var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
				userManager.UserValidator = new UserValidator<ApplicationUser>(userManager) { AllowOnlyAlphanumericUserNames = false };

				var result = userManager.Create(user, "Pa$$w0rd");
				if ((!result.Succeeded) && (result.Errors != null))
					throw new ApplicationException(string.Format("Identity system error: {0}", string.Join("\n", result.Errors)));

				user.EmailConfirmed = true;
				result = userManager.SetLockoutEnabled(user.Id, false);
				if ((!result.Succeeded) && (result.Errors != null))
					throw new ApplicationException(string.Format("Identity system error: {0}", string.Join("\n", result.Errors)));
			}
			if (!context.Users.Any(x => x.UserName == "operator@pay-in.es"))
			{
				var user = new ApplicationUser
				{
					UserName = "operator@pay-in.es",
					Email = "operator@pay-in.es",
					Name = "Operador",
					Mobile = "000000000",
					TaxName = "Operador Pay[in]",
					TaxNumber = "12345678A",
                    TaxAddress = "Valencia",
                    Birthday = DateTime.Now,
                    Sex = SexType.Undefined,
                    PhotoUrl = ""
				};
				var role = context.Roles
					.FirstOrDefault(x => x.Name == AccountRoles.Operator);
				user.Roles.Add(new IdentityUserRole { UserId = user.Id, RoleId = role.Id });
				role = context.Roles
					.FirstOrDefault(x => x.Name == AccountRoles.User);
				user.Roles.Add(new IdentityUserRole { UserId = user.Id, RoleId = role.Id });

				var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
				userManager.UserValidator = new UserValidator<ApplicationUser>(userManager) { AllowOnlyAlphanumericUserNames = false };

				var result = userManager.Create(user, "Pa$$w0rd");
				if ((!result.Succeeded) && (result.Errors != null))
					throw new ApplicationException(string.Format("Identity system error: {0}", string.Join("\n", result.Errors)));

				user.EmailConfirmed = true;
				result = userManager.SetLockoutEnabled(user.Id, false);
				if ((!result.Succeeded) && (result.Errors != null))
					throw new ApplicationException(string.Format("Identity system error: {0}", string.Join("\n", result.Errors)));
			}
			if (!context.Users.Any(x => x.UserName == "user@pay-in.es"))
			{
				var user = new ApplicationUser
				{
					UserName = "user@pay-in.es",
					Email = "user@pay-in.es",
					Name = "Usuario",
					Mobile = "000000000",
					TaxName = "Usuario Pay[in]",
					TaxNumber = "12345678A",
                    TaxAddress = "Valencia",
                    Birthday = DateTime.Now,
                    Sex = SexType.Undefined,
                    PhotoUrl = ""
				};
				var role = context.Roles
					.FirstOrDefault(x => x.Name == AccountRoles.User);
				user.Roles.Add(new IdentityUserRole { UserId = user.Id, RoleId = role.Id });

				var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
				userManager.UserValidator = new UserValidator<ApplicationUser>(userManager) { AllowOnlyAlphanumericUserNames = false };

				var result = userManager.Create(user, "Pa$$w0rd");
				if ((!result.Succeeded) && (result.Errors != null))
					throw new ApplicationException(string.Format("Identity system error: {0}", string.Join("\n", result.Errors)));

				user.EmailConfirmed = true;
				result = userManager.SetLockoutEnabled(user.Id, false);
				if ((!result.Succeeded) && (result.Errors != null))
					throw new ApplicationException(string.Format("Identity system error: {0}", string.Join("\n", result.Errors)));
			}
			if (!context.Users.Any(x => x.UserName == "api@pay-in.es"))
			{
				var user = new ApplicationUser
				{
					UserName = "api@pay-in.es",
					Email = "api@pay-in.es",
					Name = "API",
					Mobile = "000000000",
					TaxName = "Api Pay[in]",
					TaxNumber = "12345678A",
					TaxAddress = "Valencia",
                    Birthday = DateTime.Now,
                    Sex = SexType.Undefined,
                    PhotoUrl = ""
				};
				var role = context.Roles
					.FirstOrDefault(x => x.Name == AccountRoles.ControlApi);
				user.Roles.Add(new IdentityUserRole { UserId = user.Id, RoleId = role.Id });

				var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
				userManager.UserValidator = new UserValidator<ApplicationUser>(userManager) { AllowOnlyAlphanumericUserNames = false };

				var result = userManager.Create(user, "Pa$$w0rd");
				if ((!result.Succeeded) && (result.Errors != null))
					throw new ApplicationException(string.Format("Identity system error: {0}", string.Join("\n", result.Errors)));

				user.EmailConfirmed = true;
				result = userManager.SetLockoutEnabled(user.Id, false);
				if ((!result.Succeeded) && (result.Errors != null))
					throw new ApplicationException(string.Format("Identity system error: {0}", string.Join("\n", result.Errors)));
			}
#endif // DEBUG

			base.Seed(context);
		}
		#endregion Seed
	}
}
