﻿using Microsoft.AspNet.Identity;
using System;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading.Tasks;

namespace PayIn.DistributedServices.Security.Db.Services
{
	public class EmailService : IIdentityMessageService
	{
		public Task SendAsync(IdentityMessage message)
		{
			var msg = new MailMessage("system@pay-in.es", message.Destination);
			msg.Subject = message.Subject;
			msg.Body = message.Body;
			msg.IsBodyHtml = true;

			var client = new SmtpClient("smtp.1and1.com", 587)
			{
				DeliveryMethod = SmtpDeliveryMethod.Network,
				Credentials = new NetworkCredential("system@pay-in.es", "Payin2014!")
			};

			return client.SendMailAsync(msg);
		}
	}
}
