﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using PayIn.Application.Dto.Security.Arguments;
using PayIn.Common;
using PayIn.Common.Security;
using PayIn.Domain.Security;
using PayIn.Infrastructure.Security.Db;
using PayIn.Infrastructure.Security.Db.UserManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using Xp.Common;
using Xp.Infrastructure.Repositories;
using System.Text.RegularExpressions;
using Xp.Infrastructure.Services;
using System.Web;

namespace PayIn.Infrastructure.Security
{
	public class SecurityRepository : IDisposable
	{
#if TEST
		string url = "http://payin-test.cloudapp.net";
#elif DEBUG || EMULATOR
		string url = "http://localhost:8080";
#else
		string url = "https://control.pay-in.es";
#endif

		private readonly AuthContext Context;
		private readonly ApplicationUserManager UserManager;
		private readonly RoleManager<IdentityRole> RoleManager;

#region Constructors
		public SecurityRepository()
		{
			Context = new AuthContext();
			UserManager = ApplicationUserManager.Create(new UserStore<ApplicationUser>(Context));
			RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(Context));
		}
#endregion Constructors

#region GetUserAsync
		public async Task<ApplicationUser> GetUserAsync(string name)
		{
			var user = await UserManager.FindByNameAsync(name);
			return user;
		}
#endregion GetUserAsync

#region GetUserIdentityAsync
		public async Task<ApplicationUser> GetUserIdentityAsync()
		{
			var identity = Thread.CurrentPrincipal.Identity as ClaimsIdentity;
			var login = identity.Name;

			var user = await UserManager.FindByNameAsync(login);

			return user;
		}
#endregion GetUserIdentityAsync

#region CreateUserAsync
		public async Task CreateUserAsync(AccountRegisterArguments userModel, string roleName)
		{
			if (!userModel.AcceptTerms)
				throw new ApplicationException(string.Format("To register you must accept terms of service"));

			var role = Context.Roles
				.Where(x => x.Name == roleName)
				.FirstOrDefault();
			if (role == null)
				throw new ApplicationException(string.Format("Identity role not exists {0}", roleName));

			var oldUser = await UserManager.FindByNameAsync(userModel.UserName);
			if (oldUser != null && !oldUser.EmailConfirmed)
				UserManager.Delete(oldUser);

			var user = new ApplicationUser
			{
				UserName = userModel.UserName,
				Email = userModel.UserName,
				Name = userModel.Name,
				Mobile = userModel.Mobile,
				TaxName = userModel.TaxName ?? "",
				TaxNumber = userModel.TaxNumber ?? "",
				TaxAddress = userModel.TaxAddress ?? "",
				Sex = SexType.Undefined,
				Birthday = userModel.Birthday,
				PhotoUrl = userModel.PhotoUrl ?? "",
			};

			user.Roles.Add(new IdentityUserRole { UserId = user.Id, RoleId = role.Id });
			ThrowExceptionIfError(await UserManager.CreateAsync(user, userModel.Password));

			var token = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
			await UserManager.SendEmailAsync(
				user.Id,
				SecurityResources.ConfirmEmailSubject,
				string.Format(
					SecurityResources.ConfirmEmailContent,
					user.Id,
				 HttpUtility.UrlEncode(token),
					user.Name,
					user.Email,
					url
                )
			);

			var analytics = new MixPanelService(null);
			await analytics.TrackEventAsync("AccountCreate", new Dictionary<string, object>() { { "distinct_id", user.Email } });
		}

#endregion CreateUserAsync

#region ConfirmEmail
		public async Task ConfirmEmail(AccountConfirmArguments arguments)
		{
			var response = await UserManager.ConfirmEmailAsync(arguments.userId, arguments.code);
			ThrowExceptionIfError(response);
		}
#endregion ConfirmEmail

#region UpdatePassword
		public async Task UpdatePassword(AccountChangePasswordArguments userModel)
		{
			var identity = Thread.CurrentPrincipal.Identity as ClaimsIdentity;
			var login = identity.Name;

			var user = await UserManager.FindByNameAsync(login);

			var result = await UserManager.ChangePasswordAsync(user.Id, userModel.OldPassword, userModel.Password);
			ThrowExceptionIfError(result);
		}
#endregion UpdatePassword

#region ForgotPassword
		public async Task ForgotPassword(AccountForgotPasswordArguments arguments)
		{
			var user = await UserManager.FindByNameAsync(arguments.Email);
			if (user == null)
				return;

			var token = UserManager.GeneratePasswordResetToken(user.Id);
			await UserManager.SendEmailAsync(
				user.Id,
				SecurityResources.ForgotPasswordSubject,
				string.Format(
					SecurityResources.ForgotPasswordContent,
						user.Id,
						token,
						user.Name,
						user.Email
					)
			);
		}
#endregion ForgotPassword

#region ConfirmForgotPassword
		public async Task ConfirmForgotPassword(AccountConfirmForgotPasswordArguments arguments)
		{
			var result = await UserManager.ResetPasswordAsync(arguments.UserId, arguments.Code, arguments.Password);
			ThrowExceptionIfError(result);
		}
#endregion ConfirmForgotPassword

#region OverridePassword
		public async Task OverridePassword(AccountOverridePasswordArguments userModel)
		{
			var user = await UserManager.FindByNameAsync(userModel.UserName);

			var result = await UserManager.RemovePasswordAsync(user.Id);
			ThrowExceptionIfError(result);

			result = await UserManager.AddPasswordAsync(user.Id, userModel.Password);
			ThrowExceptionIfError(result);
		}
#endregion OverridePassword

#region AutenticateUserAsync
		public async Task<ApplicationUser> AutenticateUserAsync(string userName, string password)
		{
			var item = await UserManager.FindAsync(userName, password);
			return item;
		}
#endregion AutenticateUserAsync

#region GetRoleAsync
		public async Task<IEnumerable<IdentityRole>> GetRoleAsync(IEnumerable<string> ids)
		{
			var result = new List<IdentityRole>();
			foreach (var id in ids)
				result.Add(await RoleManager.FindByIdAsync(id));

			return result;
		}
#endregion GetRoleAsync

#region GetClientAsync
		public async Task<Client> GetClientAsync(string clientId)
		{
			var client = await Context.Clients.FindAsync(clientId);
			return client;
		}
#endregion GetClientAsync

#region GetRefreshTokenAsync
		public async Task<RefreshToken> GetRefreshTokenAsync(string refreshTokenId)
		{
			var refreshToken = await Context.RefreshTokens.FindAsync(refreshTokenId);

			return refreshToken;
		}
#endregion GetRefreshTokenAsync

#region UpdateRefreshTokenAsync
		public async Task<bool> UpdateRefreshTokenAsync(RefreshToken token)
		{
			var previousToken = Context.RefreshTokens
				.Where(x => x.Subject == token.Subject && x.ClientId == token.ClientId)
				.FirstOrDefault();

			if (previousToken != null)
				await DeleteRefreshTokenAsync(previousToken);

			Context.RefreshTokens.Add(token);

			return await Context.SaveChangesAsync() > 0;
		}
#endregion UpdateRefreshTokenAsync

#region DeleteRefreshTokenAsync
		public async Task<bool> DeleteRefreshTokenAsync()
		{
			var identity = Thread.CurrentPrincipal.Identity as ClaimsIdentity;

			var userClientIds = (Thread.CurrentPrincipal as ClaimsPrincipal).Claims
					.Where(x => x.Type == XpClaimTypes.ClientId)
					.Select(x => x.Value)
					.FirstOrDefault();
			
			var refreshToken = Context.RefreshTokens.Where(x => x.Subject == identity.Name && x.ClientId == userClientIds).FirstOrDefault();
			if (refreshToken == null)
				return false;

			return await DeleteRefreshTokenAsync(refreshToken);
		}
		public async Task<bool> DeleteRefreshTokenAsync(string refreshTokenId)
		{
			var refreshToken = await Context.RefreshTokens.FindAsync(refreshTokenId);
			if (refreshToken == null)
				return false;

			return await DeleteRefreshTokenAsync(refreshToken);
		}
		public async Task<bool> DeleteRefreshTokenAsync(RefreshToken refreshToken)
		{
			Context.RefreshTokens.Remove(refreshToken);
			return await Context.SaveChangesAsync() > 0;
		}
#endregion DeleteRefreshTokenAsync

#region UpdateRoles
		public async Task<bool> AddRole(String userName, String roleName)
		{
			var user = await UserManager.FindByNameAsync(userName);
			if (user == null)
				return false;

			var role = await RoleManager.FindByNameAsync(roleName);
			if (role == null)
				return false;

			await UserManager.AddToRolesAsync(user.Id, role.Name);

			return true;
		}
		public async Task<bool> RemoveRole(String userName, String roleName)
		{
			var user = await UserManager.FindByNameAsync(userName);
			if (user == null)
				return false;

			var role = await RoleManager.FindByNameAsync(roleName);
			if (role == null)
				return false;

			await UserManager.RemoveFromRolesAsync(user.Id, role.Name);

			return true;
		}
#endregion UpdateRoles

#region Dispose
		public void Dispose()
		{
			if (Context != null)
				Context.Dispose();
			if (UserManager != null)
				UserManager.Dispose();
		}
#endregion Dispose

#region ThrowExceptionIfError
		private void ThrowExceptionIfError(IdentityResult result)
		{
			if ((!result.Succeeded) && (result.Errors != null))
				throw new ApplicationException(string.Format("Identity system error: {0}", string.Join("\n", result.Errors)));
		}
#endregion ThrowExceptionIfError

#region UpdateProfileAsync
		public async Task UpdateProfileAsync(AccountUpdateArguments arguments)
		{
			var identity = Thread.CurrentPrincipal.Identity as ClaimsIdentity;
			var login = identity.Name;

			var user = await UserManager.FindByNameAsync(login);

			user.Name = arguments.Name;
			user.Mobile = arguments.Mobile;
			user.Sex = arguments.Sex ?? user.Sex;
			user.TaxNumber = arguments.TaxNumber;
			user.TaxName = arguments.TaxName;
			user.TaxAddress = arguments.TaxAddress;
			user.Birthday = arguments.Birthday.Value;

			await UserManager.UpdateAsync(user);

		}
#endregion UpdateProfileAsync

#region UpdateTaxDataAsync
		public async Task UpdateTaxDataAsync(string login, string taxNumber, string taxName, string taxAddress)
		{
			var user = await UserManager.FindByNameAsync(login);

			user.TaxNumber = taxNumber;
			user.TaxName = taxName;
			user.TaxAddress = taxAddress;

			await UserManager.UpdateAsync(user);
		}
#endregion UpdateTaxDataAsync

#region UpdateImageUrlAsync
		//Falta testeo -- israel.perez@Pay-in.es
		public async Task UpdateImageUrlAsync(byte[] image)
		{
			var identity = Thread.CurrentPrincipal.Identity as ClaimsIdentity;
			var login = identity.Name;
			var user = await UserManager.FindByNameAsync(login);
			var repository = new AzureBlobRepository();



#if TEST || DEBUG || EMULATOR
			user.PhotoUrl = SecurityResources.FotoUrlTest.FormatString(user.Id);
			repository.SaveImage(SecurityResources.FotoShortUrlTest.FormatString(user.Id), image);
#else // TEST || DEBUG
				user.PhotoUrl = SecurityResources.FotoUrl.FormatString(user.Id);
				repository.SaveImage(SecurityResources.FotoShortUrl.FormatString(user.Id), image);
#endif // TEST || DEBUG

			await UserManager.UpdateAsync(user);
		}
#endregion UpdateImageUrlAsync

#region DeleteImageAsync
		public async Task DeleteImageAsync()
		{
			var identity = Thread.CurrentPrincipal.Identity as ClaimsIdentity;
			var login = identity.Name;
			var user = await UserManager.FindByNameAsync(login);
			var repository = new AzureBlobRepository();

			if (user.PhotoUrl != "")
			{
				var route = Regex.Split(user.PhotoUrl, "[/?]");
				var fileName = route[route.Length - 2] + "/" + route[route.Length - 1];
				repository.DeleteFile(fileName);
				user.PhotoUrl = "";

				await UserManager.UpdateAsync(user);
			}
		}
#endregion DeleteImageAsync
	}
}