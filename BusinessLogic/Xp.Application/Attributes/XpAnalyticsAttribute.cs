﻿using System;

namespace Xp.Application.Attributes
{
	[AttributeUsage(AttributeTargets.Class)]
	public class XpAnalyticsAttribute : Attribute
	{
		public string RelatedClass { get; set; }
		public string RelatedMethod { get; set; }
		
		#region Constructors
		public XpAnalyticsAttribute(string relatedClass, string relatedMethod)
		{
			RelatedClass = relatedClass;
			RelatedMethod = relatedMethod;
		}
		#endregion Constructors
	}
}
