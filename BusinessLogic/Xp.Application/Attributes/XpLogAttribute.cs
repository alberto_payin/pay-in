﻿using System;

namespace Xp.Application.Attributes
{
	[AttributeUsage(AttributeTargets.Class)]
	public class XpLogAttribute : Attribute
	{
		public string RelatedClass { get; set; }
		public string RelatedMethod { get; set; }

		#region Constructors
		public XpLogAttribute(string relatedClass, string relatedMethod)
		{
			RelatedClass = relatedClass;
			RelatedMethod = relatedMethod;
		}
		#endregion Constructors
	}
}
