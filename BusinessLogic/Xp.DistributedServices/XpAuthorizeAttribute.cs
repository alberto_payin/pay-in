﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Controllers;
using Xp.Common;
using Xp.Common.Resources;

namespace PayIn.Web.Security
{
	[AttributeUsageAttribute(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
	public class XpAuthorizeAttribute : AuthorizeAttribute
	{
		public string ClientIds { get; set; }

		#region HandleUnauthorizedRequest
		protected override void HandleUnauthorizedRequest(HttpActionContext actionContext)
		{
			base.HandleUnauthorizedRequest(actionContext);
		}
		#endregion HandleUnauthorizedRequest

		#region OnAuthorization
		public override void OnAuthorization(HttpActionContext actionContext)
		{
			try
			{
				base.OnAuthorization(actionContext);
			}
			catch (Exception ex)
			{
				throw ex;
			}

			if (!ClientIds.IsNullOrEmpty())
			{
				var MethodClientIds = ClientIds.SplitString(",");

				var claims = (Thread.CurrentPrincipal as ClaimsPrincipal).Claims;
				var userClientIds = claims
					.Where(x => x.Type == XpClaimTypes.ClientId)
					.Select(x => x.Value);

				if (!userClientIds.Intersect(MethodClientIds).Any())
					throw new UnauthorizedAccessException(AccountResources.ClientIdNotAllowedException.FormatString(userClientIds.JoinString(",")));
			}

			//Thread.SetData(Thread.GetNamedDataSlot("XpUri"), actionContext.Request.RequestUri.AbsoluteUri);
			//Thread.SetData(Thread.GetNamedDataSlot("XpToken"), actionContext.Request.Headers.Authorization.Parameter);

			var identity = Thread.CurrentPrincipal.Identity as ClaimsIdentity;
			identity.AddClaim(new Claim(XpClaimTypes.Uri, actionContext.Request.RequestUri.AbsoluteUri));
			identity.AddClaim(new Claim(XpClaimTypes.Token, actionContext.Request.Headers.Authorization.Parameter));

			//var b = myMembership.Instance.Member().IsLoggedIn;
			////Is user logged in?
			//if ( b )
			//		//If user is logged in and we need a custom check:
			//		if ( ResourceKey != null && OperationKey != null )
			//				return ecMembership.Instance.Member().ActivePermissions.Where( x => x.operation == OperationKey && x.resource == ResourceKey ).Count() > 0;
			////Returns true or false, meaning allow or deny. False will call HandleUnauthorizedRequest above
			//return b;
		}
		#endregion OnAuthorization
	}
}
