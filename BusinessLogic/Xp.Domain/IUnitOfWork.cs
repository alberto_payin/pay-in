﻿using System;
using System.Threading.Tasks;

namespace Xp.Domain
{
	public interface IUnitOfWork : IDisposable
	{
		Task SaveAsync();
	}
}
