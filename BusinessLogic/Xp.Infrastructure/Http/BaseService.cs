﻿using PayIn.BusinessLogic.Common;
using System;
using System.Threading.Tasks;

namespace Xp.Infrastructure.Http
{
	public abstract class BaseService
	{
		public abstract string BaseAddress { get; }
		public abstract ISessionData SessionData { get; set; }

		#region Server
		private Server _Server = null;
		protected Server Server
		{
			get
			{
				if (_Server == null)
					_Server = new Server(BaseAddress, SessionData.Token);
				return _Server;
			}
		}
		#endregion Server
	}
}
