﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xp.Infrastructure
{
	public interface IBlobRepository
	{
		void SaveFile(string name, byte[] content);
	}
}
