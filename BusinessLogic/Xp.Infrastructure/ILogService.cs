﻿using System;

namespace Xp.Infrastructure
{
	public interface ILogService
	{
		void TrackException(Exception exception);
	}
}
