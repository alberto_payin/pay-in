﻿using System.Net;
using System.Net.Mail;
using System.Net.Mime;

namespace Xp.Infrastructure.Mail
{
	public class MailService : IMailService
	{
		public void Send(string mail, string subject, string body)
		{
			var message = new MailMessage("system@pay-in.es", mail);
			message.Subject = subject;
			message.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(body, null, MediaTypeNames.Text.Plain));
			message.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(body, null, MediaTypeNames.Text.Html));

			var client = new SmtpClient("smtp.1and1.com", 587)
			{
				DeliveryMethod = SmtpDeliveryMethod.Network,
				Credentials = new NetworkCredential("system@pay-in.es", "Payin2014!")
			};

			client.Send(message);          
		}
	}
}
