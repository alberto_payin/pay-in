﻿using Microsoft.Azure;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;

namespace Xp.Infrastructure.Repositories
{
	public class AzureBlobRepository : IBlobRepository
	{
		#region SaveFile
		public void SaveFile(string name, byte[] content)
		{
			var storageAccount = CloudStorageAccount.Parse(
				CloudConfigurationManager.GetSetting("Files"));

			// Create the blob client.
			var blobClient = storageAccount.CreateCloudBlobClient();

			// Retrieve reference to a previously created container.
			var container = blobClient.GetContainerReference("files");
			var blockBlob = container.GetBlockBlobReference(name);

			if (blockBlob.Exists() && content!=null)
			{
				DeleteFile(name);
			}
			blockBlob.UploadFromByteArray(content, 0, content.Length);
		}
		#endregion SaveFile

		#region SaveImage
		public void SaveImage(string name, byte[] content)
		{
			var storageAccount = CloudStorageAccount.Parse(
				CloudConfigurationManager.GetSetting("Files"));

			// Create the blob client.
			var blobClient = storageAccount.CreateCloudBlobClient();
			// Retrieve reference to a previously created container.
			var container = blobClient.GetContainerReference("files");
			var blockBlob = container.GetBlockBlobReference(name);	
			
			if (blockBlob.Exists() && content!=null)
			{
				DeleteFile(name);
			}
			try
			{
				var text = System.Text.Encoding.UTF8.GetString(content).Split(',')[1];	
				byte[] bytes = Convert.FromBase64String(text);
				
				//Create an Image from Base64 string				
				MemoryStream ms = new MemoryStream(bytes, 0, bytes.Length);
				//Convert byte[] to Image	
				ms.Write(bytes, 0, bytes.Length);
				Image image = Image.FromStream(ms, true);				
				ms.Dispose();
				//Convert to jpg Image
				image.Save(ms,System.Drawing.Imaging.ImageFormat.Jpeg);				
				//Return to byte[]
				byte[] imageBytes = ms.ToArray();				
					
				blockBlob.UploadFromByteArrayAsync(imageBytes, 0, imageBytes.Length);					
				ms.Close();
				
			}
			catch (System.ArgumentNullException)
			{
				throw new ArgumentNullException("Binary byte[] of photo is null.");
			}
			catch (System.Exception exc)
			{
				throw new Exception(exc.Message, exc.InnerException);
			}
		}
		#endregion SaveImage

		#region DeleteFile
		//Método para eliminar recurso de Azure
		public void DeleteFile(string name)
		{
			try
			{
				//Retrieve storage account from connection string
				var storageAccount = CloudStorageAccount.Parse(
					CloudConfigurationManager.GetSetting("Files"));

				//Create the blob client
				var blobClient = storageAccount.CreateCloudBlobClient();

				//Retrieve reference to a previously created container
				var container = blobClient.GetContainerReference("files");

				var storageUrl = container.Uri.ToString();
				var lengthShortUrl = storageUrl.Length + 1;
				var shortUrl = "";

					if (name.Contains(storageUrl)) 
					{
						shortUrl = name.Substring(lengthShortUrl);
					}
				//Retrieve reference to a blob named "myblob.txt"
				var blockBlob = container.GetBlockBlobReference(shortUrl);

				//Delete the blob
				blockBlob.Delete();
			}
			catch (System.Exception exc)
			{
				throw new Exception(exc.Message, exc.InnerException);
			}
		#endregion DeleteFile
		}
	}
}
