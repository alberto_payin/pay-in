﻿using PayIn.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xp.Infrastructure.Repositories
{
	public interface IPushSpecificRepository
	{
		DeviceType Type { get; }

		Task<string> SendNotification(string pushId, string pushCertificate, IEnumerable<string> targetIds, NotificationType type, NotificationState state, string message, string relatedName, string relatedId, int notificationId, int sourceId, string sourceNombre);
	}
}
