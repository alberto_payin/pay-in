﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xp.Domain;
using PayIn.Domain.Public;
using PayIn.BusinessLogic.Common;
using System.Threading.Tasks;
using Xp.Infrastructure.Repositories;
using PayIn.Common;

namespace Xp.Infrastructure.Repositories
{
	public class PushRepository : IPushRepository
	{
		private readonly IEntityRepository<Device> DeviceRepository;
		public readonly List<IPushSpecificRepository> PushRepositories;

		#region Constructors
		public PushRepository(
			IEntityRepository<Device> deviceRepository,
			PushAndroidRepository pushAndroidRepository
		)
		{
			if (deviceRepository == null) throw new ArgumentNullException("deviceRepository");
			DeviceRepository = deviceRepository;

			PushRepositories = new List<IPushSpecificRepository>
			{
				pushAndroidRepository
			};
		}
		#endregion Constructors

		#region SendNotification
		public async Task<string> SendNotification(IEnumerable<string> targetIds, NotificationType type, NotificationState state, string message, string relatedName, string relatedId, int notificationId)
		{
			if ((targetIds != null) && (targetIds.Count() == 0))
				throw new ArgumentNullException("targetIds"); ;

			var devices = (await DeviceRepository.GetAsync("Platform"))
				.Where(x => targetIds.Contains(x.Login))
				.GroupBy(x => x.Platform, (x, g) => new
				{
					Type = x.Type,
					PushId = x.PushId,
					PushCertificate = x.PushCertificate,
					Devices = g.Select(y => y.Token)
				});
			if (devices == null)
				throw new ArgumentNullException("targetIds");

			foreach (var item in devices)
			{
				if (item.Devices.Count() == 0)
					continue;

				foreach (var repository in PushRepositories.Where(x => x.Type == item.Type))
					await repository.SendNotification(
						item.PushId,
						item.PushCertificate,
						item.Devices,
						type,
						state,
						message,
						relatedName,
						relatedId,
						notificationId,
						0,
						null);
			}
			return "";
		}
		#endregion SendNotification
	}
}
