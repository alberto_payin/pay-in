﻿using Microsoft.ApplicationInsights;
using System;

namespace Xp.Infrastructure.Services
{
	public class ApplicationInsightsLogService : ILogService
	{
		#region TrackException
		public void TrackException(Exception exception)
		{
			var telemetry = new TelemetryClient();
			telemetry.TrackException(exception);
		}
		#endregion TrackException
	}
}
