﻿using Microsoft.ApplicationInsights;
using Mixpanel.NET.Events;
using PayIn.BusinessLogic.Common;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xp.Infrastructure.Http;

namespace Xp.Infrastructure.Services
{
	public class MixPanelService : IAnalyticsService
	{
		private readonly string ApiKey = "873691d1e07a2d6e4dd6cd61ac4bc3d7";
		private SessionData SessionData { get; set; }

		#region Constructors
		public MixPanelService(
			SessionData sessionData
		)
		{
			SessionData = sessionData;
		}
		#endregion Constructors

		#region TrackEvent
		public Task TrackEventAsync(string name, Dictionary<string, object> parameters = null)
		{
			return Task.Run(() =>
			{
				try
				{
					var param = parameters ?? new Dictionary<string, object>();
					if (SessionData != null)
						if (!param.ContainsKey("distinct_id"))
							param.Add("distinct_id", SessionData.Login);

					var tracker = new MixpanelTracker(ApiKey);
					tracker.Track(
						name,
						param
					);
				}
				catch (Exception ex)
				{
				}
			});
		}
		#endregion TrackEvent
	}
}
