@ECHO OFF

echo BORRANDO...

echo -----------------------------------------------------------------
echo BusinessLogic
echo -----------------------------------------------------------------

echo BusinessLogic\PayIn.Application.Dto
rmdir /S /Q BusinessLogic\PayIn.Application.Dto\obj
rmdir /S /Q BusinessLogic\PayIn.Application.Dto\bin

echo BusinessLogic\PayIn.Application.Dto.Security
rmdir /S /Q BusinessLogic\PayIn.Application.Dto.Security\obj
rmdir /S /Q BusinessLogic\PayIn.Application.Dto.Security\bin

echo BusinessLogic\PayIn.Application.Internal
rmdir /S /Q BusinessLogic\PayIn.Application.Internal\obj
rmdir /S /Q BusinessLogic\PayIn.Application.Internal\bin

echo BusinessLogic\PayIn.Application.Public
rmdir /S /Q BusinessLogic\PayIn.Application.Public\obj
rmdir /S /Q BusinessLogic\PayIn.Application.Public\bin

echo PayIn.BusinessLogic.Common
rmdir /S /Q BusinessLogic\PayIn.BusinessLogic.Common\obj
rmdir /S /Q BusinessLogic\PayIn.BusinessLogic.Common\bin

echo BusinessLogic\PayIn.Common.DI.Internal
rmdir /S /Q BusinessLogic\PayIn.Common.DI.Internal\obj
rmdir /S /Q BusinessLogic\PayIn.Common.DI.Internal\bin

echo BusinessLogic\PayIn.Common.DI.Public
rmdir /S /Q BusinessLogic\PayIn.Common.DI.Public\obj
rmdir /S /Q BusinessLogic\PayIn.Common.DI.Public\bin

echo BusinessLogic\PayIn.DistributedServices
rmdir /S /Q BusinessLogic\PayIn.DistributedServices\obj
rmdir /S /Q BusinessLogic\PayIn.DistributedServices\bin

echo BusinessLogic\PayIn.DistributedServices.Security
rmdir /S /Q BusinessLogic\PayIn.DistributedServices.Security\obj
rmdir /S /Q BusinessLogic\PayIn.DistributedServices.Security\bin

echo BusinessLogic\PayIn.Domain.Internal
rmdir /S /Q BusinessLogic\PayIn.Domain.Internal\obj
rmdir /S /Q BusinessLogic\PayIn.Domain.Internal\bin

echo BusinessLogic\PayIn.Domain.Public
rmdir /S /Q BusinessLogic\PayIn.Domain.Public\obj
rmdir /S /Q BusinessLogic\PayIn.Domain.Public\bin

echo BusinessLogic\PayIn.Domain.Security
rmdir /S /Q BusinessLogic\PayIn.Domain.Security\obj
rmdir /S /Q BusinessLogic\PayIn.Domain.Security\bin

echo BusinessLogic\PayIn.Infrastructure.Internal
rmdir /S /Q BusinessLogic\PayIn.Infrastructure.Internal\obj
rmdir /S /Q BusinessLogic\PayIn.Infrastructure.Internal\bin

echo BusinessLogic\PayIn.Infrastructure.Internal.Db
rmdir /S /Q BusinessLogic\PayIn.Infrastructure.Internal.Db\obj
rmdir /S /Q BusinessLogic\PayIn.Infrastructure.Internal.Db\bin

echo BusinessLogic\PayIn.Infrastructure.Public
rmdir /S /Q BusinessLogic\PayIn.Infrastructure.Public\obj
rmdir /S /Q BusinessLogic\PayIn.Infrastructure.Public\bin

echo BusinessLogic\PayIn.Infrastructure.Public.Db
rmdir /S /Q BusinessLogic\PayIn.Infrastructure.Public.Db\obj
rmdir /S /Q BusinessLogic\PayIn.Infrastructure.Public.Db\bin

echo BusinessLogic\PayIn.Infrastructure.Security
rmdir /S /Q BusinessLogic\PayIn.Infrastructure.Security\obj
rmdir /S /Q BusinessLogic\PayIn.Infrastructure.Security\bin

echo BusinessLogic\PayIn.Infrastructure.Security.Db
rmdir /S /Q BusinessLogic\PayIn.Infrastructure.Security.Db\obj
rmdir /S /Q BusinessLogic\PayIn.Infrastructure.Security.Db\bin

echo BusinessLogic\Xp.Application
rmdir /S /Q BusinessLogic\Xp.Application\obj
rmdir /S /Q BusinessLogic\Xp.Application\bin

echo BusinessLogic\Xp.DistributedServices
rmdir /S /Q BusinessLogic\Xp.DistributedServices\obj
rmdir /S /Q BusinessLogic\Xp.DistributedServices\bin

echo BusinessLogic\Xp.Domain
rmdir /S /Q BusinessLogic\Xp.Domain\obj
rmdir /S /Q BusinessLogic\Xp.Domain\bin

echo BusinessLogic\Xp.Infrastructure
rmdir /S /Q BusinessLogic\Xp.Infrastructure\obj
rmdir /S /Q BusinessLogic\Xp.Infrastructure\bin

echo -----------------------------------------------------------------
echo Common
echo -----------------------------------------------------------------

echo Common\PayIn.Application.Dto
rmdir /S /Q Common\PayIn.Application.Dto\obj
rmdir /S /Q Common\PayIn.Application.Dto\bin

echo Common\PayIn.Common
rmdir /S /Q Common\PayIn.Common\obj
rmdir /S /Q Common\PayIn.Common\bin

echo Common\PayIn.Common.Resources
rmdir /S /Q Common\PayIn.Common.Resources\obj
rmdir /S /Q Common\PayIn.Common.Resources\bin


echo Common\PayIn.Common.Security
rmdir /S /Q Common\PayIn.Common.Security\obj
rmdir /S /Q Common\PayIn.Common.Security\bin

echo Common\Xp.Common
rmdir /S /Q Common\Xp.Common\obj
rmdir /S /Q Common\Xp.Common\bin

echo Common\Xp.Common.Net
rmdir /S /Q Common\Xp.Common.Net\obj
rmdir /S /Q Common\Xp.Common.Net\bin

echo Common\Xp.Common.Resources
rmdir /S /Q Common\Xp.Common.Resources\obj
rmdir /S /Q Common\Xp.Common.Resources\bin

echo -----------------------------------------------------------------
echo Deployment
echo -----------------------------------------------------------------

echo Deployment\PayIn.Deployment.Azure
rmdir /S /Q Deployment\PayIn.Deployment.Azure\obj
rmdir /S /Q Deployment\PayIn.Deployment.Azure\bin
rmdir /S /Q Deployment\PayIn.Deployment.Azure\csx

echo Deployment\PayIn.Internal
rmdir /S /Q Deployment\PayIn.Internal\obj
rmdir /S /Q Deployment\PayIn.Internal\bin

echo -----------------------------------------------------------------
echo Presentation
echo -----------------------------------------------------------------

echo Presentation\PayIn.Presentation.Common
rmdir /S /Q Presentation\PayIn.Presentation.Common\obj
rmdir /S /Q Presentation\PayIn.Presentation.Common\bin

echo Presentation\PayIn.Presentation.Shared

echo Presentation\PayIn.Web
rmdir /S /Q Presentation\PayIn.Web\obj
rmdir /S /Q Presentation\PayIn.Web\bin

echo Presentation\PayIn.Web.Internal
rmdir /S /Q Presentation\PayIn.Web.Internal\obj
rmdir /S /Q Presentation\PayIn.Web.Internal\bin

echo Presentation\Xp.Presentation.Common
rmdir /S /Q Presentation\Xp.Presentation.Common\obj
rmdir /S /Q Presentation\Xp.Presentation.Common\bin

pause