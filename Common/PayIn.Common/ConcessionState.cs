﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayIn.Common
{
	public enum ConcessionState
	{
		Active = 0,
		Removed = 1,
		Suspended = 2,
		Pending = 3
	}
}
