﻿using System;
using System.Reflection;

namespace System
{
	public static class EnumExtension
	{
		#region ToEnumAlias
		public static string ToEnumAlias(this Enum enumerator)
		{
			var resources = new PayIn.Common.Resources.Resources();
			var value = resources.Enum.GetPropertyValue<string>(
				enumerator.GetType().Name +
				"_" +
				enumerator.ToString()
			);

			if (!value.IsNullOrEmpty())
				return value;

			return enumerator.ToString();
		}
		#endregion ToEnumAlias
	}
}
