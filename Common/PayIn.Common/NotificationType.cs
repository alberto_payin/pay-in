﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using Xp.Common;

namespace PayIn.Common
{
	public enum NotificationType
	{
		ConcessionVinculation			    = 1,
		ConcessionVinculationAccepted       = 2,
		PaymentSucceed                      = 3,
		PaymentError                        = 4,
		RefundSucceed                       = 5,
		RefundError						    = 6,
		PaymentMediaCreateSucceed		    = 7,
		PaymentMediaCreateError			    = 8,
		ConcessionVinculationRefused	    = 9,
		ConcessionDissociation			    = 10,
		PaymentWorkerConcessionDissociation = 11
	}
}
