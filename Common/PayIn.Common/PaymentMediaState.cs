﻿using System;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;
using Xp.Common;

namespace PayIn.Common
{
	public enum PaymentMediaState
	{
		Pending = 1,
		Active = 2,
		Error = 3,
		Delete = 4,
		Expired = 5
	}
}
