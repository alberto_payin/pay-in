﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayIn.Common
{
	public enum PaymentState
	{
		Active = 1,
		Error = 2,
		Pending = 3,
		Returned = 4 // Calculado: Simplemente vale como valor para devolver en consultas
	}
}
