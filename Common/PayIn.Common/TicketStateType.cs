﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayIn.Common
{
	public enum TicketStateType
	{
		Active        = 1,
		Cancelled     = 2
	}
}
