﻿namespace System
{
	public static class DateTimeExtension
	{
		#region ToUTC
		public static DateTime ToUTC(this DateTime that)
		{
			if (that.Kind == DateTimeKind.Unspecified)
				return new DateTime(that.Ticks, DateTimeKind.Utc);
			else
				return that.ToUniversalTime();
		}
		public static DateTime? ToUTC(this DateTime? that)
		{
			return that != null ?
				that.Value.ToUTC() :
				(DateTime?)null;
		}
		#endregion ToUTC

		#region ToLTC
		public static DateTime ToLTC(this DateTime that)
		{
			if (that.Kind == DateTimeKind.Unspecified)
				return new DateTime(that.Ticks, DateTimeKind.Local);
			else
				return that.ToLocalTime();
		}
		public static DateTime? ToLTC(this DateTime? that)
		{
			return that != null ?
				that.Value.ToLTC() :
				(DateTime?)null;
		}
		#endregion ToLTC

		#region ToLocalTime
		public static DateTime? ToLocalTime(this DateTime? that)
		{
			if (that == null)
				return null;
			return that.Value.ToLocalTime();
		}
		#endregion ToLocalTime
	}
}
