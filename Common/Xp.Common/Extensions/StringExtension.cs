﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.IO;
using System.Linq;
using System.Xml;

namespace System
{
	public static class StringExtension
	{
		#region ToCamel
		public static string ToCamel(this string that)
		{
			var camel = that.ToPascal();

			return camel.Substring(0, 1).ToLower() + camel.Substring(1);
		}
		#endregion ToCamel

		#region ToPascal
		public static string ToPascal(this string that)
		{
			var parts = that.Split(',');
			return string.Join("", parts
				.Select(x => x.Substring(0, 1).ToUpper() + x.Substring(1))
			);
		}
		#endregion ToPascal

		#region FormatString
		public static string FormatString(this string that, params object[] parameters)
		{
			return string.Format(that, parameters);
		}
		#endregion FormatString

		#region IsNullOrEmpty
		public static bool IsNullOrEmpty(this string that)
		{
			return string.IsNullOrEmpty(that);
		}
		#endregion IsNullOrEmpty

		#region SplitString
		public static string[] SplitString(this string that, string separator, StringSplitOptions options = StringSplitOptions.None)
		{
			return that.Split(new[] { separator }, options);
		}
		public static string[] SplitString(this string that, string separator, int count, StringSplitOptions options = StringSplitOptions.None)
		{
			return that.Split(new[] { separator }, count, options);
		}
		#endregion SplitString

		#region FromJson
		public static T FromJson<T>(this string that)
		{
			var result = JsonConvert.DeserializeObject<T>(that, new JsonSerializerSettings
			{
				ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver(),
				NullValueHandling = NullValueHandling.Ignore,
				Formatting = Formatting.None
			});
			return result;
		}
		public static dynamic FromJson(this string that)
		{
			var result = JsonConvert.DeserializeObject(that, new JsonSerializerSettings
			{
				ContractResolver = new Newtonsoft.Json.Serialization. CamelCasePropertyNamesContractResolver(),
				NullValueHandling = NullValueHandling.Ignore,
				Formatting = Formatting.None,
			});
			return result;
		}
		#endregion FromJson
	}
}