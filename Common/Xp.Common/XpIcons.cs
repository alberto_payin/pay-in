﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xp.Common
{
	public static class XpIcons
	{
		public static string ArrowRight { get { return "glyphicon glyphicon-arrow-right"; } }
		public static string Back       { get { return "fa fa-reply";                     } }	
		public static string Camera     { get { return "glyphicon glyphicon-camera";      } }
		public static string Calendar   { get { return "glyphicon glyphicon-calendar";    } }
		public static string Check	    { get { return "glyphicon glyphicon-check";		  } }
		public static string Copy       { get { return "fa fa-copy";                      } }
		public static string Day		{ get { return "fa fa-list-alt";				  } }
		public static string Deleted    { get { return "fa fa-ban";                       } }
		public static string Download   { get { return "glyphicon glyphicon-download";	  } }
		public static string Pay		{ get { return "fa fa-credit-card";				  } }
		public static string Graph      { get { return "icon-graph";                      } }
		public static string Lock		{ get { return "fa fa-lock";					  } }
		public static string List       { get { return "fa fa-th-list";                   } }
		public static string ListAlt    { get { return "glyphicon glyphicon-list-alt";    } }
		public static string ListView   { get { return "fa fa-bars";					  } }
		public static string Map        { get { return "icon-map";                        } }
		public static string MapMarker  { get { return "glyphicon glyphicon-map-marker";  } }
		public static string Money		{ get { return "fa fa-money";					  } }
		public static string Month      { get { return "icon-calendar";		              } }
		public static string Ok         { get { return "glyphicon glyphicon-ok";          } }
		public static string Pencil     { get { return "glyphicon glyphicon-pencil";      } }
		public static string Plus       { get { return "glyphicon glyphicon-plus";        } }
		public static string Question   { get { return "fa fa-question";                  } }
		public static string Remove     { get { return "glyphicon glyphicon-remove";      } }
		public static string Resend     { get { return "fa fa-reply-all";					  } }
		public static string Round      { get { return "icon-loop";     				  } }
		public static string Rss        { get { return "fa fa-rss";                       } }
		public static string Save       { get { return "glyphicon glyphicon-save";        } }
		public static string Search     { get { return "glyphicon glyphicon-search";      } }
		public static string Time       { get { return "glyphicon glyphicon-time";        } }
		public static string Today      { get { return "icon-target";                     } }
		public static string Trash      { get { return "glyphicon glyphicon-trash";       } }			
		public static string Unlock		{ get { return "fa fa-unlock";					  } }
		public static string Unpay		{ get { return "fa fa-rotate-left ";			  } }
		public static string Week		{ get { return "fa fa-ellipsis-h";				  } }
	}
}
