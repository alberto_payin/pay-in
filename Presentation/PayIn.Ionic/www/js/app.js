angular.module('app', ['ionic', 'ngCordova','xp'])
.run(function($ionicPlatform, $rootScope) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }

  });
  $rootScope.app =
  {
    apiBaseUrl: 'http://localhost:8080/', // La baseURL es la raíz
    clientId: 'PayInAndroidNativeApp',
    clientSecret: 'PayInAndroidNativeApp@123456'
  };
})
.config(function($stateProvider, $urlRouterProvider) {
  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    .state('paymentmediagetall', { url: '/PaymentMedia', templateUrl: 'templates/paymentMedia/getAll.html'})
    //.state('accountlogin', { url: '/Account/Login', templateUrl: 'templates/account/login.html'})
    .state('accountlogin', { url: '/Account/Login', templateUrl: 'templates/ticket/getAll.html'})
    .state('paymentmediagetTickets',{url: '/PaymentMedia/Tickets', templateUrl:'templates/ticket/getAll.html'})
    .state('accountregister' ,{url:'/Account/Register', templateUrl:'templates/account/register.html'})
    .state('policy' ,{url:'/Policy', templateUrl:'templates/policy/policy.html'})
    .state('accountprofile' ,{url:'/Account/Profile', templateUrl:'templates/account/profile.html'})
    .state('camera',{url:'/Camera/Picture', templateUrl:'templates/camera/getPicture.html'})
    .state('map',{url:'/Map', templateUrl:'templates/map/map.html'})
    // .state('main',                    { url: '/main',               abstract: true,                  templateUrl: 'templates/main.html' })
    // .state('main.paymentgetall',      { url: '/paymentgetall',      views: { 'paymentgetall':      { templateUrl: 'templates/paymentgetall.html',      controller: 'DashCtrl'      }}})
    // .state('main.chats',              { url: '/chats',              views: { 'tab-chats':          { templateUrl: 'templates/tab-chats.html',          controller: 'ChatsCtrl'     }}})
    // .state('main.chat-detail',        { url: '/chats/:chatId',      views: { 'tab-chats':          { templateUrl: 'templates/chat-detail.html',        controller: 'ChatDetailCtrl'}}});

  $urlRouterProvider.otherwise('/PaymentMedia/Tickets');
})
.controller('LoadingCtrl', function($scope, $ionicLoading) {
  $scope.show = function() {
    $ionicLoading.show({
      template: 'Loading...'
    })
  };
  $scope.hide = function(){
    $ionicLoading.hide();
  }
})
.factory('cordovaPlugin', ['$q', function ($q) {

        return {

            showToast: function(str) {
                var q = $q.defer();
                sEExample.showToast(str);       
                return q.promise;
            }
        };

    }]);
;
