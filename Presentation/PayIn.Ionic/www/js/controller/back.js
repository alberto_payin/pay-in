/* global angular */
angular.module('app')
.controller('back', function($scope,$ionicHistory) {
    $scope.goBack = function() {
      $ionicHistory.goBack();
  };
});