/* global Camera */
angular.module('app')
.controller('camera', function($scope) {
    $scope.buttonTitle = "Take Picture";
    $scope.takePicture = function(){   
        var cameraOptions = {
            quality: 50,
            destinationType: Camera.DestinationType.DATA_URL                
         };
        var success = function(data){
        $scope.$apply(function () {
             /*
               remember to set the image ng-src in $apply,
               i tried to set it from outside and it doesn't work.
             */
               $scope.cameraPic = "data:image/jpeg;base64," + data;
             });
         };
        var failure = function(message){
             alert('Failed because: ' + message);
        };
        //call the cordova camera plugin to open the device's camera
        navigator.camera.getPicture( success , failure , cameraOptions );            
    };
});