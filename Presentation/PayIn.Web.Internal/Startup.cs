﻿using Microsoft.Owin;
using Owin;
using System.Web.Http;

[assembly: OwinStartup(typeof(PayIn.Web.Internal.Startup))]
namespace PayIn.Web.Internal
{
	public partial class Startup
	{
		public void Configuration(IAppBuilder app)
		{
			var config = new HttpConfiguration();

			app
				.UseSecurity(config)
				.UseWebApi("internal", config)
				.UseWebApi(config)
			;
		}
	}
}
