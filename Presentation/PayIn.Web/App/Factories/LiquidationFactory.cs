﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PayIn.Web.App.Factories
{
	public partial class LiquidationFactory
	{
		public static string UrlApi { get { return "/Api/Liquidation"; } }
		public static string Url { get { return "/Liquidation"; } }

		#region GetAll
		public static string GetAllName { get { return "liquidationgetall"; } }
		public static string GetAllApi { get { return UrlApi; } }
		#endregion GetAll

		#region LiquidationPay
		public static string LiquidationPayName { get { return "liquidationpay"; } }
		public static string LiquidationPayApi { get { return UrlApi + "/Liquidations"; } }
		#endregion LiquidationPay

		#region LiquidationChange
		public static string LiquidationChangeName { get { return "liquidationchange"; } }
		public static string LiquidationChangeApi { get { return UrlApi + "/Change"; } }
		#endregion LiquidationChange

		#region LiquidationOpen
		public static string LiquidationOpenName { get { return "liquidationopen"; } }
		public static string LiquidationOpenApi { get { return UrlApi + "/Open"; } }
		#endregion LiquidationOpen

		#region LiquidationConfirmPay
		public static string LiquidationConfirmPayName { get { return "liquidationconfirmpay"; } }
		public static string LiquidationConfirmPayApi { get { return UrlApi + "/ConfirmPay"; } }
		#endregion LiquidationConfirmPay

		#region LiquidationConfirmUnpay
		public static string LiquidationConfirmUnpayName { get { return "liquidationconfirmunpay"; } }
		public static string LiquidationConfirmUnpayApi { get { return UrlApi + "/ConfirmUnpay"; } }
		#endregion LiquidationConfirmUnpay
	}
}