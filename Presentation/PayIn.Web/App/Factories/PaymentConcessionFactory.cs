﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PayIn.Web.App.Factories
{
	public partial class PaymentConcessionFactory
	{
		public static string UrlApi { get { return "/Api/PaymentConcession"; } }
		public static string Url { get { return "/PaymentConcession"; } }

		//#region GetAll
		//public static string GetAllName { get { return "paymentconcessiongetall"; } }
		//public static string GetAllApi { get { return UrlApi; } }
		//#endregion GetAll

		#region Get
		public static string GetSupplierApi { get { return "/Api/ServiceSupplier/Current"; } }
		#endregion Get

		#region Create
		public static string CreatePaymentConcessionName { get { return "paymentconcessioncreate"; } }
		public static string CreatePaymentConcessionApi { get { return UrlApi + "/Create"; } }
		public static string CreatePaymentConcessionCommerceName { get { return "paymentconcessioncommercecreate";} }
		#endregion Create

		#region Update
		public static string UpdateName { get { return "paymentconcessionupdate"; } }
		public static string UpdateApi { get { return UrlApi; } }
		#endregion Update	

		#region UpdateCommerce
		public static string UpdateCommerce { get { return "paymentconcessionupdatecommerce"; } }
		public static string UpdateCommerceApi { get { return UrlApi + "/UpdateCommerce"; } }
		#endregion UpdateCommerce

		#region GetCommerce
		public static string GetCommerceApi { get { return UrlApi + "/GetCommerce"; } }
		#endregion GetCommerce

	}
}