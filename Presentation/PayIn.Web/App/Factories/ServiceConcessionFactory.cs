﻿
namespace PayIn.Web.App.Factories
{
	public class ServiceConcessionFactory
	{
		public static string UrlApi { get { return "/Api/ServiceConcession"; } }
		public static string Url { get { return "/ServiceConcession"; } }

		#region GetAll
		public static string GetAllName { get { return "serviceconcessiongetall"; } }
		public static string GetAllApi { get { return UrlApi; } }
		#endregion GetAll

		//#region Create
		//public static string CreateName { get { return "ServiceConcessionCreate"; } }
		//public static string CreateApi { get { return UrlApi; } }
		//public static string Create { get { return Url + "/Create"; } }
		//public static string CreateUri { get { return "#" + Create; } }
		//public static string CreateTemplate { get { return Create; } }
		//#endregion Create

		//#region RetrieveAll
		//public static string RetrieveAllName { get { return "ServiceConcessionRetrieveAll"; } }
		//public static string RetrieveAllApi { get { return UrlApi; } }
		//public static string RetrieveAll { get { return Url; } }
		//public static string RetrieveAllUri { get { return "#" + RetrieveAll; } }
		//public static string RetrieveAllTemplate { get { return RetrieveAll; } }
		//#endregion RetrieveAll

		#region RetrieveSelector
		public static string RetrieveSelectorApi { get { return UrlApi + "/RetrieveSelector"; } }
		#endregion RetrieveSelector

		#region Update
		public static string UpdateName { get { return "serviceconcessionupdate"; } }
		public static string UpdateApi { get { return UrlApi; } }
		#endregion Update

		#region UpdateCommerce
		public static string UpdateCommerce { get { return "serviceconcessionupdatecommerce"; } }
		public static string UpdateCommerceApi { get { return UrlApi +"/UpdateCommerce"; } }
		#endregion UpdateCommerce

		#region GetCommerce
		public static string GetCommerce { get { return "serviceconcessiongetcommerce"; } }
		public static string GetCommerceApi { get { return UrlApi; } }
		#endregion GetCommerce

		#region Delete
		public static string Delete { get { return "serviceconcessiondelete"; } }
		public static string DeleteApi { get { return UrlApi; } }
		#endregion Delete

	}
}
