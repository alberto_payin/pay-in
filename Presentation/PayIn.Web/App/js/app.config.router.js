'use strict';

/**
 * Config for the router
 */
angular.module('app')
.run(
  ['$rootScope', '$state', '$stateParams',
    function ($rootScope, $state, $stateParams) {
      $rootScope.$state = $state;
      $rootScope.$stateParams = $stateParams;
    }
  ]
)
.config(
	['$stateProvider', '$urlRouterProvider',
		function ($stateProvider, $urlRouterProvider) {
			$urlRouterProvider
				.otherwise('');

			$stateProvider
				.state('app1',                            { url: '',                                              templateUrl: '/Home/Deals'                             })
				.state('app2',                            { url: '/',                                             templateUrl: '/Home/Deals'                             })
				//Account
				.state('accountupdate',                   { url: '/Account/Update',                               templateUrl: 'Account/Update'                         })
				.state('accountimagecrop',                { url: '/Account/ImageCrop',                            templateUrl: 'Account/ImageCrop'                      })
				.state('accountcurrent',                  { url: '/Account/Current',                              templateUrl: 'Home/Deals'                             })
				.state('accountdeleteimage',              { url: '/Account/DeleteImage',                          templateUrl: 'Account/DeleteImage'                    })
			
				// ControlForm
				.state('controlformargumentslist',        { url: '/ControlForm/Arguments/:id',                    templateUrl: 'ControlForm/Arguments'                  })
				.state('controlformcreate',               { url: '/ControlForm/Create',                           templateUrl: 'ControlForm/Create'                     })
				.state('controlformdelete',               { url: '/ControlForm/Delete/{id}',                      templateUrl: 'ControlForm/Delete'                     })
				.state('controlformgetall',               { url: '/ControlForm',                                  templateUrl: 'ControlForm'                            })
				.state('controlformupdate',               { url: '/ControlForm/Update/{id}',                      templateUrl: 'ControlForm/Update'                     })
				// ControlFormArgument
				.state('controlformargumentgetform',      { url: '/ControlFormArgument/Form/{formId}',            templateUrl: 'ControlFormArgument/Form'               })
				.state('controlformargumentcreate',       { url: '/ControlFormArgument/Create/{formId}',          templateUrl: 'ControlFormArgument/Create'             })
				.state('controlformargumentupdate',       { url: '/ControlFormArgument/Update/{id}',              templateUrl: 'ControlFormArgument/Update'             })
				.state('controlformargumentdelete',       { url: '/ControlFormArgument/Delete/{id}',              templateUrl: 'ControlFormArgument/Delete'             })
				// ControlFormAssign
				.state('controlformassigncreate',         { url: '/ControlFormAssign/Create/{checkId}',           templateUrl: 'ControlFormAssign/Create'               })
				.state('controlformassignupdate',         { url: '/ControlFormAssign/Update/{id}',                templateUrl: 'ControlFormAssign/Update'               })
				.state('controlformassigndelete',         { url: '/ControlFormAssign/Delete/{id}',                templateUrl: 'ControlFormAssign/Delete'               })
				.state('controlformassigngetcheck',       { url: '/ControlFormAssign/Check/{checkId}',            templateUrl: 'ControlFormAssign/Check'                })
				.state('controlformassignform',           { url: '/ControlFormAssign/ViewCompleteForm/{checkId}', templateUrl: 'ControlFormAssign/ViewCompleteForm'     })
				// ControlFormAssignTemplate
				.state('controlformassigntemplatecreate', { url: '/ControlFormAssignTemplate/Create/{checkId}', templateUrl: 'ControlFormAssignTemplate/Create'         })
				.state('controlformassigntemplateupdate', { url: '/ControlFormAssignTemplate/Update/{id}', templateUrl: 'ControlFormAssignTemplate/Update'              })
				.state('controlformassigntemplatedelete', { url: '/ControlFormAssignTemplate/Delete/{id}', templateUrl: 'ControlFormAssignTemplate/Delete'              })
				.state('controlformassigntemplategetcheck', { url: '/ControlFormAssignTemplate/Check/{checkId}', templateUrl: 'ControlFormAssignTemplate/Check'         })
				// ControlFormAssignCheckPoint
				.state('controlformassigncheckpointcreate', { url: '/ControlFormAssignCheckPoint/Create/{checkId}', templateUrl: 'ControlFormAssignCheckPoint/Create'   })
				.state('controlformassigncheckpointupdate', { url: '/ControlFormAssignCheckPoint/Update/{id}', templateUrl: 'ControlFormAssignCheckPoint/Update'        })
				.state('controlformassigncheckpointdelete', { url: '/ControlFormAssignCheckPoint/Delete/{id}', templateUrl: 'ControlFormAssignCheckPoint/Delete'        })
				.state('controlformassigncheckpointgetcheck', { url: '/ControlFormAssignCheckPoint/Check/{checkId}', templateUrl: 'ControlFormAssignCheckPoint/Check'   })
				// ControlItem
				.state('controlitemgetall', { url: '/ControlItem', templateUrl: 'ControlItem'                                                                           })
				.state('controlitemcreate', { url: '/ControlItem/Create', templateUrl: 'ControlItem/Create'                                                             })
				.state('controlitemupdate', { url: '/ControlItem/Update/{id}', templateUrl: 'ControlItem/Update'                                                        })
				.state('controlitemdelete', { url: '/ControlItem/Delete/{id}', templateUrl: 'ControlItem/Delete'                                                        })
				.state('controlitemaddtag', { url: '/ControlItem/AddTag/{id}', templateUrl: 'ControlItem/AddTag'                                                        })
				.state('controlitemremovetag', { url: '/ControlItem/RemoveTag', templateUrl: 'ControlItem/RemoveTag'                                                    })
				// ControlPlanning
				.state('controlplanningclear', { url: '/ControlPlanning/Clear/{workerId}', templateUrl: 'ControlPlanning/Clear'                                         })
				.state('controlplanningcreate', { url: '/ControlPlanning/Create/{workerId}', templateUrl: 'ControlPlanning/Create'                                      })
				.state('controlplanningcreatecheck', {
					url: '/ControlPlanning/CreateCheck/{workerId}', templateUrl: 'ControlPlanning/CreateCheck',
					resolve: {
						deps: ['uiLoad', function (uiLoad) {
							return uiLoad.load([
								//'/Vendors/angulr/js/app/map/load-google-maps.js',
								'/Vendors/angulr/js/app/map/ui-map.js',
								'/Vendors/angulr/js/app/map/map.js'
							]).then(
								function () {
									return loadGoogleMaps();
								}
							);
						}]
					}
				})
				.state('controlplanningcreatetemplate', { url: '/ControlPlanning/CreateTemplate/{workerId}', templateUrl: 'ControlPlanning/CreateTemplate' })
				.state('controlplanningdelete', { url: '/ControlPlanning/Delete/{id}', templateUrl: 'ControlPlanning/Delete' })
				.state('controlplanningupdate', { url: '/ControlPlanning/Update/{id}', templateUrl: 'ControlPlanning/Update' })
				.state('controlplanninggetall', {
					url: '/ControlPlanning/{workerId}', templateUrl: 'ControlPlanning',
					resolve: {
						deps: ['$ocLazyLoad', 'uiLoad', function ($ocLazyLoad, uiLoad) {
							return uiLoad.load(
								['Vendors/angulr/vendor/jquery/fullcalendar/fullcalendar.css',
									'Vendors/angulr/vendor/jquery/fullcalendar/theme.css',
									'Vendors/angulr/vendor/jquery/jquery-ui-1.10.3.custom.min.js',
									'Vendors/angulr/vendor/jquery/fullcalendar/fullcalendar.js',
									'Vendors/angulr/vendor/modules/angular-ui-calendar/calendar.js',
									'app/js/xp.calendar.js'
								]).then(
								function () {
									return $ocLazyLoad.load('ui.calendar');
								}
							)
						}]
					}
				})
				// ControlPlanningCheck
				.state('controlplanningcheckcreate',          { url: '/ControlPlanningCheck/Create/{workerId}',       templateUrl: 'ControlPlanningCheck/Create'        })
				.state('controlplanningcheckdelete',          { url: '/ControlPlanningCheck/Delete/{id}',             templateUrl: 'ControlPlanningCheck/Delete'        })
				.state('controlplanningcheckget',             { url: '/ControlPlanningCheck/{id}',                    templateUrl: 'ControlPlanningCheck/View'          })
				.state('controlplanningcheckupdate',          {	url: '/ControlPlanningCheck/Update/{id}',             templateUrl: 'ControlPlanningCheck/Update'        })
				// ControlPlanningItem
				.state('controlplanningitemcreate',           { url: '/ControlPlanningItem/Create/{workerId}',        templateUrl: 'ControlPlanningItem/Create'         })
				.state('controlplanningitemdelete',           { url: '/ControlPlanningItem/Delete/{id}',              templateUrl: 'ControlPlanningItem/Delete'         })
				.state('controlplanningitemupdate',           { url: '/ControlPlanningItem/Update/{id}',              templateUrl: 'ControlPlanningItem/Update'         })
				// ControlPresence
				.state('controlpresencegetall', {
					url: '/ControlPresence', templateUrl: 'ControlPresence',
					resolve: {
						deps: ['uiLoad', function (uiLoad) {
						return uiLoad.load([
							//'/Vendors/angulr/js/app/map/load-google-maps.js',
							'/Vendors/angulr/js/app/map/ui-map.js',
							'/Vendors/angulr/js/app/map/map.js'
						]).then(
							function () {
								return loadGoogleMaps();
							}
						);
						}]
					}
				})
				.state('controlpresencelegacy', { url: '/ControlPresence/Legacy', templateUrl: 'ControlPresence/Legacy' })
				.state('controlpresencecreate', { url: '/ControlPresence/Create', templateUrl: 'ControlPresence/Create' })
				.state('controlpresenceupdate', { url: '/ControlPresence/Update/{id}', templateUrl: 'ControlPresence/Update' })
				.state('controlpresencedelete', { url: '/ControlPresence/Delete/{id}', templateUrl: 'ControlPresence/Delete' })
				.state('controlpresencegethours', {
					url: '/ControlPresence/Graph/{workerId}', templateUrl: 'ControlPresence/Graph',
					resolve: {
						deps: ['$ocLazyLoad', 'uiLoad', function ($ocLazyLoad, uiLoad) {

							return uiLoad.load(
								['/Vendors/D3/d3.min.js',
								'/app/js/xp.graph.js'

								])
						}]
					}
				})
				// ControlTemplate
				.state('controltemplategetall', { url: '/ControlTemplate', templateUrl: 'ControlTemplate' })
				.state('controltemplatecreate', { url: '/ControlTemplate/Create', templateUrl: 'ControlTemplate/Create' })
				.state('controltemplateupdate', { url: '/ControlTemplate/Update/{id}', templateUrl: 'ControlTemplate/Update' })
				.state('controltemplatedelete', { url: '/ControlTemplate/Delete/{id}', templateUrl: 'ControlTemplate/Delete' })
				// ControlTemplateItem
				.state('controltemplateitemgettemplate', { url: '/ControlTemplateItem/Template/{templateId}', templateUrl: 'ControlTemplateItem/Template' })
				.state('controltemplateitemcreate', { url: '/ControlTemplateItem/Create/{templateId}', templateUrl: 'ControlTemplateItem/Create' })
				.state('controltemplateitemupdate', { url: '/ControlTemplateItem/Update/{id}', templateUrl: 'ControlTemplateItem/Update' })
				.state('controltemplateitemdelete', { url: '/ControlTemplateItem/Delete/{id}', templateUrl: 'ControlTemplateItem/Delete' })
				// ControlTemplateCheck
				.state('controltemplatecheckcreate', { url: '/ControlTemplateCheck/Create/{templateId}', templateUrl: 'ControlTemplateCheck/Create' })
				.state('controltemplatecheckdelete', { url: '/ControlTemplateCheck/Delete/{id}', templateUrl: 'ControlTemplateCheck/Delete' })
				.state('controltemplatecheckupdate', { url: '/ControlTemplateCheck/Update/{id}', templateUrl: 'ControlTemplateCheck/Update' })
				.state('controltemplatecheckgettemplate', { url: '/ControlTemplateCheck/Template/{templateId}', templateUrl: 'ControlTemplateCheck/Template' })
				// ControlTrack				
				.state('controltrackgetall', { url: '/ControlTrack', templateUrl: 'ControlTrack' })
				.state('controltrackget', {
					url: '/ControlTrack/{id}', templateUrl: 'ControlTrack/View',
					resolve: {
						deps: ['uiLoad', function (uiLoad) {
							return uiLoad.load([
								'/app/js/xp.map2.js',
								'/Vendors/D3/d3.min.js',
								'/app/js/xp.graph.js',
								'/app/js/xp.slide.js'
						]);
						}]
					}
				})
				.state('controltrackgetitem', {
					url: '/ControlTrack/Item/{itemId}', templateUrl: 'ControlTrack/Item',
					resolve: {
						deps: ['uiLoad', function (uiLoad) {
							return uiLoad.load([
								//'/Vendors/angulr/js/app/map/load-google-maps.js',
								//'/Vendors/angulr/js/app/map/jsapi.js',
								'/app/js/xp.map2.js'
						]);
						}]
					}
				})
				//Home
				.state('homepopup',                   { url: '/Home/Popup/{id}',                             templateUrl: '/Home/Popup' })
				//Liquidation																                 
				.state('liquidationgetall',           { url: '/Liquidation',                                 templateUrl: 'Liquidation' })
				.state('liquidationpay',              { url: '/Liquidation/Liquidations',                    templateUrl: '/Liquidation/Liquidations' })
				.state('liquidationchange',           { url: '/Liquidation/Change',                          templateUrl: '/Liquidation/Change' })
				.state('liquidationopen',             { url: '/Liquidation/Open',                            templateUrl: '/Liquidation/Open' })
				.state('liquidationconfirmpay',       { url: '/Liquidation/ConfirmPay/{id}',                 templateUrl: '/Liquidation/ConfirmPay' })
				.state('liquidationconfirmunpay',     { url: '/Liquidation/ConfirmUnpay/{id}',               templateUrl: '/Liquidation/ConfirmUnpay' })
				 //Payment																		             
				.state('paymentgetall',               { url: '/Payment',                                     templateUrl: 'Payment' })
				.state('paymentgetcharges',           { url: '/Payment/Charges',                             templateUrl: 'Payment/Charges' })
				.state('paymentpaymentdetails',       { url: '/Payment/PaymentDetails/{id}',                 templateUrl: 'Payment/PaymentDetails' })
				.state('liquidationpayments',         { url: '/Payment/LiquidationPayments/{liquidationId}', templateUrl: 'Payment/LiquidationPayments' })
				.state('liquidationnull',             { url: '/Payment/LiquidationPayment',                  templateUrl: 'Payment/LiquidationNull' })
			    .state('paymentgraph', {
			    	url: '/Payment/Graph', templateUrl: 'Payment/Graph',
			    	resolve: {
			    		deps: ['$ocLazyLoad', 'uiLoad', function ($ocLazyLoad, uiLoad) {

			    			return uiLoad.load(
								['/Vendors/D3/d3.min.js',
								'/app/js/xp.graph.js'
								])
			    		}]
			    	}
			    })
				//PaymentConcession
				//.state('paymentconcessiongetall',         { url: '/PaymentConcession',                     templateUrl: 'PaymentConcession'})
				.state('paymentconcessioncreate',         { url: '/PaymentConcession/Create',              templateUrl: 'PaymentConcession/Create' })
				.state('paymentconcessioncommercecreate', { url: '/PaymentConcession/CreateCommerce',      templateUrl: 'PaymentConcession/CreateCommerce' })
				.state('paymentconcessionupdate',         { url: '/PaymentConcession/Update/{id}',         templateUrl: 'PaymentConcession/Update' })
				.state('paymentconcessionupdatecommerce', { url: '/PaymentConcession/UpdateCommerce/{id}', templateUrl: 'PaymentConcession/UpdateCommerce' })
				//PaymentWorker
				.state('paymentworkergetall',			  { url: '/PaymentWorker',					       templateUrl: 'PaymentWorker'})
				.state('paymentworkercreate',             { url: '/PaymentWorker/Create', templateUrl: 'PaymentWorker/Create' })
				.state('paymentworkergetallconcession',   { url: '/PaymentWorker/Concession', templateUrl: 'PaymentWorker/Concession' })
				.state('paymentworkerdelete',             { url: '/PaymentWorker/Delete/{id}', templateUrl: 'PaymentWorker/Delete' })
				.state('paymentworkerdissociateconcession', { url: '/PaymentWorker/DissociateConcession/{id}', templateUrl: 'PaymentWorker/DissociateConcession' })
				.state('paymentworkerresendnotification', { url: '/PaymentWorker/{id}', templateUrl: 'PaymentWorker/ResendNotification' })
				// ServiceAddress
				.state('serviceaddressgetall',            { url: '/ServiceAddress',                     templateUrl: 'ServiceAddress' })
				.state('serviceaddresscreate',            { url: '/ServiceAddress/Create',              templateUrl: 'ServiceAddress/Create' })
				.state('serviceaddressdelete',            { url: '/ServiceAddress/Delete/{id}',         templateUrl: 'ServiceAddress/Delete' })
				.state('serviceaddresscreatename',        { url: '/ServiceAddress/CreateName/{id}',     templateUrl: 'ServiceAddress/CreateName' })
				.state('serviceaddressdeletename',        { url: '/ServiceAddress/DeleteName/{id}',     templateUrl: 'ServiceAddress/DeleteName' })
 				.state('serviceaddressupdatename',        { url: '/ServiceAddress/UpdateName/{id}',     templateUrl: 'ServiceAddress/UpdateName' })
				// ServiceCheckPoint
				.state('servicecheckpointgetitemchecks', { url: '/ServiceCheckPoint/Item/{itemId}', templateUrl: 'ServiceCheckPoint/Item' })
				.state('servicecheckpointdelete', { url: '/ServiceCheckPoint/Delete/{id}', templateUrl: 'ServiceCheckPoint/Delete' })
				//.state('servicecheckpointgetallcheckpoint', { url: '/ServiceCheckPoint/IndexCheckPoint',	   templateUrl: 'ServiceCheckPoint/IndexCheckPoint'})
				.state('servicecheckpointadditem', { url: '/ServiceCheckPoint/AddItem/{id}', templateUrl: 'ServiceCheckPoint/AddItem' })
				.state('servicecheckpointcreate', {
					url: '/ServiceCheckPoint/Create?itemId', templateUrl: 'ServiceCheckPoint/Create',
					resolve: {
						deps: ['uiLoad', function (uiLoad) {
							return uiLoad.load([
								'/Vendors/angulr/js/app/map/load-google-maps.js',
								'/Vendors/angulr/js/app/map/ui-map.js',
								'/Vendors/angulr/js/app/map/map.js',
								'/app/js/xp.map2.js'
							]).then(
								function () {
									return loadGoogleMaps();
								}
							);
						}]
					}
				})
				.state('servicecheckpointcreatecheckpoint', {
					url: '/ServiceCheckPoint/CreateCheckPoint', templateUrl: 'ServiceCheckPoint/CreateCheckPoint',
					resolve: {
						deps: ['uiLoad', function (uiLoad) {
							return uiLoad.load([
								'/Vendors/angulr/js/app/map/load-google-maps.js',
								'/Vendors/angulr/js/app/map/ui-map.js',
								'/Vendors/angulr/js/app/map/map.js',
								'/app/js/xp.map2.js'
							]).then(
								function () {
									return loadGoogleMaps();
								}
							);
						}]
					}
				})
				.state('servicecheckpointupdate', {
					url: '/ServiceCheckPoint/Update/{id}', templateUrl: 'ServiceCheckPoint/Update',
					resolve: {
						deps: ['uiLoad', function (uiLoad) {
							return uiLoad.load([
								'/Vendors/angulr/js/app/map/load-google-maps.js',
								'/Vendors/angulr/js/app/map/ui-map.js',
								'/Vendors/angulr/js/app/map/map.js',
								'/app/js/xp.map2.js'

							]).then(
								function () {
									return loadGoogleMaps();
								}
							);
						}]
					}
				})
				//ServiceConcession
				.state('serviceconcessiongetall',		{ url: '/ServiceConcession', templateUrl: 'ServiceConcession' })
				.state('serviceconcessiongetstate',    { url: '/ServiceConcession/States', templateUrl: '/ServiceConcession/States'                            })
                .state('serviceconcessionupdatestate', { url: '/ServiceConcession/UpdateState', templateUrl: '/ServiceConcession/UpdateState'                  })
				.state('serviceconcessionupdate',      { url: '/ServiceConcession/Update/{id}', templateUrl: 'ServiceConcession/Update'                        })
				.state('serviceconcessionupdatecommerce', { url: '/ServiceConcession/UpdateCommerce/{id}', templateUrl: 'ServiceConcession/UpdateCommerce' })
				.state('serviceconcessiondelete',      { url: '/ServiceConcession/Delete/{id}', templateUrl: 'ServiceConcession/Delete' })

			
				// ServiceFreeDays
				.state('servicefreedaysgetall', {
					url: '/ServiceFreeDays', templateUrl: 'ServiceFreeDays',
					resolve: {
						deps: ['$ocLazyLoad', 'uiLoad', function ($ocLazyLoad, uiLoad) {
						return uiLoad.load(
							['Vendors/angulr/vendor/jquery/fullcalendar/fullcalendar.css',
								'Vendors/angulr/vendor/jquery/fullcalendar/theme.css',
								'Vendors/angulr/vendor/jquery/jquery-ui-1.10.3.custom.min.js',
								'Vendors/angulr/vendor/jquery/fullcalendar/fullcalendar.js',
								'Vendors/angulr/vendor/modules/angular-ui-calendar/calendar.js',
								'Vendors/angulr/js/app/calendar/calendar.js'
							]).then(
							function () {
								return $ocLazyLoad.load('ui.calendar');
					}
						)
						}]
					}
				})
				.state('servicefreedayscreate', { url: '/ServiceFreeDays/Create', templateUrl: 'ServiceFreeDays/Create'                                        })
				.state('servicefreedaysupdate', { url: '/ServiceFreeDays/Update/{id}', templateUrl: 'ServiceFreeDays/Update'                                   })
				.state('servicefreedaysdelete', { url: '/ServiceFreeDays/Delete/{id}', templateUrl: 'ServiceFreeDays/Delete'                                   })
				// ServicePrice
				.state('servicepricegetall', { url: '/ServicePrice', templateUrl: 'ServicePrice'                                                               })
				.state('servicepricecreate', { url: '/ServicePrice/Create/{id}', templateUrl: 'ServicePrice/Create'                                            })
				.state('servicepriceupdate', { url: '/ServicePrice/Update/{id}', templateUrl: 'ServicePrice/Update'                                            })
				.state('servicepricedelete', { url: '/ServicePrice/Delete/{id}', templateUrl: 'ServicePrice/Delete'                                            })
				//ServiceSupplier
                .state('servicesuppliergetall', { url: '/Concessions', templateUrl: 'ServiceSupplier' })
				.state('servicesupplierupdate', { url: '/Concession/Update/{id}', templateUrl: 'ServiceSupplier/Update' })
				// ServiceTag
				.state('servicetaggetall', { url: '/ServiceTag', templateUrl: 'ServiceTag' })
				.state('servicetagcreate', { url: '/ServiceTag/Create', templateUrl: 'ServiceTag/Create' })
				.state('servicetagdelete', { url: '/ServiceTag/Delete/{id}', templateUrl: 'ServiceTag/Delete' })
				.state('servicetagupdate', { url: '/ServiceTag/{id}', templateUrl: 'ServiceTag/Update' })
				// ServiceTimeTable
				.state('servicetimetablegetall', {
					url: '/ServiceTimeTable', templateUrl: 'ServiceTimeTable',
					resolve: {
						deps: ['$ocLazyLoad', 'uiLoad', function ($ocLazyLoad, uiLoad) {
						return uiLoad.load(
							['Vendors/angulr/vendor/jquery/fullcalendar/fullcalendar.css',
								'Vendors/angulr/vendor/jquery/fullcalendar/theme.css',
								'Vendors/angulr/vendor/jquery/jquery-ui-1.10.3.custom.min.js',
								'Vendors/angulr/vendor/jquery/fullcalendar/fullcalendar.js',
								'Vendors/angulr/vendor/modules/angular-ui-calendar/calendar.js',
								'Vendors/angulr/js/app/calendar/calendar.js'
							]).then(
							function () {
								return $ocLazyLoad.load('ui.calendar');
							}
						)
						}]
					}
				})
				.state('servicetimetablecreate', { url: '/ServiceTimeTable/Create', templateUrl: 'ServiceTimeTable/Create' })
				.state('servicetimetabledelete', { url: '/ServiceTimeTable/Delete/{id}', templateUrl: 'ServiceTimeTable/Delete' })
				.state('servicetimetableupdate', { url: '/ServiceTimeTable/Update/{id}', templateUrl: 'ServiceTimeTable/Update' })
				// ServiceWorker
				.state('serviceworkergetall', { url: '/ServiceWorker', templateUrl: 'ServiceWorker' })
				.state('serviceworkergetcontrol', { url: '/ServiceWorker/Control', templateUrl: 'ServiceWorker/Control' })
				.state('serviceworkercreate', { url: '/ServiceWorker/Create', templateUrl: 'ServiceWorker/Create' })
				.state('serviceworkerupdate', { url: '/ServiceWorker/Update/{id}', templateUrl: 'ServiceWorker/Update' })
				.state('serviceworkerdelete', { url: '/ServiceWorker/Delete/{id}', templateUrl: 'ServiceWorker/Delete' })

				// Ticket
				.state('ticketgetall',		     { url: '/Ticket',					           templateUrl: 'Ticket' })
				.state('ticketcreate',		     { url: '/Ticket/Create',			         templateUrl: 'Ticket/Create' })
				.state('ticketupdate',		     { url: '/Ticket/Update/{id}',		     templateUrl: 'Ticket/Update' })
				.state('ticketdetails',        { url: '/Ticket/Details/{id}',        templateUrl: 'Ticket/Details' })
				.state('ticketchargedetails',  { url: '/Ticket/ChargeDetails/{id}',  templateUrl: 'Ticket/ChargeDetails' })
				.state('ticketdetailsdelete',  { url: '/Ticket/Delete/{id}',         templateUrl: 'Ticket/Delete' })
				.state('ticketdeletedetail',   { url: '/Ticket/DeleteDetail/:id',    templateUrl: 'Ticket/DeleteDetail' })
				.state('ticketcreatedetail',   { url: '/Ticket/CreateDetail/:id',    templateUrl: 'Ticket/CreateDetail' })
				.state('ticketupdatedetail',   { url: '/Ticket/UpdateDetail/:id',    templateUrl: 'Ticket/UpdateDetail' })
				.state('ticketgraph',          { url: '/Ticket/Graph',               templateUrl: 'Ticket/Graph',
						resolve: {
							deps: ['$ocLazyLoad', 'uiLoad', function ($ocLazyLoad, uiLoad) {

								return uiLoad.load(
									['/Vendors/D3/d3.min.js',
									'/app/js/xp.graph.js'
									])
							}]
						}
					})
		}
	]
);