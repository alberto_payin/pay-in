'use strict';
angular.module('app')
  .controller('AppCtrl', ['$rootScope', '$translate', '$localStorage', '$window', function ($rootScope, $translate, $localStorage, $window) {
  	// add 'ie' classes to html
  	var isIE = !!navigator.userAgent.match(/MSIE/i);
  	isIE && angular.element($window.document.body).addClass('ie');
  	isSmartDevice($window) && angular.element($window.document.body).addClass('smart');
  

  	// config
  	$rootScope.app = {
  		apiBaseUrl: '', // La baseURL es la ra�z
  		//tenant: '',
  		pageSize: 12,
  		columns: 3,
  		name: 'Pay[in]',
  		version: 'v2.1.0a',
  		// for chart colors
  		color: {
  			primary: '#E9AF30', //'#7266ba',
  			info: '#23b7e5',
  			success: '#27c24c',
  			warning: '#fad733',
  			danger: '#f05050',
  			light: '#e8eff0',
  			dark: '#3a3f51',
  			black: '#1c2b36'
  		},
  		settings: {
  			themeID: 1,
  			navbarHeaderColor: 'bg-orange',
  			navbarCollapseColor: 'bg-white-only',
  			asideColor: 'bg-black',
  			headerFixed: true,
  			asideFixed: false,
  			asideFolded: false,
  			asideDock: false,
  			container: false
  		}    		
  	}



  	// save settings to local storage
  	if (angular.isDefined($localStorage.settings)) {
  		$rootScope.app.settings = $localStorage.settings;
  	} else {
  		$localStorage.settings = $rootScope.app.settings;
  	}
  	$rootScope.$watch('app.settings', function () {
  		if ($rootScope.app.settings.asideDock && $rootScope.app.settings.asideFixed) {
  			// aside dock and fixed must set the header fixed.
  			$rootScope.app.settings.headerFixed = true;
  		}
  		// save to local storage
  		$localStorage.settings = $rootScope.app.settings;
  	}, true);

  	// angular translate
  	$rootScope.lang = { isopen: false };
  	//$scope.langs = { en: 'English', de_DE: 'German', it_IT: 'Italian' };
  	$rootScope.langs = { es_ES: 'Spanish', en: 'English' };
  	var language = $localStorage.lang || navigator.userLanguage || navigator.language  || navigator.languages[0];
  	((language == 'es') || (language=='es-ES') || (language == 'es_ES')) ? $rootScope.selectLang = "Spanish" : $rootScope.selectLang = "English";

  	$rootScope.setLang = function (langKey, $event) {
  		// set the current lang
  		$rootScope.selectLang = $rootScope.langs[langKey];
  		// You can change the language during runtime
  		$translate.use(langKey);
  		$rootScope.lang.isopen = !$rootScope.lang.isopen;
  		$localStorage.lang = langKey;
  	}; 
  	

  	function isSmartDevice($window) {
  		// Adapted from http://www.detectmobilebrowsers.com
  		var ua = $window['navigator']['userAgent'] || $window['navigator']['vendor'] || $window['opera'];
  		// Checks for iOs, Android, Blackberry, Opera Mini, and Windows mobile devices
  		return (/iPhone|iPod|iPad|Silk|Android|BlackBerry|Opera Mini|IEMobile/).test(ua);
  	}
  }])
	.filter('xpJourneyStart', function () {
		return function (since) {
			if (since) {
				var hours = moment(since, ["HH:mm:ssZ", moment.ISO_8601]).hours() * 100 / 24;
				return hours;
			};
		}
	})
	.filter('xpJourneyStartCSS', function () {
		return function (since) {
			if (since) {
				var hours = (moment(since, ["HH:mm:ssZ", moment.ISO_8601]).hours() * 100 / 24) - 9;
				return hours;
			};
		}
	})
	.filter('xpJourneyLength', function () {
		return function (until, since) {
			if (until && since) {
				var hours = (
					moment(until, ["HH:mm:ssZ", moment.ISO_8601]).hours() - moment(since, ["HH:mm:ssZ", moment.ISO_8601]).hours()
				) * 100 / 24;
				return hours;
			};
		}
	})
	.filter('xpJourneyLengthCSS', function () {
		return function (until, since) {
			if (until && since) {
				var hours = (
					(moment(until, ["HH:mm:ssZ", moment.ISO_8601]).hours() - moment(since, ["HH:mm:ssZ", moment.ISO_8601]).hours()
				) * 100 / 24) - 7;
				return hours;
			};
		}
	})
	.filter('xpHourToTime', function() {
		return function(time) {		
			var hours = Math.trunc(time);
			var minutes = Math.trunc(Math.abs(time - hours) * 60);
			var seconds = Math.trunc(Math.abs(time - hours) * 60 - minutes) * 60;
			var timeString =
				(hours || "0") +
				((minutes < 10) ? ":0" + minutes : ":" + minutes) +
				((seconds < 10) ? ":0" + seconds : ":" + seconds);
			return timeString;
		}
	})
	.controller('ControlTrackGetItemController', ['$scope', 'xpCommunication', function ($scope, xpCommunication) {
		$scope.temp.tracks = [];

		$scope.$watch('data', function (data) {
			var tracks = [];
			var trackEnum = [];

			angular.forEach(data, function (item) {
				var positions = [];

				angular.forEach(item.items, function (pos) {
					var position = {
						latitude: pos.latitude,
						longitude: pos.longitude,
						title: moment(pos.date, moment.ISO_8601).format("HH:mm:ss"),
						date: new Date(pos.date),
						velocity: pos.velocity * 3600 / 1000
					};

					positions.push(position);
				});

				tracks.push({
					name: item.since ? moment(item.since.date || item.until.date).format("HH:mm:ss") : "",
					since: item.since,
					until: item.until,
					positions: positions
				});
			});

			$scope.temp.tracks = tracks;
			
		});
		$scope.$watch('arguments.date', function () {
			$scope.temp.tracks = [];

			if ($scope.arguments.trackId)
				delete $scope.arguments.trackId; // $scope.search() executed in watch
			else
				$scope.search();

			// Load tracks enum
			xpCommunication
				.get('Api/ControlTrack', "", { date: $scope.arguments.date, itemId: $scope.arguments.itemId })
				.success(function (data) {
					$scope.temp.trackEnum = data.data;
				});
		});
		$scope.$watch('arguments.trackId', function () {
			$scope.search();
		});
	}])
	.controller('ControlTrackGetController', ['$scope', function ($scope) {
		$scope.temp.tracks = [];
		$scope.temp.tracksmobile = [];
		var start = {};
		var end = {};

		$scope.$watch('data', function (data) {
			if (!$scope.temp.tracks.length && data.length) {
				var tracks = [];
				var tracksmobile = [];

				angular.forEach(data, function (item) {
					var positions = [];
					var positionsmobile = [];

					angular.forEach(item.items, function (pos) {
						var position = {
							latitude: pos.latitude,
							longitude: pos.longitude,
							title: moment(pos.date, moment.ISO_8601).format("HH:mm:ss"),
							date: new Date(pos.date),
							velocity: (pos.velocity * 3600 / 1000) || 0
						};
						positions.push(position);
						var positionmobile = {
							latitude: pos.latitude,
							longitude: pos.longitude,
							title: moment(pos.date, moment.ISO_8601).format("HH:mm:ss"),
							date: new Date(pos.date),
							velocity: pos.mobileVelocity * 3600 / 1000 || 0
						};
						positionsmobile.push(positionmobile);
					});

					tracks.push({
						name: item.since ? moment(item.since.date || item.until.date).format("HH:mm:ss") : "",
						since: item.since,
						until: item.until,
						positions: positions
					});
					tracksmobile.push({
						name: item.since ? moment(item.since.date || item.until.date).format("HH:mm:ss") : "",
						since: item.since,
						until: item.until,
						positions: positionsmobile
					});
				});
			
				$scope.temp.tracks = tracks;
				$scope.temp.tracksmobile = tracksmobile;
				
			}
		});

		var refresh = function () {
			if (
				start === $scope.arguments.start &&
				end === $scope.arguments.end
			)
				return;

			start = $scope.arguments.start;
			end = $scope.arguments.end;

			$scope.search();
		}
		
		$scope.$watch('arguments.start', function () {
			refresh();
		});
		$scope.$watch('arguments.end', function () {
			refresh();
		});
	}])
	.controller('ControlTrackGetAllController', ['$scope', function ($scope) {
		var date = {};
		var workerId = {};
		var itemId = {};

		var refresh = function () {
			if (
				date === $scope.arguments.date &&
				workerId === $scope.arguments.workerId &&
				itemId === $scope.arguments.itemId
			)
				return;

			date = $scope.arguments.date;
			workerId = $scope.arguments.workerId;
			itemId = $scope.arguments.itemId;

			$scope.search();
		}

		$scope.$watch('arguments.date', function () {
			refresh();
		});
		$scope.$watch('arguments.workerId', function () {
			refresh();
		})
		$scope.$watch('arguments.itemId', function () {
			refresh();
		})
	}])

	.controller('ControlPresenceGraphController', ['$scope', function ($scope) {
		var since = {};
		var until = {};
		var workerId = {};


		var refresh = function () {
			if (
				since === $scope.arguments.since &&
				until === $scope.arguments.until &&
				workerId === $scope.arguments.workerId
			)
				return;

			since = $scope.arguments.since;
			until = $scope.arguments.until;
			workerId = $scope.arguments.workerId;

			$scope.search();
		}

		$scope.$watch('arguments.since', function () {
			refresh();
		});
		$scope.$watch('arguments.until', function () {
			refresh();
		});
		$scope.$watch('arguments.workerId', function () {
			refresh();
		})

	}])
		

	.controller('PaymentGraphController', ['$scope', function ($scope) {
		var since = {};
		var until = {};


		var refresh = function () {
			if (
				since === $scope.arguments.since &&
				until === $scope.arguments.until
			)
				return;

			since = $scope.arguments.since;
			until = $scope.arguments.until;
			$scope.search();
		}

		$scope.$watch('arguments.since', function () {
			refresh();
		});
		$scope.$watch('arguments.until', function () {
			refresh();
		});
	}])

	.controller('PaymentGetChargesController', ['$scope', function ($scope) {
		var since = {};
		var until = {};


		var refresh = function () {
			if (
				since === $scope.arguments.since &&
				until === $scope.arguments.until
			)
				return;

			since = $scope.arguments.since;
			until = $scope.arguments.until;
			$scope.search();
		}

		$scope.$watch('arguments.since', function () {
			refresh();
		});
		$scope.$watch('arguments.until', function () {
			refresh();
		});
	}])

	.controller('TicketGraphController', ['$scope', function ($scope) {
			var since = {};
			var until = {};


			var refresh = function () {
				if (
					since === $scope.arguments.since &&
					until === $scope.arguments.until
				)
					return;

				since = $scope.arguments.since;
				until = $scope.arguments.until;
				$scope.search();
			}

			$scope.$watch('arguments.since', function () {
				refresh();
			});
			$scope.$watch('arguments.until', function () {
				refresh();
			});
		}])
			.controller('PaymentGetAllController', ['$scope', function ($scope) {
				var since = {};
				var until = {};


				var refresh = function () {
					if (
						since === $scope.arguments.since &&
						until === $scope.arguments.until
					)
						return;

					since = $scope.arguments.since;
					until = $scope.arguments.until;
					$scope.search();
				}

				$scope.$watch('arguments.since', function () {
					refresh();
				});
				$scope.$watch('arguments.until', function () {
					refresh();
				});
		}])
			.controller('PaymentGetChargesController', ['$scope', function ($scope) {
				var since = {};
				var until = {};


				var refresh = function () {
					if (
						since === $scope.arguments.since &&
						until === $scope.arguments.until
					)
						return;

					since = $scope.arguments.since;
					until = $scope.arguments.until;
					$scope.search();
				}

				$scope.$watch('arguments.since', function () {
					refresh();
				});
				$scope.$watch('arguments.until', function () {
					refresh();
				});
		}])
			.controller('TicketGetAllController', ['$scope', function ($scope) {
				var since = {};
				var until = {};


				var refresh = function () {
					if (
						since === $scope.arguments.since &&
						until === $scope.arguments.until
					)
						return;

					since = $scope.arguments.since;
					until = $scope.arguments.until;
					$scope.search();
				}

				$scope.$watch('arguments.since', function () {
					refresh();
				});
				$scope.$watch('arguments.until', function () {
					refresh();
				});
			}])
		.controller('LiquidationGetAllController', ['$scope', function ($scope) {
			var since = {};
			var until = {};


			var refresh = function () {
				if (
					since === $scope.arguments.since &&
					until === $scope.arguments.until
				)
					return;

				since = $scope.arguments.since;
				until = $scope.arguments.until;
				$scope.search();
			}

			$scope.$watch('arguments.since', function () {
				refresh();
			});
			$scope.$watch('arguments.until', function () {
				refresh();
			});
		}])

			.controller('LiquidationPayController', ['$scope', function ($scope) {
				var since = {};
				var until = {};


				var refresh = function () {
					if (
						since === $scope.arguments.since &&
						until === $scope.arguments.until
					)
						return;

					since = $scope.arguments.since;
					until = $scope.arguments.until;
					$scope.search();
				}

				$scope.$watch('arguments.since', function () {
					refresh();
				});
				$scope.$watch('arguments.until', function () {
					refresh();
				});
			}])

	  .controller('TicketGetAllDayController', ['$scope', function ($scope) {
		var since = {};
		var until = {};		

		var refresh = function () {
			if (
				since === $scope.arguments.since &&
				until === $scope.arguments.until
				)
				return;

			since = $scope.arguments.since;
			until = $scope.arguments.until;
			
			$scope.search();
		}

		$scope.$watch('arguments.since', function () {
			refresh();
		});
		$scope.$watch('arguments.until', function () {
			refresh();
		})

			}])
	  .controller('ControlPresenceGraphController', ['$scope', function ($scope) {
		var since = {};
		var until = {};
		var workerId = {};
		

		var refresh = function () {
			if (
				since === $scope.arguments.since &&
				until === $scope.arguments.until &&
				workerId === $scope.arguments.workerId
			)
				return;

			since = $scope.arguments.since;
			until = $scope.arguments.until;
			workerId = $scope.arguments.workerId;
			
			$scope.search();
		}

		$scope.$watch('arguments.since', function () {
			refresh();
		});
		$scope.$watch('arguments.until', function () {
			refresh();
		});
		$scope.$watch('arguments.workerId', function () {
			refresh();
		})
		
	}])
	.controller('ControlFormAssignUpdate_ValueController', ['$scope', '$element', '$compile', function ($scope, $element, $compile) {
		var isRequired = ($scope.arg.isRequired) ? "required" : "";
		var requiredMessage = "<div ng-show='form.value{{$index}}.$error.required'><span class='error control-label'>'{{arg.name}}' is required</span></div>";
		var observationMessage = "<div class='help-block m-b-none'>{{arg.observations}}</div>";
		var lineMessage = "<div class='line line-dashed b-b line-lg pull-in'></div>";

		//var value =
		//	($scope.arg.type == 6) ? "{{arg.valueDateTime|xpTime}}" : // Time
		//	($scope.arg.type == 8) ? "{{arg.valueDateTime|xpDuration}}" : // Duration
		//	($scope.arg.type == 5) ? "{{arg.valueDateTime|xpDate}}" : // Date
		//	($scope.arg.type == 7) ? "{{arg.valueDateTime|xpDateTime}}" : // Datetime
		//	($scope.arg.type == 3) ? "{{arg.valueNumeric}}" : // Double
		//	($scope.arg.type == 2) ? "{{arg.valueNumeric}}" : // Int
		//	($scope.arg.type == 4) ? "{{arg.valueBool}}" : // Bool
		//	"{{arg.valueString}}";

		//if ($scope.arg.target == 2) { // Check
		//	$element.html(
		//		"<div class='form-group col-md-12'>" +
		//			"<label>{{arg.name}}</label>" +
		//			"<div>" + value + "</div>" +
		//			observationMessage +
		//		"</div>" +
		//		lineMessage
		//	);
		//	// } else if ($scope.$arg.type == "enum") { }
		//} else
		if ($scope.arg.type == 6) { // Time
			$element.html(
				"<div data-ng-class=\"{'has-error':!form.value{{$index}}.$valid}\" class='form-group col-md-12'>" +
					"<label for='value{{$index}}' class='control-label'>{{arg.name}}</label>" +
					"<div class='input-group date' data-xp-time='arg.valueDateTime'>" +
						"<input id='value{{$index}}' name='value{{$index}}' type='text' class='form-control' data-ng-model='value' " + isRequired + "/>" +
						"<span class='input-group-addon'><span class='glyphicon glyphicon-time'></span></span>" +
						($scope.arg.isRequired ? "" : "<span class='input-group-addon'><span class='glyphicon glyphicon-remove'></span></span>") +
					"</div>" +
					requiredMessage +
					observationMessage +
				"</div>" +
				lineMessage
			);
		} else if ($scope.arg.type == 8) { // Duration
			$element.html(
				"<div data-ng-class=\"{'has-error':!form.value{{$index}}.$valid}\" class='form-group col-md-12'>" +
					"<label for='value{{$index}}' class='control-label'>{{arg.name}}</label>" +
					"<div class='input-group date' data-xp-duration='arg.valueDateTime'>" +
						"<input id='value{{$index}}' name='value{{$index}}' type='text' class='form-control' data-ng-model='value' " + isRequired + "/>" +
						"<span class='input-group-addon'><span class='glyphicon glyphicon-time'></span></span>" +
						($scope.arg.isRequired ? "" : "<span class='input-group-addon'><span class='glyphicon glyphicon-remove'></span></span>") +
					"</div>" +
					requiredMessage +
					observationMessage +
				"</div>" +
				lineMessage
			);
		} else if ($scope.arg.type == 5) { // Date
			$element.html(
				"<div data-ng-class=\"{'has-error':!form.value{{$index}}.$valid}\" class='form-group col-md-12'>" +
					"<label for='value{{$index}}' class='control-label'>{{arg.name}}</label>" +
					"<div class='input-group date' data-xp-date='arg.valueDateTime'>" +
						"<input id='value{{$index}}' name='value{{$index}}' type='text' class='form-control' data-ng-model='value' " + isRequired + "/>" +
						"<span class='input-group-addon'><span class='glyphicon glyphicon-calendar'></span></span>" +
						($scope.arg.isRequired ? "" : "<span class='input-group-addon'><span class='glyphicon glyphicon-remove'></span></span>") +
					"</div>" +
					requiredMessage +
					observationMessage +
				"</div>" +
				lineMessage
			);
		} else if ($scope.arg.type == 7) { // Datetime
			$element.html(
				"<div data-ng-class=\"{'has-error':!form.value{{$index}}.$valid}\" class='form-group col-md-12'>" +
					"<label for='value{{$index}}' class='control-label'>{{arg.name}}</label>" +
					"<div class='input-group date' data-xp-date-time='arg.valueDateTime'>" +
						"<input id='value{{$index}}' name='value{{$index}}' type='text' class='form-control' data-ng-model='value' " + isRequired + "/>" +
						"<span class='input-group-addon'><span class='glyphicon glyphicon-calendar'></span></span>" +
						($scope.arg.isRequired ? "" : "<span class='input-group-addon'><span class='glyphicon glyphicon-remove'></span></span>") +
					"</div>" +
					requiredMessage +
					observationMessage +
				"</div>" +
				lineMessage
			);
		} else if ($scope.arg.type == 3) { // Double
			$element.html(
				"<div data-ng-class=\"{'has-error':!form.value{{$index}}.$valid}\" class='form-group col-md-12'>" +
					"<label for='value{{$index}}' class='control-label'>{{arg.name}}</label>" +
					"<input id='value{{$index}}' name='value{{$index}}' type='text' class='form-control' data-ng-model='arg.valueNumeric' " + isRequired + " ui-jq='TouchSpin' data-verticalbuttons='true' data-verticalupclass='fa fa-caret-up' data-verticaldownclass='fa fa-caret-down' data-step='0.000001' data-min='-180' data-decimals='6'>" +
					requiredMessage +
					observationMessage +
				"</div>" +
				lineMessage
			);
		} else if ($scope.arg.type == 2) { // Int
			$element.html(
				"<div data-ng-class=\"{'has-error':!form.value{{$index}}.$valid}\" class='form-group col-md-12'>" +
					"<label for='value{{$index}}' class='control-label'>{{arg.name}}</label>" +
					"<input id='value{{$index}}' name='value{{$index}}' type='custom' class='form-control' data-ng-model='arg.valueNumeric' " + isRequired + ">" +
					requiredMessage +
					observationMessage +
				"</div>" +
				lineMessage
			);
		// } else if ($scope.arg.type == "currency") {
		//	$element.html(
		//"<div data-ng-class=\"{'has-error':!form.value{{$index}}.$valid}\" class='form-group col-md-12'>" +
		//	"<label for='value{{$index}}' class='control-label'>{{arg.name}}</label>" +
		//		"<input id='value" + $scope.$index + "' name='value" + $scope.$index + "' type='text' class='form-control' data-ng-model='arg.valueDecimal' " + isRequired + " ui-jq='TouchSpin' data-verticalbuttons='true' data-verticalupclass='fa fa-caret-up' data-verticaldownclass='fa fa-caret-down' data-prefix='EUR'>" + 
		//    requiredMessage +
		//		observationMessage +
		//"</div>" +
		//lineMessage
		//	);
		//}
		} else if ($scope.arg.type == 4) { // Bool
			$element.html(
				"<div data-ng-class=\"{'has-error':!form.value{{$index}}.$valid}\" class='form-group col-md-12'>" +
					"<label class='i-checks'>" +
						"<input id='value{{$index}}' name='value{{$index}}' type='checkbox' class='form-control' data-ng-model='arg.valueBool'/>" +
						"<i></i>" +
						$scope.arg.name +
					"</label>" +
					observationMessage +
				"</div>" +
				lineMessage
			);
		} else {
			$element.html(
				"<div data-ng-class=\"{'has-error':!form.value{{$index}}.$valid}\" class='form-group col-md-12'>" +
					"<label for='value{{$index}}' class='control-label'>{{arg.name}}</label>" +
					"<input id='value{{$index}}' name='value{{$index}}' type='custom' class='form-control' ng-model='arg.valueString' " + isRequired + ">" +
					requiredMessage +
					observationMessage +
				"</div>" +
				lineMessage
			);
		}
		$compile($element.contents())($scope);
	}])
;