﻿/*
 * bootstrap-filestyle
 * doc: http://markusslima.github.io/bootstrap-filestyle/
 * github: https://github.com/markusslima/bootstrap-filestyle
 */

'use strict';
app
.directive('xpZip', [function () {
	function apply($scope, zip, model) {
		$scope.files[model] = zip.generate({ type: "blob" });
		if (!$scope.$$phase)
			$scope.$apply();
	}

	return {
		scope: true,
		link: function ($scope, $element, $attrs) {
			var model = $attrs['xpZip'];
			var inputs = $element.find('input');

			angular.forEach(inputs, function (input) {
				// Add disabled
				var disabled = $(input).attr('ng-disabled');
				$(input).filestyle({ buttonText: "" });
				var label = $element.find('label[for="' + model + '"]');

				if (disabled && label) {
					$scope.$watch(disabled, function () {
						var val = $scope.$eval(disabled);
						$(input).filestyle('disabled', val);
					});
				}

				// On change
				$(input).change(function (args) {
					var zip = new JSZip();
					var readers = [];
					var workingFiles = 0;
					var finished = false;
						
					angular.forEach(args.currentTarget.files, function (file) {
						// Add File
						var reader = new FileReader();
						readers.push(reader);
						workingFiles++;
						$scope.isBusy++;

						// On loaded
						reader.onload = function (evt, evt2) {
							var fileName = file.name;			
							zip.file(fileName, evt.target.result);
							workingFiles--;
							$scope.isBusy--;
								
							if (finished && (workingFiles === 0)) {
								finished = false; // se pone a false para que entre solo una vez
								apply($scope, zip, model);
							}
						};

						// Read
						reader.readAsArrayBuffer(file);

						finished = true;
						if (finished && (workingFiles === 0)) {
							finished = false; // se pone a false para que entre solo una vez
							apply($scope, zip, model);
						}
					});
				});
			});
		}
	};
}])	