﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PayIn.Common.Security;
using PayIn.Infrastructure.Security;
using PayIn.Web.Models;
using PayIn.Web.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace PayIn.Web.Security.Controllers
{
	public class AccountController : Controller
	{
		#region /Login
		[HttpGet]
		public ActionResult Login()
		{
            LoginViewModel model = new LoginViewModel();
            return View();
        }

        //ASM 20150921
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            HttpResponseMessage resultCode=null;
            JToken resultJSON = "";
            HttpClientNetwork.Instance.Login(model.Email, model.Password,out resultCode, out resultJSON);
            switch (resultCode.StatusCode)
            {
                case HttpStatusCode.OK:
                    TempData["accessToken"] = resultJSON["access_token"];
                    TempData["refreshToken"] = resultJSON["refresh_token"];
                    TempData["userName"] = resultJSON["as:name"];
                    TempData["roles"] = resultJSON["as:roles"];
                    TempData["clientId"] = resultJSON["as:client_id"];
                    break;
                default: //Error
                    ViewBag.Message = resultJSON["error_description"];
                    return View();
            }
            //ASM 20150928
            //Redirect url before asking for authentication!
            if (!string.IsNullOrEmpty(returnUrl))
            {
                string sBaseUrl = "";
                if (PayIn.Web.Helpers.UrlHelper.Instance.IsLocalUrl(returnUrl, out sBaseUrl))
                {
                    return Redirect(returnUrl);
                }
            }
            //ENDASM
            return RedirectToAction("Index", "Home");
        }
        //ENDASM
        #endregion /Login

		#region /Register
		[HttpGet]
		public ActionResult Register()
		{
			return View();
		}

		//ASM 20150928
		public bool IsValid(object value)
		{
			return value != null && (bool)value == true;
		}

		[HttpPost]
		[AllowAnonymous]
		[ValidateAntiForgeryToken]
		public ActionResult Register(RegisterViewModel model, string returnUrl)
		{
			if (!ModelState.IsValid || !IsValid(model.CheckTerms))
			{
				checkErrors(ModelState);
				return View(model);
			}

			HttpResponseMessage resultCode = null;
			JToken resultJSON = "";
			HttpClientNetwork.Instance.Register(model.Email, model.Name, model.Birthday, model.Mobile, model.Password, model.ConfirmPassword, model.CheckTerms, out resultCode, out resultJSON);
			switch (resultCode.StatusCode)
			{
				case HttpStatusCode.OK:
				case HttpStatusCode.NoContent:
					return RedirectToAction("Login", "Account");

			}
			//Error
			if (model.Password.Length < 6) ViewBag.Message = SecurityResources.passwordLengthException;
			else ViewBag.Message = SecurityResources.EmailExists;
			return View();
		}

		//ENDASM
		#endregion /Register

		#region /ConfirmEmail
		[HttpGet]
		public ActionResult ConfirmEmail()
		{
			ValidateEmailViewModel validateEmail = new ValidateEmailViewModel();
			if (!string.IsNullOrEmpty(this.Request.Params["userid"]))
			validateEmail.UserId = this.Request.Params["userid"];
			if (!string.IsNullOrEmpty(this.Request.Params["code"]))
				validateEmail.Code = System.Uri.EscapeUriString(this.Request.Params["code"]);
            return View(validateEmail);
		}
		//ASM 20150925
		[HttpPost]
		[AllowAnonymous]
		[ValidateAntiForgeryToken]
		public ActionResult ConfirmEmail(ValidateEmailViewModel model, string returnUrl)
		{
			if (!ModelState.IsValid)
			{
				return View(model);
			}
			HttpResponseMessage resultCode = null;
			HttpClientNetwork.Instance.ConfirmEmail(model.UserId, model.Code, out resultCode);
			switch (resultCode.StatusCode)
			{
				case HttpStatusCode.OK:
				case HttpStatusCode.NoContent:
					return RedirectToAction("Login", "Account");
			}

			return View();
		}
		//ENDASM
		#endregion /ConfirmEmail

		#region /ForgotPassword
		[HttpGet]
		public ActionResult ForgotPassword()
		{
			return View();
		}
		//ASM 20150925
		[HttpPost]
		[AllowAnonymous]
		[ValidateAntiForgeryToken]
		public ActionResult ForgotPassword(ForgotPasswordViewModel model, string returnUrl)
		{
			if (!ModelState.IsValid)
			{
				return View(model);
			}
			HttpResponseMessage resultCode = null;
			HttpClientNetwork.Instance.ForgotPassword(model.Email, out resultCode);
			switch (resultCode.StatusCode)
			{
				case HttpStatusCode.OK:
					return RedirectToAction("ConfirmForgotPassword", "Account");
			}

			return View();
		}
		//ENDASM
		#endregion /ForgotPassword

		#region /ConfirmForgotPassword
		[HttpGet]
		public ActionResult ConfirmForgotPassword()
		{
			return View();
		}
		//ASM 20150925
		[HttpPost]
		[AllowAnonymous]
		[ValidateAntiForgeryToken]
		public ActionResult ConfirmForgotPassword(ConfirmForgotPasswordViewModel model, string returnUrl)
		{
			if (!ModelState.IsValid)
			{
				checkErrors(ModelState);
				return View(model);
			}
			HttpResponseMessage resultCode = null;
			HttpClientNetwork.Instance.ConfirmForgotPassword(model.UserId, model.Code, model.Password, model.ConfirmPassword, out resultCode);
			switch (resultCode.StatusCode)
			{
				case HttpStatusCode.OK:
					return RedirectToAction("Login", "Account");
			}
			ViewBag.Message = SecurityResources.ValidateConfirmForgotPassword;
			return View();
		}
		//ENDASM
		#endregion /ConfirmForgotPassword

		#region /UpdatePassword
		[HttpGet]
		public ActionResult UpdatePassword()
		{
			return View();
		}
		#endregion /UpdatePassword

		#region /ChangePassword
		[HttpGet]
		public ActionResult ChangePassword()
		{
			return View();
		}
		#endregion /ChangePassword

		#region /Update
		[HttpGet]
		public ActionResult Update()
		{
			return PartialView();
		}
		#endregion /Update

		#region /ImageCrop
		[HttpGet]
		public ActionResult ImageCrop()
		{
			return PartialView();
		}
		#endregion /ImageCrop

		#region /DeleteImage
		public ActionResult DeleteImage()
		{
			return PartialView();
		}
        #endregion /DeleteImage

		#region methods
		private void checkErrors(ModelStateDictionary modelState)
		{
				try
				{
						ViewBag.Message = modelState.Values.SelectMany(v => v.Errors).First<ModelError>().ErrorMessage;
				}
				catch
				{
						ViewBag.Message = SecurityResources.CheckAcceptTerms;
				}
		}
		#endregion
	}
}
