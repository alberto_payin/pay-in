﻿using System.Web.Mvc;


namespace PayIn.Web.Controllers
{
	public class LiquidationController : Controller
	{
		#region /Index
		public ActionResult Index()
		{
			return PartialView();
		}
		#endregion /Index

		#region /Liquidations
		public ActionResult Liquidations()
		{
			return PartialView();
		}
		#endregion /Liquidations

		#region /Change
		public ActionResult Change()
		{
			return PartialView();
		}
		#endregion /Change


		#region /Open
		public ActionResult Open()
		{
			return PartialView();
		}
		#endregion /Open

		#region /ConfirmPay
		public ActionResult ConfirmPay()
		{
			return PartialView();
		}
		#endregion /ConfirmPay

		#region /ConfirmUnpay
		public ActionResult ConfirmUnpay()
		{
			return PartialView();
		}
		#endregion /ConfirmUnpay

		


	}
}