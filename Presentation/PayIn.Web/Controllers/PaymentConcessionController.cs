﻿using System.Web.Mvc;

namespace PayIn.Web.Controllers
{
	public class PaymentConcessionController : Controller
	{
		#region /
		public ActionResult Index()
		{
			return PartialView();
		}
		#endregion /

		#region /Create
		public ActionResult Create()
		{
			return PartialView();
		}
		#endregion /Create

		#region /CreateCommerce
		public ActionResult CreateCommerce()
		{
			return PartialView();
		}
		#endregion /CreateCommerce

		#region /Update
		public ActionResult Update()
		{
			return PartialView();
		}
		#endregion /Update

		#region /UpdateCommerce
		public ActionResult UpdateCommerce()
		{
			return PartialView();
		}
		#endregion /UpdateCommerce

	}
}