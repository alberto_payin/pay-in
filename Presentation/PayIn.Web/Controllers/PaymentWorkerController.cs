﻿using System.Web.Mvc;

namespace PayIn.Web.Controllers
{
	public class PaymentWorkerController : Controller
	{
		#region /
		public ActionResult Index()
		{
			return PartialView();
		}
		#endregion /

		#region /Create
		public ActionResult Create()
		{
			return PartialView();
		}
		#endregion /Create

		#region /Delete
		public ActionResult Delete()
		{
			return PartialView();
		}
		#endregion /Delete

		#region /Concession
		public ActionResult Concession()
		{
			return PartialView();
		}
		#endregion /Concession

		#region /DissociateConcession
		public ActionResult DissociateConcession()
		{
			return PartialView();
		}
		#endregion /DissociateConcession

		#region /ResendNotification
		public ActionResult ResendNotification()
		{
			return PartialView();
		}
		#endregion /ResendNotification
	}
}