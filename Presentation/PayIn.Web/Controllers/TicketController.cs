﻿using System.Web.Mvc;

namespace PayIn.Web.Controllers
{
	public class TicketController : Controller
	{
		#region /
		public ActionResult Index()
		{
			return PartialView();
		}
		#endregion /

		#region /Create
		public ActionResult Create()
		{
			return PartialView();
		}
		#endregion /Create

		#region /CreateDetail
		public ActionResult CreateDetail()
		{
			return PartialView();
		}
		#endregion

		#region /Details
		public ActionResult Details()
		{
			return PartialView();
		}
		#endregion /Details

		#region /Update
		public ActionResult Update()
		{
			return PartialView();
		}
		#endregion /Update

		#region /UpdateDetail
		public ActionResult UpdateDetail()
		{
			return PartialView();
		}
		#endregion /UpdateDetail

		#region /Delete
		public ActionResult Delete()
		{
			return PartialView();
		}
		#endregion /Delete

		#region /DeleteDetail
		public ActionResult DeleteDetail()
		{
			return PartialView();
		}
		#endregion /DeleteDetail

		#region /Graph
		public ActionResult Graph()
		{
			return PartialView();
		}
		#endregion /Graph
	}
}
