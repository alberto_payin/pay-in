﻿using Microsoft.Owin;
using Owin;
using System.Web.Http;

[assembly: OwinStartup(typeof(PayIn.Web.Startup))]
namespace PayIn.Web
{
	public partial class Startup
	{
		public void Configuration(IAppBuilder app)
		{
			var config = new HttpConfiguration();

			app
				.UseSecurity("token", config)
				.UseWebApi("api", config)
				.UseWebApi(config)
			;
		}
	}
}
