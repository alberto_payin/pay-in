﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PayIn.DistributedServices.Test.Helpers;
using System;
using System.Threading.Tasks;

namespace PayIn.DistributedServices.Test
{
	public class BaseTest
	{
#if DEBUG
		protected static string BaseAddress = "http://127.0.0.1:81/";
#else
		protected static string BaseAddress = "http://payin.cloudapp.net/";
#endif

		#region Server
		private Server _Server = null;
		protected Server Server
		{
			get
			{
				if (_Server == null)
					_Server = new Server(BaseAddress);
				return _Server;
			}
		}
		#endregion Server

		#region TokenWeb
		private string _TokenWeb = null;
		protected string TokenWeb
		{
			get
			{
				if (_TokenWeb == null)
				{
					_TokenWeb = Server.LoginWebAsync("superadministrator@pay-in.es", "Pa$$w0rd")
						.WaitWithResult<string>();
					Assert.AreNotEqual(_TokenWeb, "");
				}
				return _TokenWeb;
			}
		}
		#endregion TokenWeb
	}
}
