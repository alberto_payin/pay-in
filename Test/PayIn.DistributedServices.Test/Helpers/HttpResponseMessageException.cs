﻿using System;
using System.Net.Http;

namespace PayIn.DistributedServices.Test.Helpers
{
	public static class HttpResponseMessageException
	{
		#region ThrowException
		public static HttpResponseMessage ThrowException(this HttpResponseMessage that)
		{
			if (!that.IsSuccessStatusCode)
			{
#if (DEBUG)
				throw new Exception(that.ToString());
#else
				throw new Exception(that.StatusCode + " (" + ((int)that.StatusCode) + ")\n" + that.ReasonPhrase);
#endif
			}
			return that;
		}
		#endregion ThrowException
	}
}
