﻿using Newtonsoft.Json.Linq;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace PayIn.DistributedServices.Test.Helpers
{
	public class Server
	{
		#region BaseAddress
		public string BaseAddress { get; set; }
		#endregion BaseAddress

		#region Token
		public string Token { get; set; }
		#endregion Token

		#region Constructors
		public Server(string baseAddress)
		{
			BaseAddress = baseAddress;
		}
		#endregion Constructors

		#region ArgumentsToUrl
		public string ArgumentsToUrl(object query)
		{
			if (query == null)
				return "";

			var arguments = new StringBuilder();
			foreach (var property in query.GetType().GetProperties())
			{
				var value = property.GetValue(query);
				if (value != null)
				{
					var text = "";
					var isEnum = property.PropertyType.IsEnum;

					// Convert to communicate
					if (isEnum)
						text = ((int)value).ToString();
					else
						text = value.ToString();

					if (text != "")
					{
						if (arguments.Length > 0)
							arguments.Append("&" + property.Name + "=" + text);
						else
							arguments.Append(property.Name + "=" + text);
					}
				}
			}

			return arguments.ToString();
		}
		#endregion ArgumentsToUrl

		#region LoginWebAsync
		public async Task<string> LoginWebAsync(string user, string password)
		{
			var uri = new Uri(new Uri(BaseAddress), "token");
			var request = string.Format("grant_type=password&username={0}&password={1}&client_id=PayInWebApp", user, password);
			using (var client = new HttpClient())
			{
				var content = new StringContent(request, Encoding.UTF8);
				content.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");

				var response = (await client.PostAsync(uri, content))
					.ThrowException();

				var json = (await response.Content.ReadAsStringAsync());
				var result = json
					.FromJson<JObject>();

				Token = result["access_token"].Value<string>();
				return Token;
			}
		}
		#endregion LoginWebAsync

		#region GetAsync
		public async Task<JObject> GetAsync(string url, int id, object arguments = null)
		{
			using (var client = new HttpClient())
			{
				var argumentUris = ArgumentsToUrl(arguments);
				var uri = new Uri(BaseAddress + url + "/" + id +
					"?t=" + DateTime.Now.Ticks +
					(!argumentUris.IsNullOrEmpty() ? "&" + argumentUris : ""));

				client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);

				var response = (await client.GetAsync(uri))
					.ThrowException();
				var json = await response.Content.ReadAsStringAsync();

				var result = json
					.FromJson<JObject>();
				return result;
			}
		}
		public async Task<JObject> GetAsync(string url, object arguments = null)
		{
			using (var client = new HttpClient())
			{
				var argumentUris = ArgumentsToUrl(arguments);
				var uri = new Uri(BaseAddress + url +
					"?t=" + DateTime.Now.Ticks +
					(!argumentUris.IsNullOrEmpty() ? "&" + argumentUris : ""));

				client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);

				var response = (await client.GetAsync(uri))
					.ThrowException();
				var json = await response.Content.ReadAsStringAsync();

				var result = json
					.FromJson<JObject>();
				return result;
			}
		}
		#endregion GetAsync

		#region PostAsync
		public async Task<JObject> PostAsync(string url, object arguments = null)
		{
			using (var client = new HttpClient())
			{
				var content = new StringContent(arguments.ToJson(), Encoding.UTF8);
				content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

				client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);

				var uri = new Uri(BaseAddress + url);
				var response = (await client.PostAsync(uri, content))
					.ThrowException();

				var result = (await response.Content.ReadAsStringAsync())
					.FromJson<JObject>();
				return result;
			}
		}
		#endregion PostAsync

		#region PutAsync
		public async Task<JObject> PutAsync(string url, int id, object arguments = null)
		{
			using (var client = new HttpClient())
			{
				var content = new StringContent(arguments.ToJson(), Encoding.UTF8);
				content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

				client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);

				var uri = new Uri(BaseAddress + url + "/" + id);
				var response = (await client.PutAsync(uri, content))
					.ThrowException();

				var result = (await response.Content.ReadAsStringAsync())
					.FromJson<JObject>();
				return result;
			}
		}
		#endregion PutAsync

		#region DeleteAsync
		public async Task DeleteAsync(string url, int id)
		{
			using (var client = new HttpClient())
			{
				var uri = new Uri(BaseAddress + url + "/" + id);

				client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);

				(await client.DeleteAsync(uri))
					.ThrowException();
			}
		}
		#endregion DeleteAsync
	}
}